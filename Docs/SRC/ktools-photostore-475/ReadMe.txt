Use ktools-photostore-4XX-php52.zip if you are running PHP 5.2, 5.3 or 5.4 on your server.

Use ktools-photostore-4XX-php55.zip if you are running PHP 5.5 or higher on your server.

If you are unsure which version of PHP your server is running please contact us.
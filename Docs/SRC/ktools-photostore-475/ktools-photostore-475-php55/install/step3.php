<?php
	define('BASE_PATH',dirname(__FILE__)); // Define the base path
	
	require_once BASE_PATH.'/session.php';
	require_once BASE_PATH.'/config.php';
	require_once BASE_PATH.'/lang.php';
	require_once BASE_PATH.'/functions.php';
	$inc = 1;
	require_once BASE_PATH.'/version.php';
	
	$baseURL = dirname(curPageURL()); // Current URL
	$activationKey = createUniqueKey(); // Create an activation key
	
	if($_GET['fatalError']) // Convert to local if fatal error is passed
		$instError = $_GET['fatalError'];
	
	if(!empty($_POST['activate']))
	{
		// Make sure new key was passed back and that it is correct
		if($_POST['newKey'] == strrev(strtoupper(md5($_POST['oldKey']))) and !empty($_POST['newKey'])) // Check to make sure the key passed back is correct
		{	
			/* Done on the activation side
			// Make sure the mgrPass is passed or create it
			if(!empty($_POST['mgrPass']))
				$mgrPass = $_POST['mgrPass'];
			else
				$mgrPass = strtoupper(substr(md5($_POST['activate']),0,14));
			*/
			
			$_SESSION['activationInfo']['serialNumber'] = $_POST['serialNumber'];
			$_SESSION['activationInfo']['oldKey'] = $_POST['oldKey'];
			$_SESSION['activationInfo']['newKey'] = $_POST['newKey'];
			$_SESSION['activationInfo']['lockKey'] = $_POST['lockKey'];
			$_SESSION['activationInfo']['mgrPass'] = $_POST['mgrPass'];
			$apiPass = substr(createUniqueKey(),0,10);			
			$_SESSION['activationInfo']['apiPassEnc'] = k_encrypt($apiPass,$_POST['serialNumber']);
			//$_SESSION['activationInfo']['addons'] = $_POST['addons'];
			$_SESSION['activationInfo']['addonNames'] = $_POST['addonNames'];
			
			/*
			# UPDATE THE DATABASE WITH THE NEW INFORMATION
			$sql = "UPDATE {$dbinfo[pre]}settings SET serial_number='$_POST[activate]',newkey='$_POST[newkey]',lockkey='$_POST[lockkey]',debug_pass='$mgrpass',api_pass='$api_pass' where settings_id = '1'";
			$result = mysql_query($sql);
			*/
			
			$activationStatus = 1;
			
		}
		else
		{
			$instError = 'keys';
		}
	}
	
?>
<!DOCTYPE html>
<html>
<head>
	<?php require_once BASE_PATH.'/head.php'; ?>
	<script type="text/javascript" language="javascript">
		$(function()
		{
			// Go back to the previous page
			$('#formBackButton').click(function()
			{
				goto('step2.php');
			});
			
			$('.goodInfoBox').hide().fadeIn('slow');

		});
	</script>
</head>
<body>
	<form action="step4.php" name="dataform" id="dataform" method="post">
	<div id="container">
		<div id="installerBox">
			<p class="headerIcons">
				<a href="phpinfo.php" target="_blank"><img src="./images/php.logo.png" class="prodLogo opac40" title="PHP Info"></a>
				<a href="http://www.ktools.net/photostore/" target="_blank"><img src="./images/prod.logo.png" class="prodLogo opac40" title="Ktools.net PhotoStore"></a>
			</p>
			<h1 class="stepOff" style="margin-left: 20px; border-left: none;">Server Check</h1>
			<h1 class="stepOff">Activation</h1>
			<h1 class="stepOn">Add-ons</h1>
			<h1 class="stepOff">Install</h1>
			<h1 class="stepOff">Ioncube</h1>
			<h1 class="stepOff">Database</h1>
			<div id="content">
				<p>Your product has been activated and you can now continue installing: <strong><?php echo $_POST['installProdName']; ?></strong></p>
				<?php
					if($_POST['addons'])
					{
						foreach($_POST['addons'] as $addonID) // See if there are other add-ons besides pro
						{
							if($addonID != 'pro') $addonToInstall = true;
						}
						
						if(in_array('pro',$_POST['addons']))
							echo "<input type='checkbox' name='addons[pro]' value='pro' id='pro' checked='checked' style='display: none;'>";
					}
					
					if($addonToInstall)
					{
						echo "<div class='goodInfoBox'>";
						echo "<h2 class='checkFailed'>Add-ons</h2>";
						echo "<p style='padding: 15px; line-height: 1.3;'>We found that you have purchased the following add-ons. Please select any that you would like to install from the list below:</p>";
						echo "<ul class='addonsList'>";
						foreach($_POST['addons'] as $key => $addon)
						{
								if($key != 'pro')
									echo "<li><input type='checkbox' name='addons[{$key}]' value='{$key}' id='{$key}' checked='checked'> <label for='{$key}'>{$_POST[addonNames][$key]}</label></li>";
						}
						echo "</ul>";
						echo "</div>";
					}
					else
					{
						//echo "<p>No add-ons were found under this Ktools.net account. You can purchase and install add-ons at any time in the future.</p>";	
					}
				?>
				<p class="buttonRow"><input type="button" value="&laquo; Back" id="formBackButton"><input type="submit" value="Continue &raquo;" id="formSubmitButton" ></p>
			</div>
			<?php require_once BASE_PATH.'/footer.php'; ?>
		</div>
	</div>
	</form>
</body>
</html>

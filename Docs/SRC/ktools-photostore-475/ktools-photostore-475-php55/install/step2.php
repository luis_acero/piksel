<?php
	define('BASE_PATH',dirname(__FILE__)); // Define the base path
	
	require_once BASE_PATH.'/session.php';
	require_once BASE_PATH.'/config.php';
	require_once BASE_PATH.'/lang.php';
	require_once BASE_PATH.'/functions.php';
	$inc = 1;
	require_once BASE_PATH.'/version.php';
	
	$baseURL = curPageURL(); // Current URL
	$baseURL = str_replace('/install','',$baseURL);
	//echo $baseURL; exit;
	
	$activationKey = createUniqueKey(); // Create an activation key
	
	if($_GET['fatalError']) // Convert to local if fatal error is passed
		$instError = $_GET['fatalError'];
?>
<!DOCTYPE html>
<html>
<head>
	<?php require_once BASE_PATH.'/head.php'; ?>
	<script type="text/javascript" language="javascript">
		$(function()
		{
			// Go back to the previous page
			$('#formBackButton').click(function()
			{
				goto('step1.php');
			});
			
			// Launch support window
			$('.helpIcon').click(function()
			{
				supportPopup(0);
			});
		});
	</script>
</head>
<body>
	<form action="http://www.ktools.net/webmgr/activation.php" name="dataform" id="dataform" method="post">
	<input type="hidden" name="productName" value="<?php echo $config['productName']; ?>">
	<input type="hidden" name="productVersion" value="<?php echo $config['productVersion']; ?>">
	<input type="hidden" name="productType" value="<?php echo $config['productType']; ?>">
	<input type="hidden" name="productCode" value="<?php echo $config['productCode']; ?>">
	<input type="hidden" name="key" value="<?php echo $activationKey; ?>">
	<input type="hidden" name="baseURL" value="<?php echo $baseURL; ?>">
	<input type="hidden" name="ip" value="<?php echo $_SERVER['SERVER_ADDR']; ?>">
	<input type="hidden" name="proInstall" value="<?php echo $_SESSION['proInstall']; ?>">
	<div id="container">
		<div id="installerBox">
			<p class="headerIcons">
				<a href="phpinfo.php" target="_blank"><img src="./images/php.logo.png" class="prodLogo opac40" title="PHP Info"></a>
				<a href="http://www.ktools.net/photostore/" target="_blank"><img src="./images/prod.logo.png" class="prodLogo opac40" title="Ktools.net PhotoStore"></a>
			</p>
			<h1 class="stepOff" style="margin-left: 20px; border-left: none;">Server Check</h1>
			<h1 class="stepOn">Activation</h1>
			<h1 class="stepOff">Add-ons</h1>
			<h1 class="stepOff">Install</h1>
			<h1 class="stepOff">Ioncube</h1>
			<h1 class="stepOff">Database</h1>
			<div id="content">
				<p>Please enter your PhotoStore serial number below and click the <strong>activate</strong> button.</p>
				<?php
					if($instError == 'invalid')
					{
						echo "<div id='fatalErrorsBox'>";
						echo "<h2 class='checkFailed'>Serial number invalid. Please try again.</h2>";
						echo "</div>";
					}
					
					if($instError == 'keys')
					{
						echo "<div id='fatalErrorsBox'>";
						echo "<h2 class='checkFailed'>The keys do not match. Please try again.</h2>";
						echo "</div>";
					}
				?>
				
				<div class="divTable tableForm">
					<div class="divTableRow" <?php if($instError){ echo "style='background-color: #fce9e9'"; } ?>>
						<div class="divTableCell formQuestion"><img src="./images/question.png" class="helpIcon"></div>
						<div class="divTableCell formLabel">Serial Number:<br><span class="infoTag">Found in your Ktools.net account under order history.</span></div>
						<div class="divTableCell formFieldCell"><input type="text" value="" name="serialNumber" style="width: 200px;"></div>
					</div>
				</div>
				<p class="buttonRow"><input type="button" value="&laquo; Back" id="formBackButton"><input type="submit" value="Activate" id="formSubmitButton" ></p>
			</div>
			<?php require_once BASE_PATH.'/footer.php'; ?>
		</div>
	</div>
	</form>
</body>
</html>

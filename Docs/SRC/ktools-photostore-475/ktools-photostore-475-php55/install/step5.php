<?php
	define('ROOT_PATH',dirname(dirname(__FILE__))); // Define the root path of install
	define('BASE_PATH',dirname(__FILE__)); // Define the base path of install
	
	require_once BASE_PATH.'/session.php';
	require_once BASE_PATH.'/config.php';
	require_once BASE_PATH.'/lang.php';
	require_once BASE_PATH.'/functions.php';
	$inc = 1;
	require_once BASE_PATH.'/version.php';
	
	//$lictxt = ioncube_license_properties(); 
	//$lictxt = array($lictxt);
	
	if(!$_SESSION['activationInfo'])
		die('No activation information is present in the session.'); // Make sure the activation session is present
	
	//$fatalError = array();
	
	# DOWNLOAD THE CORRECT LOADER
	/*	
	if($_GET['getloader']==1)
	{
		if(ini_get("allow_url_fopen"))
		{
			if(is_writable("ioncube"))
			{
				if(!copy("http://www.ktools.net/webmgr/loaders/".$sf.find_ic_loader(),"ioncube/".find_ic_loader()))
				{					
					echo "We could not automatically get the loader. You will need to download it from <a href='$ktools_path'>$ktools_path</a> and upload it to the ioncube directory on your site.";
					exit;
				}
				else
				{
					echo "The loaders have been downloaded and installed successfully.<br>";
					if($upgrade != 1 && $_SESSION['loadit'] != 1)
					{
						echo "<br>Retesting... Please Wait...<br>";
						unset($_SESSION['loadit']);
						?>
						<SCRIPT LANGUAGE="JavaScript">
						if (navigator.javaEnabled()) 
						window.location = "install.php";
						Else 
						window.location = "install.php";
						</script>
					<?PHP
					} 
					else
					{
						echo "If this is an upgrade, you may now delete the install.php and the upgrade file from your site as it is no longer needed.<br>";
					}
					exit;
				}
			}
			else
			{
					echo "The ioncube directory isn't writable. You will need to make this directory writable before proceeding. <a href='".$pagename."?getloader=1'>Try again</a>";
					exit;
			}
		}
		else
		{
			echo "We are unable to automatically download the loader. You will need to download it from <a href='$ktools_path'>$ktools_path</a> and upload it to the ioncube directory on your site.<br>You can also download any loaders you need directly from ioncube website here: <a href=\"http://www.ioncube.com/loaders.php\">Ioncube Loaders</a>";
			exit;
		}
	}
	*/
	
	ob_start();
	phpinfo(INFO_GENERAL);
	$php_info = ob_get_contents();
	ob_end_clean();
	foreach(preg_split("/\n/",$php_info) as $line)
	{
		if (preg_match('/command/i',$line))
			continue;
		
		if(preg_match('/thread safety.*(enabled|yes)/Ui',$line)) 
			$threadSafe = true;
	}
	
	// Determine specialty loaders
	// Check for x86_64 bit loaders
	$sixfour = php_uname();
	$sixfour = substr_count($sixfour,"x86_64");
	if($sixfour > 0)
		$sf = "x86_64/";
	else
		$sf = "";
	
	# Find the correct loader for IC
	function find_ic_loader()
	{
		global $threadSafe;
		$_u = php_uname();
		$_os = substr($_u,0,strpos($_u,' '));
		$_os_key = strtolower(substr($_u,0,3));
		$_php_version = phpversion();
		$_php_family = substr($_php_version,0,3);
		$ts = ((($_os_key != 'win') && $thread_safe) ? '_ts' : '');
		$_loader_sfix = (($_os_key == 'win') ? '.dll' : '.so');
		$_ln_new="ioncube_loader_${_os_key}_${_php_family}${ts}${_loader_sfix}";
		return $_ln_new;
	}
	$uri = explode("/",$_SERVER['REQUEST_URI']);
	$pagename = array_pop($uri);
	
	// Check to see if the license file exists
	if(!file_exists(ROOT_PATH."/ps4.lic"))
	{	
		$noLicenseFileError = "No license file found. Please download your license file by clicking following <strong>Download License</strong> button. Once downloaded please upload the ps4.lic to the directory where you are installing PhotoStore and then click <strong>Recheck</strong><br><input type='button' value='Download License' id='downloadLicenseFile' style='margin-left: 0; margin-top: 5px;' />"; 
		if(ini_get("allow_url_fopen"))
		{
			if(@!copy("http://www.ktools.net/webmgr/create.license.php?serialNumber={$_SESSION[activationInfo][serialNumber]}",ROOT_PATH.'/ps4.lic'))
			{
				$instError[] = $noLicenseFileError; // Can't download from Ktools servers
			}
		}
		else
			$instError[] = $noLicenseFileError; // No license file exists and cannot grab it from Ktools server
	}	
	
	if(!is_dir(ROOT_PATH."/assets/ioncube"))
	{
		$instError[] = 'Ioncube directory missing. Cannot continue installation.'; 
	}
	
	if(!file_exists(ROOT_PATH."/assets/ioncube/ioncube-encoded-file.php"))
	{
		$instError[] = 'ioncube-encoded-file.php file is missing. Cannot continue installation.'; 
	}
	else
	{
		/* OLD Ioncube check
		ob_start();
		$success = include(ROOT_PATH."/assets/ioncube/ioncube-encoded-file.php");
		$php_info = ob_get_contents();
		ob_end_clean();
		*/
		
		@$extensions = get_loaded_extensions();
		
		if(@in_array('ionCube Loader', $extensions))
			$success = 1;
		else
			$success = 0;
	}
	
	if($success)
	{
		// Loading worked - all set to continue
	}
	else
	{	
		$noLoaderError = 'Ioncube loader file not present. Please download the loader file by clicking <strong>Download Ioncube Loader</strong>. Then upload the file to assets/ioncube/ and click <strong>Recheck</strong>. <input type="button" value="Download Ioncube Loader" id="downloadLoader">';
		if(!file_exists(ROOT_PATH."/assets/ioncube/".find_ic_loader()))
		{
			$ktoolsLoaderPath = "http://www.ktools.net/webmgr/loaders/".$sf.find_ic_loader();
			
			// Attempt to automatically grab license file
			if(ini_get("allow_url_fopen"))
			{
				if(@copy("http://www.ktools.net/webmgr/loaders/".$sf.find_ic_loader(),ROOT_PATH."/assets/ioncube/".find_ic_loader()))
				{
					header("location: step5.php?loaderDownloaded");
					exit;
				}
				else
					$instError[] = $noLoaderError;
			}
			else
				$instError[] = $noLoaderError;
		}
		else // Loader exists
		{	
			$ic_php_ini = "zend_extension".$ts."=\"".dirname(dirname(__FILE__))."/assets/ioncube/".find_ic_loader()."\"";
			$ic_php_ini = str_replace("/",DIRECTORY_SEPARATOR,$ic_php_ini);
			
			if(file_exists(ROOT_PATH.'/php.ini'))
			{	
				$instError[] = 'The ioncube loader is present but it is not decoding files. It appears that you are using a local php.ini file to adjust your php settings. If this is the case then 
				you may need to add additional code to that php.ini to make the loader work. Please try adding the following code to any local php.ini files. Make sure you have a php.ini file 
				in the directory where you are installing PhotoStore and also the manager directory. You can check if this worked by using the <strong>Recheck</strong> button after adding the code.<br><br>'
				.$ic_php_ini.'<br><br>If you do not understand how to add this code then please login to your Ktools.net account and open a support ticket and our support team can assist you.';
			}
			else
				$instError[] = 'The ioncube loader is present but is not decoding files. We are unable to determine the cause of the issue. Try creating a php.ini file and add the 
				following code to it. Upload your php.ini file to the directory where you are installing PhotoStore and also the manager directory. Then click <strong>Recheck</strong> to see if it 
				resolves the issue.<br><br>'
				.$ic_php_ini.'<br><br>If you do not understand how to add this code then please login to your Ktools.net account and open a support ticket and our support team can assist you.';;
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?php require_once BASE_PATH.'/head.php'; ?>
	<script type="text/javascript" language="javascript">
		$(function()
		{
			// Disable submit button if fatalErrors are found
			if($('#fatalError').val() == 1) 
				$('#formSubmitButton').attr('disabled','disabled');
			
			// Reload the page
			$('#formRecheckButton').click(function()
			{
				goto('step5.php');
			});
			
			// Go back to the previous page
			$('#formBackButton').click(function()
			{
				goto('step4.php');
			});
			
			// Download license file
			$('#downloadLicenseFile').click(function()
			{
				goto('http://www.ktools.net/webmgr/create.license.php?serialNumber=<?php echo $_SESSION['activationInfo']['serialNumber']; ?>');
				$(this).attr('disabled','disabled');
			});
			
			// Launch support window
			$('.helpIcon').click(function()
			{
				supportPopup(0);
			});
			
			// Launch support window
			$('#downloadLoader').click(function()
			{
				goto('<?php echo "http://www.ktools.net/webmgr/loaders/".$sf.find_ic_loader(); ?>');
			});
			
		});
	</script>
</head>
<body>
	<form action="step6.php" method="post">
	<div id="container">
		<div id="installerBox">
			<p class="headerIcons">
				<a href="phpinfo.php" target="_blank"><img src="./images/php.logo.png" class="prodLogo opac40" title="PHP Info"></a>
				<a href="http://www.ktools.net/photostore/" target="_blank"><img src="./images/prod.logo.png" class="prodLogo opac40" title="Ktools.net PhotoStore"></a>
			</p>
			<h1 class="stepOff" style="margin-left: 20px; border-left: none;">Server Check</h1>
			<h1 class="stepOff">Activation</h1>
			<h1 class="stepOff">Add-ons</h1>
			<h1 class="stepOff">Install</h1>
			<h1 class="stepOn">Ioncube</h1>
			<h1 class="stepOff">Database</h1>
			<div id="content">
				<?php
					if(!$instError and !$warning)
					{
						echo "<div class='goodInfoBox'>";
						echo "<h2 class='checkFailed'>Ioncube Working</h2>";
							echo "<div style='padding: 15px;'>";
								echo "Ioncube is installed and working properly and we have found a valid license file (ps4.lic). You may now continue.";
							echo "</div>";
						echo "</div>";
					}
				
					if($instError)
					{
						echo "<div id='fatalErrorsBox'>";
						echo "<h2 class='checkFailed'>Fatal Errors</h2>";
						echo "<ul class='fatalErrors'>";
						foreach($instError as $key => $error)
						{
							echo "<li>{$error}</li>";	
						}
						echo "</ul>";
						echo "</div>";
					}
					
					if($warning)
					{
						echo "<div id='warningsBox'>";
						echo "<h2 class='checkLow'>Warnings</h2>";
						echo "<ul class='warnings'>";
						foreach($warning as $key => $warn)
						{
							echo "<li>{$warn}</li>";	
						}
						echo "</ul>";
						echo "</div>";
					}
				?>
				<p class="buttonRow"><input type="button" value="&laquo; Back" id="formBackButton"><input type="button" value="Recheck" id="formRecheckButton" <?php if(!$warning and !$instError){ echo "disabled='disabled'"; } ?>><input type="submit" value="Continue &raquo;" id="formSubmitButton" ></p>
			</div>
			<?php require_once BASE_PATH.'/footer.php'; ?>
		</div>
	</div>
	<input type="hidden" id="fatalError" name="fatalError" value="<?php if($warning or $instError){ echo "1"; } else { echo "0"; } ?>">
	</form>
</body>
</html>

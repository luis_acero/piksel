<?php
	// Current page URL
	function curPageURL()
	{
		$pageURL = 'http';
		if ($_SERVER["HTTPS"] == "on") $pageURL .= "s";
		$pageURL .= "://";
		if($_SERVER['REQUEST_URI']){
			$directories = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/'));
		} else {
			$directories = substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
		}
		$pageURL .= $_SERVER["SERVER_NAME"].$directories; //$_SERVER["REQUEST_URI"]
		$pageURL =str_replace($_SERVER['PHP_SELF'],'',$pageURL);
		return $pageURL;
	}
	
	// Create a unique key
	function createUniqueKey(){
		return strtoupper(md5(uniqid(rand(),1)));
	}
	
	/*
	// Encryption function
	function k_encrypt($value,$serialNumber){
		return base64_encode(strrev($serialNumber.$value));	
	}
	
	// Description function
	function k_decrypt($value,$serialNumber){
		global $config;
		return str_replace($serialNumber,"",strrev(base64_decode($value)));	
	}
	*/
	
	// Encryption function
	function k_encrypt($value,$serialNumber){
		global $config;
		return base64_encode(strrev(substr(md5($serialNumber),0,10) . $value));	
	}
	
	// Description function
	function k_decrypt($value,$serialNumber){
		global $config;
		return str_replace(substr(md5($serialNumber),0,10),"",strrev(base64_decode($value)));	
	}
	
?>
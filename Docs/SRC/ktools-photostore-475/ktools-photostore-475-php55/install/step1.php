<?php
	define('BASE_PATH',dirname(__FILE__)); // Define the base path
	
	require_once BASE_PATH.'/session.php';
	require_once BASE_PATH.'/config.php';
	require_once BASE_PATH.'/lang.php';
	require_once BASE_PATH.'/functions.php';
	$inc = 1;
	require_once BASE_PATH.'/version.php';

	// Try to find ioncube support
	/*
	ob_start();
	phpinfo();
	$phpInfoContent = ob_get_contents();
	ob_get_clean();
	*/	
	@$extensions = get_loaded_extensions();
		
	if(@in_array('ionCube Loader', $extensions))
		$ioncubeSupport = 1;
	else
		$ioncubeSupport = 0;
		
	/* More Ioncube checks
	
		$isCorrectVersion = true;
		$ioncubeVersion = '';
		if (function_exists('ioncube_loader_version')) {
			$ioncubeVersion = ioncube_loader_version();
			$ioncubeMajorVersion = (int)substr($ioncubeVersion, 0, strpos($ioncubeVersion, '.'));
			$ioncubeMinorVersion = (int)substr($ioncubeVersion, strpos($ioncubeVersion, '.')+1);
			if ($ioncubeMajorVersion < 4 || ($ioncubeMajorVersion == 4 && $ioncubeMinorVersion < 3)) {
				$isCorrectVersion = false;
				echo 'ionCube Loader '.$ioncubeVersion.' - old, required is a minimum of ionCube Loader version 4.3<br>';
			}
		}
		if ($isCorrectVersion) {
			if (is_int(PHP_MAJOR_VERSION) && is_int(PHP_MINOR_VERSION) && PHP_MAJOR_VERSION == 5 && PHP_MINOR_VERSION >= 3) {
				echo "<b>ionCube Loader</b>".($ioncubeVersion != '' ? " (v$ioncubeVersion)" : '' )." - use product ZIP ending with '<b>_ionEncoded53.zip</b>' (PHP 5.3.0 and above)<br>";
			} else {
				echo "<b>ionCube Loader</b>".($ioncubeVersion != '' ? " (v$ioncubeVersion)" : '' )." - use product ZIP ending with '<b>_ionEncoded52.zip</b>' (All PHP 5.2.X versions)<br>";
			}
		}
	*/
	
	if(ini_get("allow_url_fopen"))
	{
		$timeout = 3;
		$old = ini_set('default_socket_timeout', $timeout);
		$dataFile = fopen('http://www.ktools.net/webmgr/push.updatecheck.php?product='.$config['productCode'].'&version='.$config['productVersion'].'&builddate='.$config['productBuildDate'], 'r');
		ini_set('default_socket_timeout', $old);
		stream_set_timeout($dataFile, $timeout);
		stream_set_blocking($dataFile, 0);
		
		if ($dataFile)
		{
			//while (!feof($dataFile)){
			$currentVersion = fgets($dataFile, 4096);
			//}		
			fclose($dataFile);
			$versionCheck = true;
		}
		else
			$versionCheck = false;
	}
	else
		$versionCheck = false;
?>
<!DOCTYPE html>
<html>
<head>
	<?php require_once BASE_PATH.'/head.php'; ?>
	<script type="text/javascript" language="javascript">
		$(function()
		{
			// Initially hide the results
			$('.tableForm .divTableRow').hide();
			$('#warningsBox').hide();
			$('#fatalErrorsBox').hide();
			
			var timeout = <?php if($_SESSION['proInstall']){ echo 20; } else { echo 500; } ?>;
			
			// Show each row 1 at a time
			$('.tableForm .divTableRow').each(function(i)
			{
				$(this).delay(timeout*i).fadeIn('slow');
			});
			
			// Set the timeout on the warnings and fatal error boxes so that they appear after the checks
			setTimeout(function()
			{
				$('#warningsBox').fadeIn(2000);
				setTimeout(function(){ $('#fatalErrorsBox').fadeIn(2000); },timeout);
			},$('.tableForm .divTableRow').size()*timeout);
			
			// Disable submit button if fatalErrors are found
			if($('#fatalError').val() == 1) 
				$('#formSubmitButton').attr('disabled','disabled');
			
			// Reload the page to recheck settings
			$('#formRecheckButton').click(function()
			{
				goto('step1.php');
			});
			
			// Go back to the previous page
			$('#formBackButton').click(function()
			{
				goto('index.php');
			});
			
			// Launch support window
			$('.helpIcon').click(function()
			{
				supportPopup(0);
			});			
		});
	</script>
</head>
<body>
	<form action="step2.php" method="post" id="step1Form">
	<div id="container">
		<div id="installerBox">
			<p class="headerIcons">
				<a href="phpinfo.php" target="_blank"><img src="./images/php.logo.png" class="prodLogo opac40" title="PHP Info"></a>
				<a href="http://www.ktools.net/photostore/" target="_blank"><img src="./images/prod.logo.png" class="prodLogo opac40" title="Ktools.net PhotoStore"></a>
			</p>
			<h1 class="stepOn" style="margin-left: 20px; border-left: none;">Server Check</h1>
			<h1 class="stepOff">Activation</h1>
			<h1 class="stepOff">Add-ons</h1>
			<h1 class="stepOff">Install</h1>
			<h1 class="stepOff">Ioncube</h1>
			<h1 class="stepOff">Database</h1>
			<div id="content">
				<p>The installer will now check your server to make sure it meets the requirements for PhotoStore. Please see the report below.</p>	
				<div class="divTable tableForm serverInfo">
					<?php
						if($versionCheck) // Make sure version check was possible
						{
					?>
						<div class="divTableRow">
							<div class="divTableCell"><img src="./images/question.png" class="helpIcon">PhotoStore Version</div>
							<?php
								if($config['productVersion'] >= (int)$currentVersion)
								{
									echo "<div class='divTableCell'>{$config[productVersion]}</div>";
									echo "<div class='divTableCell'><p class='checkOK'>OK</p></div>";
								}
								else
								{
									$warning[] = 'There is a newer version of PhotoStore available. We suggest you download the newest version from <strong>Version History/Upgrades</strong> in your Ktools.net account.';
									echo "<div class='divTableCell'>{$config[productVersion]}</div>";
									echo "<div class='divTableCell'><p class='checkLow'>OLD</p></div>";
								}
							?>
						</div>
					<?php
						}
						$phpVer = phpversion();
					?>
					<div class="divTableRow">
						<div class="divTableCell"><img src="./images/question.png" class="helpIcon">PHP Version</div>
						<?php
							if(phpversion() >= 5.2)
							{
								if($phpVer >= '5.2' and $phpVer <= '5.4.9')
								{
									if($config['ioncubeVersion'] != 'php52')
									{	
										$instError[] = 'You are using the wrong installer. Please use the PhotoStore installer for <strong>PHP 5.2</strong>.';
										echo "<div class='divTableCell'>".phpversion()."</div>";
										echo "<div class='divTableCell'><p class='checkFailed'>FAILED</p></div>";
									}
									else
									{
										echo "<div class='divTableCell'>".phpversion()."</div>";
										echo "<div class='divTableCell'><p class='checkOK'>OK</p></div>";
									}
								}
								if($phpVer >= '5.5')
								{
									if($config['ioncubeVersion'] != 'php55')
									{
										$instError[] = 'You are using the wrong installer. Please use the PhotoStore installer for <strong>PHP 5.5</strong>.';
										echo "<div class='divTableCell'>".phpversion()."</div>";
										echo "<div class='divTableCell'><p class='checkFailed'>FAILED</p></div>";
									}
									else
									{
										echo "<div class='divTableCell'>".phpversion()."</div>";
										echo "<div class='divTableCell'><p class='checkOK'>OK</p></div>";
									}
								}
							}
							else
							{
								$instError[] = 'You must have at least PHP version 5.2 to install PhotoStore.';
								echo "<div class='divTableCell'>".phpversion()."</div>";
								echo "<div class='divTableCell'><p class='checkFailed'>FAILED</p></div>";
							}
						?>
					</div>
					<div class="divTableRow">
						<div class="divTableCell"><img src="./images/question.png" class="helpIcon">GD Library</div>
						<?php
							if(function_exists('imagecreatetruecolor'))
							{
								echo "<div class='divTableCell'>Installed</div>";
								echo "<div class='divTableCell'><p class='checkOK'>OK</p></div>";
							}
							else
							{
								$instError[] = 'GD Library must be installed to use PhotoStore.';
								echo "<div class='divTableCell'>Not Installed</div>";
								echo "<div class='divTableCell><p class='checkFailed'>FAILED</p></div>";
							}
						?>
					</div>
					<div class="divTableRow">
						<div class="divTableCell"><img src="./images/question.png" class="helpIcon">Ioncube</div>
						<?php
							if($ioncubeSupport)
							{
								echo "<div class='divTableCell'>Installed</div>";
								echo "<div class='divTableCell'><p class='checkOK'>OK</p></div>";
							}
							else
							{
								$warning[] = 'PhotoStore requires Ioncube to function properly. You can continue but if you get a white page on step 5 of the installer this is because there is no Ioncube support.';
								echo "<div class='divTableCell'>Not Found</div>";
								echo "<div class='divTableCell'><p class='checkLow'>MISSING</p></div>";
							}
						?>
					</div>
					<div class="divTableRow">
						<div class="divTableCell"><img src="./images/question.png" class="helpIcon">Memory Limit</div>
						<?php
							$memoryLimit = str_replace('M','',ini_get("memory_limit"));							
							if($memoryLimit > "63" or !ini_get("memory_limit"))
							{
								if(ini_get("memory_limit"))
									echo "<div class='divTableCell'>".ini_get("memory_limit")."</div>";
								else
									echo "<div class='divTableCell'>None</div>";
								echo "<div class='divTableCell'><p class='checkOK'>OK</p></div>";
							}
							else
							{
								$warning[] = 'The memory limit your server has in place will restrict the size of photos PhotoStore can work with. Please contact your host to have them raise this limit. We recommend 64MB or higher.';
								echo "<div class='divTableCell'>".ini_get("memory_limit")."</div>";
								echo "<div class='divTableCell'><p class='checkLow'>LOW</p></div>";
							}
						?>
					</div>
					<div class="divTableRow">
						<div class="divTableCell"><img src="./images/question.png" class="helpIcon">Max Execution Time</div>
						<?php
							echo "<div class='divTableCell'>".ini_get("max_execution_time")."</div>";
							if(ini_get("max_execution_time") > 29 or ini_get("max_execution_time") == '-1')
								echo "<div class='divTableCell'><p class='checkOK'>OK</p></div>";
							else
							{
								$warning[] = 'Max execution time could be higher. We recommend 30 or more but PhotoStore will still work with your current settings.';
								echo "<div class='divTableCell'><p class='checkLow'>LOW</p></div>";
							}
						?>
					</div>
					<div class="divTableRow">
						<div class="divTableCell"><img src="./images/question.png" class="helpIcon">Max Input Time</div>
						<?php
							echo "<div class='divTableCell'>".ini_get("max_input_time")."</div>";
							if(ini_get("max_input_time") > 90 or ini_get("max_input_time") == "-1")
								echo "<div class='divTableCell'><p class='checkOK'>OK</p></div>";
							else
							{
								$warning[] = 'Max input time could be higher. We recommend 90 or more but PhotoStore will still work with your current settings.';
								echo "<div class='divTableCell'><p class='checkLow'>LOW</p></div>";
							}
						?>
					</div>
					<div class="divTableRow">
						<div class="divTableCell"><img src="./images/question.png" class="helpIcon">Max Upload Size</div>
						<?php
							echo "<div class='divTableCell'>".ini_get("upload_max_filesize")."</div>";
							if(ini_get("upload_max_filesize") > 9)
								echo "<div class='divTableCell'><p class='checkOK'>OK</p></div>";
							else
							{
								$warning[] = 'Max upload size could be higher. We recommend 10MB or more but PhotoStore will still work with your current settings.';
								echo "<div class='divTableCell'><p class='checkLow'>LOW</p></div>";
							}
						?>
					</div>
					<div class="divTableRow">
						<div class="divTableCell"><img src="./images/question.png" class="helpIcon">Max Post Size</div>
						<?php
							echo "<div class='divTableCell'>".ini_get("post_max_size")."</div>";
							if(ini_get("post_max_size") > 10)
								echo "<div class='divTableCell'><p class='checkOK'>OK</p></div>";
							else
							{
								$warning[] = 'Max post size could be higher. We recommend 11MB or more but PhotoStore will still work with your current settings.';
								echo "<div class='divTableCell'><p class='checkLow'>LOW</p></div>";
							}
						?>
					</div>
					<div class="divTableRow">
						<div class="divTableCell"><img src="./images/question.png" class="helpIcon">Safe Mode</div>
						<?php
							if(!ini_get("safe_mode") or strtolower(ini_get("safe_mode")) == "off")
							{
								echo "<div class='divTableCell'>Off</div>";
								echo "<div class='divTableCell'><p class='checkOK'>OK</p></div>";
							}
							else
							{
								$instError[] = 'Safe mode is on. Safe mode must be OFF to use PhotoStore.';
								echo "<div class='divTableCell'>On</div>";
								echo "<div class='divTableCell'><p class='checkFailed'>FAILED</p></div>";
							}
						?>
					</div>
					<?php
						/*
					<div class="divTableRow">
						<div class="divTableCell"><img src="./images/question.png" class="helpIcon">Open Basedir</div>
						<?php
							if(ini_get("open_basedir") != "")
							{	
								$warning[] = 'There seems to be an open basedir restriction in place for PHP. In most cases this is fine but could cause issues if not configured properly by your host.';
								
								$openBaseDir = explode(':',ini_get("open_basedir"));
								
								echo "<div class='divTableCell' style='word-wrap: break-word;'>".implode('<br>',$openBaseDir)."</div>";
								echo "<div class='divTableCell'><p class='checkLow'>WARN</p></div>";
							}
							else
							{
								echo "<div class='divTableCell'>None</div>";
								echo "<div class='divTableCell'><p class='checkOK'>OK</p></div>";
							}
						?>
					</div>
						*/
					?>
				</div>
				
				<?php
					if($warning)
					{
						echo "<div id='warningsBox'>";
						echo "<h2 class='checkLow'>Warnings</h2>";
						echo "<ul class='warnings'>";
						foreach($warning as $key => $warn)
						{
							echo "<li>{$warn}</li>";	
						}
						echo "</ul>";
						echo "</div>";
					}
				?>
				
				<?php
					if($instError)
					{
						echo "<div id='fatalErrorsBox'>";
						echo "<h2 class='checkFailed'>Fatal Errors</h2>";
						echo "<ul class='fatalErrors'>";
						foreach($instError as $key => $error)
						{
							echo "<li>{$error}</li>";	
						}
						echo "</ul>";
						echo "</div>";
					}
				?>
				
				<p class="buttonRow"><input type="button" value="&laquo; Back" id="formBackButton"><input type="button" value="Recheck" id="formRecheckButton"><input type="submit" value="Continue &raquo;" id="formSubmitButton" ></p>
			</div>
			<?php require_once BASE_PATH.'/footer.php'; ?>
		</div>
	</div>
	<input type="hidden" id="fatalError" name="fatalError" value="<?php if($instError){ echo "1"; } else { echo "0"; } ?>">
</form>
</body>
</html>

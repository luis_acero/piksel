<?php
	define('BASE_PATH',dirname(__FILE__)); // Define the base path
	
	require_once BASE_PATH.'/session.php';
	require_once BASE_PATH.'/config.php';
	require_once BASE_PATH.'/lang.php';
	require_once BASE_PATH.'/functions.php';
	require_once BASE_PATH.'/zip.php';
	$inc = 1;
	require_once BASE_PATH.'/version.php';
	
	if(!$_SESSION['activationInfo'])
		die('No activation information is present in the session.'); // Make sure the activation session is present
	
	if($_POST)
		$_SESSION['activationInfo']['addons'] = $_POST['addons'];
	
	$writableDirectories[] = 'addons';
	$writableDirectories[] = 'avatars';
	$writableDirectories[] = 'backups';
	$writableDirectories[] = 'cache';
	$writableDirectories[] = 'contributors';
	$writableDirectories[] = 'incoming';
	$writableDirectories[] = 'item_photos';	
	$writableDirectories[] = 'library';
	$writableDirectories[] = 'logos';
	$writableDirectories[] = 'files';
	$writableDirectories[] = 'tmp';
	$writableDirectories[] = 'watermarks';
	
	$rootWritable = is_writable('../../');

	// Get the zip from Ktools
	if($_GET['fetchZIP'])
	{
		if(ini_get("allow_url_fopen"))
		{
			if(!copy("http://www.ktools.net/webmgr/get.ps.php?serialNumber={$_SESSION[activationInfo][serialNumber]}", "../photostore.zip"))
				die('Could not automatically get the photostore.zip');
		}
		else
			die('No allow_url_fopen');
	}
	
	if(file_exists('../assets/includes/version.php'))
	{
		// Product already unzipped
		$unzipped = true;
		
		// Check for writable directories
		foreach($writableDirectories as $key => $dir)
		{
			$dirFull = "../assets/{$dir}";
			//if(!is_writable($dirFull)) // Not writable - try to make writable - removed this to make sure it sets it to 777 regardless
				@chmod($dirFull,0777);
		}
		
		// Create the incoming/ftp directory
		if(file_exists('../assets/incoming') and is_writable('../assets/incoming'))
		{
			//@mkdir('../assets/incoming/ftp',0777);
			//@chmod('../assets/incoming/ftp',0777);
		}
		
		// Create the add-ons
		if(file_exists('../assets/addons') and is_writable('../assets/addons'))
		{
			if($_SESSION['activationInfo']['addons'])
			{
				foreach($_SESSION['activationInfo']['addons'] as $key => $addon)
				{
					$addonFile = "../assets/addons/{$key}.addon";
					$fileHandle = fopen($addonFile, 'w') or die("Can't create add-on file.");
					fclose($fileHandle);
				}
			}
		}
	}
	else
	{
		if(file_exists('../photostore.zip'))
		{
			$zipExists = true;
		}
		else
		{
			// Please upload zip file
			$zipExists = false;
			
			
			
			$fatalErrorText = "The file <strong>photostore.zip</strong> cannot be found. Make sure you have uploaded it to the directory where you are installing PhotoStore. Once uploaded click <strong>Recheck</strong>.";
			if($_SESSION['proInstall'])
				$fatalErrorText.= "<br><br><input type='button' id='fetchZIP' value='Fetch photostore.zip from Ktools server'>";
			
			$instError[] = $fatalErrorText;
		}	
	}
	
	
?>
<!DOCTYPE html>
<html>
<head>
	<?php require_once BASE_PATH.'/head.php'; ?>
	<script type="text/javascript" language="javascript">
		$(function()
		{
			// Initially hide the results
			$('.tableForm .divTableRow').hide();
			$('#warningsBox').hide();
			$('#fatalErrorsBox').hide();
			
			// Go back to the previous page
			$('#formBackButton').click(function()
			{
				goto('step3.php');
			});
			
			// Reload the page
			$('#formRecheckButton').click(function()
			{
				goto('step4.php');
			});
			
			// Disable submit button if fatalErrors are found
			if($('#fatalError').val() == 1) 
				$('#formSubmitButton').attr('disabled','disabled');
			
			<?php
				if($zipExists and $rootWritable) // Disable the continue button until extraction has completed
				{
			?>
				$('#formSubmitButton').attr('disabled','disabled');
				$('.tableForm').fadeIn('slow');
				
				setTimeout(function()
				{	
					$.ajax({
						type: 'POST',
						url: 'unzipper.php',
						data: "name=John&location=Boston",
						dataType: 'json',
						success: function(data)
						{
							if(data.errorCode == '1')
							{
								$('#formRecheckButton').removeAttr('disabled');							
								$('.tableForm').hide();
								$('#unzipError').fadeIn('slow');
								$('#unzipErrorList').append('<li>'+data.errorMessage+'</li>');
							}
							else
							{
								var numberOfItems = data.numberOfItems;
								
								//alert(data.numberOfItems);
								
								$(data.files).each(function(key,file)
								{	
									setTimeout(function()
									{
										if(key == (numberOfItems-1))
										{
											setTimeout(function()
											{
												//$('.tableForm').hide();
												//$('.goodInfoBox').fadeIn('slow');
												goto('step4.php');
											},700);
										}
										
										var percentage = Math.round((key/(numberOfItems-1))*100);
										$('#progressBar').css('width',percentage+'%');
										$('#progressPercent').text(percentage+'%');
										$('#unzipOutput').html(file);
									},key*<?php if($_SESSION['proInstall']){ echo 0; } else { echo 2; } ?>);
								});
							}
						},
						error: function()
						{							
							$('#formRecheckButton').removeAttr('disabled');							
							$('.tableForm').hide();
							$('#unzipError').fadeIn('slow');
						}
					});
				},1000);
				
				/*
				$('#newCommentMessage').html(data.errorMessage);							
				$('#newComment').val('');
				
				loadComments();
											
				switch(data.errorCode)
				{
					default:									
					break;
					case "newCommentFailed":									
					break;
				}
				*/
				
				
			<?php
				}
			?>
				
			var timeout = <?php if($_SESSION['proInstall']){ echo 20; } else { echo 500; } ?>;
			
			// Show each row 1 at a time
			$('.tableForm .divTableRow').each(function(i)
			{
				$(this).delay(timeout*i).fadeIn('slow');
			});
			
			// Set the timeout on the warnings and fatal error boxes so that they appear after the checks
			setTimeout(function()
			{
				$('#warningsBox').fadeIn(2000);
				
				setTimeout(function(){ $('#fatalErrorsBox').fadeIn(2000); },timeout);
			},$('.tableForm .divTableRow').size()*timeout);
			
			setTimeout(function()
			{
				$('#psAddonsBox').fadeIn(2000);
				
			},($('.tableForm .divTableRow').size()-<?php echo count($_SESSION['activationInfo']['addons']); ?>)*timeout);
			
			$('#fetchZIP').click(function()
			{
				$('#fatalErrorsBox').fadeOut('slow');
				goto('step4.php?fetchZIP=1');
			});
			
			// Launch support window
			$('.helpIcon').click(function()
			{
				supportPopup(0);
			});
		});
	</script>
</head>
<body>
	<form action="step5.php" method="post">
	<div id="container">
		<div id="installerBox">
			<p class="headerIcons">
				<a href="phpinfo.php" target="_blank"><img src="./images/php.logo.png" class="prodLogo opac40" title="PHP Info"></a>
				<a href="http://www.ktools.net/photostore/" target="_blank"><img src="./images/prod.logo.png" class="prodLogo opac40" title="Ktools.net PhotoStore"></a>
			</p>
			<h1 class="stepOff" style="margin-left: 20px; border-left: none;">Server Check</h1>
			<h1 class="stepOff">Activation</h1>
			<h1 class="stepOff">Add-ons</h1>
			<h1 class="stepOn">Install</h1>
			<h1 class="stepOff">Ioncube</h1>
			<h1 class="stepOff">Database</h1>
			<div id="content">
				<?php
					$extractionError = "Your server does not support extracting of zip files or there were permissions issues while extracting. You will need to manually unzip the file. First delete 
							<strong>photostore.zip</strong> from the server. Then unzip the file <strong>photostore.zip</strong> on your local computer and upload the contents to the directory on 
							your server where you originally uploaded photostore.zip and the install directory. When all files are uploaded click <strong>Recheck</strong>.";
					
					if($zipExists)
					{
						if($rootWritable)
						{
							echo "<div class='divTable tableForm' style='display: none;'>";
								echo "<div class='divTableRow'>";
									echo "<div class='divTableCell'>";
									echo "<img src='./images/loader.eeeeee.gif' id='extractionLoader' style='vertical-align:middle; margin-top: -2px;' /> Found <strong>photostore.zip</strong>. Extracting...";
									echo "<div id='unzipOutput'></div>";
									echo "<div id='progressBarContainer'><p id='progressPercent'>0%</p><div id='progressBar'></div></div>";
									echo "</div>";
								echo "</div>";
							echo "</div>";
						}
						else
						{
							$instError[] = $extractionError;	
						}
						
						echo "<div class='fatalErrorsBox' id='unzipError' style='display: none;'>";
						echo "<h2 class='checkFailed'>Extraction Failed</h2>";
						echo "<ul class='fatalErrors' id='unzipErrorList'>";
							echo "<li>{$extractionError}</li>";	
						echo "</ul>";
						echo "</div>";
					}
					
					if($unzipped)
					{
						echo "<div class='goodInfoBox'>";
						echo "<h2 class='checkFailed'>Found PhotoStore Files</h2>";
							echo "<div style='padding: 15px;'>";
								echo "Your PhotoStore files have been found on the server. The following directories will be checked for write permissions (CHMOD 777). These directories must all have write
								permissions in order to continue.";
							echo "</div>";
						echo "</div>";
				?>
					<div class="divTable tableForm serverInfo">
						<?php
							foreach($writableDirectories as $key => $dir)
							{
								$dirFull = "assets/{$dir}";
						?>
						<div class="divTableRow">
							<div class="divTableCell"><img src="./images/question.png" class="helpIcon"> <?php echo $dirFull; ?></div>
							<div class="divTableCell">
								<?php
									if(is_writable("../{$dirFull}"))
										echo "<p class='checkOK'>OK</p>";
									else
									{
										echo "<p class='checkLow'>NOT WRITABLE</p>";
										$warning[] = "Please make the directory <strong>{$dirFull}</strong> writable (CHMOD 777).";
									}
								?>
							</div>
						</div>
						<?php
							}
						?>
					</div>
				<?php
						if(!$warning and $_SESSION['activationInfo']['addons'])
						{
							//print_r($_SESSION['activationInfo']['addons']); // Testing
							
							if($_SESSION['activationInfo']['addons'])
							{
								foreach($_SESSION['activationInfo']['addons'] as $addonID) // See if there are other add-ons besides pro
								{
									if($addonID != 'pro' and $addonID != '') $addonToInstall = true;
								}
							}
							
							if($addonToInstall)
							{
								echo "<div class='goodInfoBox' id='psAddonsBox' style='display: none;'>";
								echo "<h2 class='checkFailed'>PhotoStore Add-ons</h2>";
									echo "<div style='padding: 15px;'>";
										echo "PhotoStore add-ons have been found. Installing them now.";
									echo "</div>";
								echo "</div>";
				?>
								<div class="divTable tableForm serverInfo">
									<?php
										foreach($_SESSION['activationInfo']['addons'] as $key => $addon)
										{
											if($key != 'pro')
											{
									?>
										<div class="divTableRow">
											<div class="divTableCell"><img src="./images/question.png" class="helpIcon"> <?php echo $_SESSION['activationInfo']['addonNames'][$key]; ?></div>
											<div class="divTableCell">
												<?php
													if(file_exists("../assets/addons/{$key}.addon"))
														echo "<p class='checkOK'>INSTALLED</p>";
													else
													{
														echo "<p class='checkLow'>NOT INSTALLED</p>";
														$warning[] = "Add-on not installed: ".$_SESSION['activationInfo']['addonNames'][$key];
													}
												?>
											</div>
										</div>
									<?php
											}
										}
									?>
								</div>
				<?php
							}
						}
					}
					
					if($instError)
					{
						echo "<div id='fatalErrorsBox' style='display: none;'>";
						echo "<h2 class='checkFailed'>Fatal Errors</h2>";
						echo "<ul class='fatalErrors'>";
						foreach($instError as $key => $error)
						{
							echo "<li>{$error}</li>";	
						}
						echo "</ul>";
						echo "</div>";
					}
					
					if($warning)
					{
						echo "<div id='warningsBox' style='display: none;'>";
						echo "<h2 class='checkLow'>Warnings</h2>";
						echo "<ul class='warnings'>";
						foreach($warning as $key => $warn)
						{
							echo "<li>{$warn}</li>";	
						}
						echo "</ul>";
						echo "</div>";
					}
				?>
				
				<p>
					<!--
					<br><br><br>Check for photostore.zip<br>
					If photostore.zip exists them try to unzip it<br>
					If it doesn't exist then download from ktools<br>
					If download from ktools fails ask them to unzip photostore.zip and manually upload the files. Then check for a certain file to make sure it was uploaded.<br><br>
					-->
					<?php
						/* Testing
						foreach($_SESSION['activationInfo'] as $key => $info)
							echo "{$key}: {$info}<br>";
						
						echo "<br><strong>Addons</strong><br>";
						foreach($_SESSION['activationInfo']['addons'] as $key => $addon)
							echo "{$key}: {$addon}<br>";
							
						echo "<br>Pro Install: {$_SESSION[proInstall]}";
						*/
					?>
					
				</p>
				<p class="buttonRow"><input type="button" value="&laquo; Back" id="formBackButton"><input type="button" value="Recheck" id="formRecheckButton" <?php if(!$warning and !$instError){ echo "disabled='disabled'"; } ?>><input type="submit" value="Continue &raquo;" id="formSubmitButton" ></p>
			</div>
			<?php require_once BASE_PATH.'/footer.php'; ?>
		</div>
	</div>
	<input type="hidden" id="fatalError" name="fatalError" value="<?php if($warning or $instError){ echo "1"; } else { echo "0"; } ?>">
	</form>
</body>
</html>

-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306

-- Generation Time: Aug 25, 2015 at 06:09 PM
-- Server version: 5.5.10
-- PHP Version: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ps4dev_clean`
--

-- --------------------------------------------------------

--
-- Table structure for table `ps4_activity_log`
--

CREATE TABLE `ps4_activity_log` (
  `log_id` int(7) NOT NULL AUTO_INCREMENT,
  `member_id` int(6) NOT NULL DEFAULT '0',
  `manager` tinyint(1) NOT NULL DEFAULT '0',
  `area` varchar(100) NOT NULL DEFAULT '',
  `log_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `details` text NOT NULL,
  UNIQUE KEY `log_id` (`log_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `ps4_activity_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_admins`
--

CREATE TABLE `ps4_admins` (
  `admin_id` int(4) NOT NULL AUTO_INCREMENT,
  `uadmin_id` varchar(40) NOT NULL DEFAULT '',
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(250) NOT NULL DEFAULT '',
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `superadmin` tinyint(1) NOT NULL DEFAULT '0',
  `permissions` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `wp_left` varchar(250) NOT NULL DEFAULT '',
  `wp_right` varchar(250) NOT NULL DEFAULT '',
  `wizard` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `admin_id` (`admin_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `ps4_admins`
--

INSERT INTO `ps4_admins` VALUES(3, '460B1BDD209A534EF0AF1EVYKHF04EE9', 'admin', 'bmltZGExQUFDN0Q2NzNDODY2OA==', 'info@ktools.net', '2012-04-11 17:06:38', 1, '', 1, 'extras,sitestats', 'quickstats', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ps4_billings`
--

CREATE TABLE `ps4_billings` (
  `bill_id` int(8) NOT NULL AUTO_INCREMENT,
  `ubill_id` varchar(40) NOT NULL,
  `bill_number` int(8) NOT NULL,
  `member_id` int(8) NOT NULL,
  `bill_type` tinyint(1) NOT NULL DEFAULT '1',
  `exchange_rate` decimal(6,4) NOT NULL DEFAULT '1.0000',
  `currency_id` int(3) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `membership` int(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `bill_id` (`bill_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_billings`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_collections`
--

CREATE TABLE `ps4_collections` (
  `coll_id` int(5) NOT NULL AUTO_INCREMENT,
  `ucoll_id` varchar(40) NOT NULL,
  `description` text NOT NULL,
  `sortorder` int(3) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_code` varchar(255) NOT NULL,
  `quantity` varchar(10) NOT NULL,
  `price` varchar(10) NOT NULL DEFAULT '0',
  `credits` int(10) NOT NULL,
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `multiple` tinyint(1) NOT NULL DEFAULT '0',
  `commission` int(3) NOT NULL DEFAULT '0',
  `commission_type` tinyint(1) NOT NULL DEFAULT '1',
  `commission_dollar` float NOT NULL,
  `ptype` varchar(4) NOT NULL DEFAULT 'ps',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `everyone` tinyint(1) NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  `colltype` tinyint(1) NOT NULL DEFAULT '1',
  `homepage` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `item_name_dutch` varchar(255) NOT NULL,
  `description_dutch` text NOT NULL,
  `item_name_english` varchar(255) NOT NULL,
  `description_english` text NOT NULL,
  `item_name_german` varchar(255) NOT NULL,
  `description_german` text NOT NULL,
  `item_name_spanish` varchar(255) NOT NULL,
  `description_spanish` text NOT NULL,
  `item_name_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL,
  UNIQUE KEY `coll_id` (`coll_id`),
  KEY `quantity` (`quantity`,`active`,`deleted`,`everyone`,`featured`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_collections`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_color_palettes`
--

CREATE TABLE `ps4_color_palettes` (
  `cp_id` int(10) NOT NULL AUTO_INCREMENT,
  `media_id` int(10) NOT NULL,
  `hex` varchar(10) NOT NULL,
  `red` int(3) NOT NULL,
  `green` int(3) NOT NULL,
  `blue` int(3) NOT NULL,
  `percentage` decimal(6,6) NOT NULL,
  UNIQUE KEY `cp_id` (`cp_id`),
  KEY `media_id` (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_color_palettes`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_commission`
--

CREATE TABLE `ps4_commission` (
  `com_id` int(8) NOT NULL AUTO_INCREMENT,
  `contr_id` int(8) NOT NULL,
  `oitem_id` int(8) NOT NULL,
  `omedia_id` int(6) NOT NULL DEFAULT '0',
  `odsp_id` int(6) NOT NULL DEFAULT '0',
  `dlitem_id` int(6) NOT NULL DEFAULT '0',
  `com_total` decimal(15,4) NOT NULL,
  `com_credits` int(10) NOT NULL,
  `per_credit_value` decimal(10,4) NOT NULL,
  `comtype` varchar(10) NOT NULL DEFAULT 'cur',
  `item_qty` int(4) NOT NULL DEFAULT '1',
  `order_status` tinyint(1) NOT NULL DEFAULT '0',
  `compay_status` tinyint(1) NOT NULL DEFAULT '0',
  `order_date` datetime NOT NULL,
  `pay_date` datetime NOT NULL,
  `item_percent` int(3) NOT NULL,
  `mem_percent` int(3) NOT NULL,
  `dl_sub_id` int(6) NOT NULL DEFAULT '0',
  `dl_mem_id` int(6) NOT NULL DEFAULT '0',
  UNIQUE KEY `com_id` (`com_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_commission`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_content`
--

CREATE TABLE `ps4_content` (
  `content_id` int(3) NOT NULL AUTO_INCREMENT,
  `content_code` varchar(30) NOT NULL,
  `ca_id` int(3) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL,
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `name_dutch` varchar(255) NOT NULL,
  `content_dutch` text NOT NULL,
  `name_french` varchar(255) NOT NULL,
  `content_french` text NOT NULL,
  `name_german` varchar(255) NOT NULL,
  `content_german` text NOT NULL,
  `name_spanish` varchar(255) NOT NULL,
  `content_spanish` text NOT NULL,
  `name_english` varchar(255) NOT NULL,
  `content_english` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `linked` text NOT NULL,
  UNIQUE KEY `content_id` (`content_id`),
  KEY `ca_id` (`ca_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `ps4_content`
--

INSERT INTO `ps4_content` VALUES(1, 'homeWelcome', 1, 'Homepage Welcome Message', '', '<div><span style="font-weight: bold; font-size: 14pt; ">Welcome</span></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(2, 'termsOfUse', 1, 'Terms Of Use Page Content', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div><div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>', 1, '', 'dsadasds', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(4, '', 3, 'Custom Page 1', '', 'Custom Page Content Example', 0, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(8, 'welcomeEmail', 4, 'Welcome To {$config.settings.site_title}', 'Email that receives sent to a new member when they signup.', '{$member.f_name} {$member.l_name},<div><br></div><div>Welcome to&nbsp;{$config.settings.site_title}. Your account has been created and you can login at any time with the information below.</div><div><br></div><div>{$loginPageURL}</div><div>Email: {$member.email}</div><div>Password: {$member.unencryptedPassword}</div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(60, 'quoteEmailAdmin', 6, 'Quote Requested', 'Email that the sales email address receives when a quote is requested by a visitor or member.', '<span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">{$user}&nbsp;</span><span style="font-family: Arial; font-size: 10pt; ">has requested pricing for the following media:</span><span style="font-weight: bold; font-family: Arial; font-size: 10pt; "><br></span><div><br></div><div><div><table width="" align="" cellpadding="12" cellspacing="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 1205px; background-image: url(http://localhost:1978/assets/javascript/innovaeditor/scripts/saf/undefined); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(220, 220, 220); border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><tbody><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 100px; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-weight: bold; font-size: 10pt; ">Name:</span></td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{$contactForm.name}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Email:</span></td><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$contactForm.email}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Media:</span></td><td colspan="1" style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{mediaImage mediaID=$media.encryptedID type=thumb folderID=$media.encryptedFID mode=imgtag}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 13px; font-weight: bold; ">Media ID:</span>\r\n</td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 10pt; ">{$media.media_id}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 13px; font-weight: bold; ">Digital Profile:</span>\r\n</td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 10pt; ">{$dsp.name}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Message:</span></td><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$contactForm.message}</span></td></tr></tbody></table></div><div><br></div><br class="Apple-interchange-newline"></div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(10, 'newOrderEmailAdmin', 6, 'New Order', 'Email that the sales email receives when a new order is placed.', 'You have received a new order:\r\n<div><br />\r\n	</div>\r\n<div>\r\n	<div>\r\n		<table width="" align="" style="border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; width: 100%; margin-left: 0px; margin-right: 0px; margin-top: 0px; margin-bottom: 0px; background-image: url(http://localhost:1978/assets/javascript/innovaeditor/scripts/saf/undefined); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(240, 246, 253); border-collapse: collapse; background-position: initial initial; background-repeat: initial initial; ">\r\n			<tbody>\r\n				<tr>\r\n					<td style="border-width: initial; border-color: initial; border-image: initial; padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px; letter-spacing: 0px; ">You can view the details of this order in your management area under Sales > Orders</td>\r\n				</tr>\r\n			</tbody>\r\n		</table></div>\r\n	<div>\r\n		<div style="text-align: left; ">\r\n			<div style="line-height: normal; text-align: -webkit-auto; "><span style="text-align: left; line-height: 1.3; "><br />\r\n					</span></div>\r\n			<div style="line-height: normal; text-align: -webkit-auto; "><span style="text-align: left; line-height: 1.3; ">Invoice Number: </span><span style="text-align: left; line-height: 1.3; font-weight: bold; ">{$invoice.invoice_number}</span></div>\r\n			<div style="line-height: normal; text-align: -webkit-auto; ">\r\n				<div style="text-align: right; line-height: 1.3; ">\r\n					<div style="text-align: left; "><span style="line-height: 1.3; ">Order Number:<span style="font-weight: bold;">&nbsp;{$order.order_number}</span></span></div></div>\r\n				<div style="text-align: left; "><span style="line-height: 20px; ">Order Date: </span><span style="line-height: 20px; font-weight: bold; ">{$invoice.invoice_date_display_admin}</span></div>\r\n				<div style="text-align: left; ">Payment Status: <span style="font-weight: bold; ">{$invoice.payment_status_lang}</span></div>\r\n				<div style="text-align: left; ">Order Status: <span style="font-weight: bold; ">{$order.order_status_lang}</span></div></div></div>\r\n		<div style="text-align: left;">Email Address: <span style="font-weight: bold; ">{$invoice.ship_email}</span></div></div>\r\n	<div style="text-align: right; "><br />\r\n		</div>\r\n	<div style="text-align: left;">{if $invoice.bill_name or $invoice.ship_name}</div>\r\n	<div>\r\n		<div style="text-align: left; ">\r\n			<table width="" align="" cellspacing="2" style="border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; width: 100%; ">\r\n				<tbody>\r\n					<tr>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px; background-image: initial; background-color: rgb(230, 230, 230); vertical-align: top; letter-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; "><span style="font-weight: bold; font-size: 14pt; ">Billing Address:<br />\r\n									</span><br />\r\n								<span style="font-weight: bold; ">{$invoice.bill_name}</span><br />\r\n								{$invoice.bill_address} {if $invoice.bill_address2}<br />\r\n								{$invoice.bill_address2}{/if}</span><br />\r\n							{$invoice.bill_city} {$invoice.bill_state} {$invoice.bill_zip}<br />\r\n							{$invoice.bill_country}</td>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px; background-image: initial; background-color: rgb(230, 230, 230); vertical-align: top; letter-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; "><span style="font-weight: bold; font-size: 14pt; ">Shipping Address:<br />\r\n									</span><br />\r\n								<span style="font-weight: bold; ">{$invoice.ship_name}</span><br />\r\n								{$invoice.ship_address} {if $invoice.ship_address2}<br />\r\n								{$invoice.ship_address2}{/if}</span><br />\r\n							{$invoice.ship_city} {$invoice.ship_state} {$invoice.ship_zip}<br />\r\n							{$invoice.ship_country}</td>\r\n					</tr>\r\n				</tbody>\r\n			</table></div>\r\n		<div style="text-align: left; ">{/if}</div>\r\n		<div style="text-align: left; "><br />\r\n			</div>\r\n		<div style="text-align: left; ">{foreach $invoiceItems as $invoiceItem}</div>\r\n		<div style="text-align: left; ">\r\n			<table class="invoiceItems" width="" align="" style="border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; width: 100%; margin-bottom: 2px; ">\r\n				<tbody>\r\n					<tr>\r\n						<td style="text-align: center; width: 30px; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; ">{$invoiceItem.quantity}</td>\r\n						<td rowspan="1" style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; ">{$invoiceItem.thumbnail}</td>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; width: 100%; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; word-spacing: 0px; "><span style="letter-spacing: 0px; ">{$invoiceItem.name}<br />\r\n								<span style="letter-spacing: 0px;"><span style="background-color: rgb(240, 240, 240);">{if $invoiceItem.rm}<br />\r\n										</span><span style="font-weight: bold;">{$lang.mediaLicenseRM}<br />\r\n										</span></span><span style="letter-spacing: 0px;">{foreach $invoiceItem.rm as $rm}</span><span style="letter-spacing: 0px; font-weight: bold;">{$rm.grpName}:</span><span style="letter-spacing: 0px;">&nbsp;{$rm.opName}<br />\r\n									{/foreach}&nbsp;<br />\r\n									{/if}</span><br />\r\n								</span></td>\r\n						<td style="text-align: right; width: 60px; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">{$invoiceItem.cost_value}<br />\r\n								</span><span style="font-size: 10pt; ">{if $invoiceItem.paytype==''cred''}credits{/if}</span></td>\r\n					</tr>\r\n				</tbody>\r\n			</table></div>\r\n		<div style="text-align: left; ">{/foreach}</div>\r\n		<div style="text-align: left; ">{if $invoice.credits_subtotal}</div>\r\n		<div style="text-align: left; "><br />\r\n			</div>\r\n		<div style="text-align: left; ">\r\n			<table width="" align="" style="border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; width: 100%; ">\r\n				<tbody>\r\n					<tr>\r\n						<td style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-color: rgb(240, 240, 240); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">Credits Subtotal:</span></td>\r\n						<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">{$invoice.credits_subtotal}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">Credits Discounts:</span></td>\r\n						<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">{$invoice.credits_discounts_total}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">Credits Total:</span></td>\r\n						<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">{$invoice.credits_total}</span></td>\r\n					</tr>\r\n				</tbody>\r\n			</table>{/if}</div>\r\n		<div style="text-align: left; "><br />\r\n			</div>\r\n		<div style="text-align: left; ">\r\n			<table width="" align="" style="border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; width: 100%; ">\r\n				<tbody>\r\n					<tr>\r\n						<td colspan="1" style="border-width: initial; border-color: initial; border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; "><span style="font-weight: bold; ">Sub Total:</span></td>\r\n						<td colspan="1" style="border-width: initial; border-color: initial; border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-color: rgb(220, 220, 220); letter-spacing: 0px; "><span style="font-weight: bold; border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; ">{$invoice.subtotal}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; "><span style="font-weight: bold; ">Shipping:</span></td>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; font-weight: bold; ">{$invoice.shipping_cost}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; "><span style="font-weight: bold; ">Tax/VAT:</span></td>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; font-weight: bold; ">{$invoice.tax_total}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; "><span style="font-weight: bold; ">Discounts:</span></td>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-color: rgb(220, 220, 220); letter-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; font-weight: bold; ">{$invoice.discounts_total}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td colspan="1" style="border-width: initial; border-color: initial; border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; "><span style="font-weight: bold; ">Total:</span></td>\r\n						<td colspan="1" style="border-width: initial; border-color: initial; border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; "><span style="font-weight: bold; ">{$invoice.total}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td colspan="1" style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">Payment:</span></td>\r\n						<td colspan="1" style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">{$invoice.payment}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td colspan="1" style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; text-align: right; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">Balance:</span></td>\r\n						<td colspan="1" style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">{$invoice.balance}</span></td>\r\n					</tr>\r\n				</tbody>\r\n			</table></div>\r\n		<div style="text-align: left; "><br />\r\n			</div>\r\n		<div style="text-align: left; ">*Totals shown in <span style="font-weight: bold; ">{$adminCurrency.code}</span></div></div>\r\n	<div><br />\r\n		</div></div>', 1, '', '', '', '', '', '', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(11, 'signupAgreement', 7, 'Signup Agreement', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.<div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div><div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(16, 'aboutUs', 1, 'About Us Page Content', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div><div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(17, 'privacyPolicy', 1, 'Privacy Policy Page Content', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div><div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(18, 'purchaseAgreement', 7, 'Purchase Agreement', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div><div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(24, '', 3, 'Custom Page 2', '', '<br class="innova">', 0, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(52, 'invoiceTemplate', 1, 'Invoice Template', '', '<span style="font-weight: bold; font-size: 18pt; ">{$config.settings.business_name} </span>\r\n<div style="text-align: right; line-height: 1.3; ">Invoice Number: <span style="font-weight: bold; ">{$invoice.invoice_number}</span><br />\r\n	{if $order.order_number}Order Number: <span style="font-weight: bold; ">{$order.order_number}</span>{/if}</div>\r\n<div style="text-align: right;"><br />\r\n	</div>\r\n<div style="text-align: left;">\r\n	<table style="width: 100%; border-collapse: collapse; ">\r\n		<tbody>\r\n			<tr>\r\n				<td style="padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; vertical-align: top; letter-spacing: 0px; word-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; "><span style="font-weight: bold; font-size: 14pt; ">Bill To:<br />\r\n							</span><br />\r\n						<span style="font-weight: bold; ">{$invoice.bill_name}</span><br />\r\n						{$invoice.bill_address} {if $invoice.bill_address2}<br />\r\n						{$invoice.bill_address2}{/if}</span><br />\r\n					{$invoice.bill_city}, {$invoice.bill_state} {$invoice.bill_zip}<br />\r\n					{$invoice.bill_country}</td>\r\n				<td style="padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; vertical-align: top; letter-spacing: 0px; word-spacing: 0px; "><span style="letter-spacing: 0px; font-weight: bold; font-size: 14pt; ">Pay To:</span><br />\r\n					<br />\r\n					<span style="font-weight: bold; ">{$config.settings.business_name}</span><br />\r\n					{$config.settings.business_address} {$config.settings.business_address2}<br />\r\n					{$config.settings.business_city}, {$config.settings.business_state} {$config.settings.business_zip}<br />\r\n					{$config.settings.business_country}</td>\r\n			</tr>\r\n		</tbody>\r\n	</table><br />\r\n	</div>\r\n<div style="text-align: left; line-height: 1.3; ">Invoice Date: <span style="font-weight: bold; ">{$invoice.invoice_date_display}</span><br />\r\n	{if $invoice.bill_id}Due Date: <span style="font-weight: bold; ">{$invoice.due_date_display}</span>{/if}</div>\r\n<div style="text-align: left;"><span style="font-weight: bold; "><br />\r\n		</span></div>\r\n<div style="text-align: left;">{foreach $invoiceItems as $invoiceItem}</div>\r\n<div style="text-align: left;">\r\n	<table style="width: 100%; margin-bottom: 2px; border-collapse: collapse; " class="invoiceItems">\r\n		<tbody>\r\n			<tr>\r\n				<td style="text-align: center;border-image: initial; width: 30px; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; ">{$invoiceItem.quantity}</td>\r\n				<td style="border-image: initial; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="letter-spacing: 0px;">{$invoiceItem.thumbnail} {$invoiceItem.name}</span><br />\r\n					{if $invoiceItem.rm} \r\n					<div style="background-color: #e5e5e5; border: 1px solid #FFF; padding: 10px; margin-top: 10px"><span style="font-weight: bold;">{$lang.mediaLicenseRM}</span><br />\r\n						\r\n						<ul> {foreach $invoiceItem.rm as $rm}<br />\r\n							\r\n							<li><span style="font-weight: bold;"> {$rm.grpName}:</span> {$rm.opName}</li>{/foreach} \r\n						</ul></div> {/if}</td>\r\n				<td style="text-align: right; border-image: initial; width: 60px; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; "><span style="letter-spacing: 0px; font-weight: bold; ">{$invoiceItem.cost_value} </span><br />\r\n					<span style="letter-spacing: 0px; word-spacing: 0px; font-size: 13px; ">{if $invoiceItem.paytype==''cred''}credits{/if}</span><span style="letter-spacing: 0px;"></span></td>\r\n			</tr>\r\n		</tbody>\r\n	</table></div>\r\n<div style="text-align: left;">{/foreach}</div>\r\n<div style="text-align: left;">{if $invoice.credits_subtotal}</div>\r\n<div style="text-align: left;"><br />\r\n	</div>\r\n<div style="text-align: left;">\r\n	<table style="width: 100%; border-collapse: collapse; ">\r\n		<tbody>\r\n			<tr>\r\n				<td style="border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">Credits Subtotal:</span></td>\r\n				<td style="border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">{$invoice.credits_subtotal}</span></td>\r\n			</tr>\r\n			<tr>\r\n				<td style="border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">Credits Discounts:</span></td>\r\n				<td style="border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">{$invoice.credits_discounts_total}</span></td>\r\n			</tr>\r\n			<tr>\r\n				<td style="border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">Credits Total:</span></td>\r\n				<td style="border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">{$invoice.credits_total}</span></td>\r\n			</tr>\r\n		</tbody>\r\n	</table>{/if}</div>\r\n<div style="text-align: left;"><br />\r\n	</div>\r\n<div style="text-align: left;">\r\n	<table style="width: 100%; border-collapse: collapse; " width="" align="">\r\n		<tbody>\r\n			<tr>\r\n				<td style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; " colspan="1"><span style="font-weight: bold; ">Sub Total:</span></td>\r\n				<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; " colspan="1"><span style="font-weight: bold; border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; ">{$invoice.subtotal}</span></td>\r\n			</tr>\r\n			<tr>\r\n				<td style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">Shipping:</span></td>\r\n				<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; font-weight: bold; ">{$invoice.shipping_cost}</span></td>\r\n			</tr>\r\n			<tr>\r\n				<td style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">Tax/VAT:</span></td>\r\n				<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; font-weight: bold; ">{$invoice.tax_total}<span style="font-weight: bold; text-align: right;"><br />\r\n							</span></span></td>\r\n			</tr>\r\n			<tr>\r\n				<td style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">Discounts:</span></td>\r\n				<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; font-weight: bold; ">{$invoice.discounts_total}</span></td>\r\n			</tr>\r\n			<tr>\r\n				<td style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; " colspan="1"><span style="font-weight: bold; ">Total:</span></td>\r\n				<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; " colspan="1"><span style="font-weight: bold; ">{$invoice.total}</span></td>\r\n			</tr>\r\n			<tr>\r\n				<td style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; " colspan="1"><span style="font-weight: bold; ">Payment:</span></td>\r\n				<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; " colspan="1"><span style="font-weight: bold; ">{$invoice.payment}</span></td>\r\n			</tr>\r\n			<tr>\r\n				<td style="border-top-color: rgb(0, 0, 0); border-right-color: rgb(0, 0, 0); border-bottom-color: rgb(0, 0, 0); border-left-color: rgb(0, 0, 0); padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; text-align: right; background-color: rgb(220, 220, 220); border-left-width: 1px; border-left-style: solid; border-right-width: 1px; border-right-style: solid; border-top-width: 1px; border-top-style: solid; border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; " colspan="1"><span style="font-weight: bold; ">Balance:</span></td>\r\n				<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); border-left-color: rgb(0, 0, 0); border-left-width: 1px; border-left-style: solid; border-right-color: rgb(0, 0, 0); border-right-width: 1px; border-right-style: solid; border-top-color: rgb(0, 0, 0); border-top-width: 1px; border-top-style: solid; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 1px; border-bottom-style: solid; letter-spacing: 0px; word-spacing: 0px; " colspan="1"><span style="font-weight: bold; ">{$invoice.balance}</span></td>\r\n			</tr>\r\n		</tbody>\r\n	</table></div>\r\n<div style="text-align: left;"><br />\r\n	</div>\r\n<div style="text-align: left;">*Totals shown in <span style="font-weight: bold; ">{$adminCurrency.code}</span></div>\r\n<div style="text-align: -webkit-auto;"><span style="border-collapse: collapse; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;"><br />\r\n		</span></div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(50, 'verifyAccountEmail', 4, 'Verify Your New {$config.settings.business_name} Account', 'Email that is sent to a new member for them to verify their account.', '{$member.f_name} {$member.l_name},<div><br></div><div>Please verify your new account at {$config.settings.business_name}&nbsp;by clicking the link below or copying and pasting the link in to your browser. <div><br></div><div>{$confirmLink}</div><div><br></div><div>Once verified you will be able to login and access your account.</div> </div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(51, 'newMemberEmailAdmin', 6, 'New Member', 'Email that the support email address receives when a new member signs up.', '<span style="font-family: Arial; font-size: 10pt; ">A new member has signed up on your site. This new member''s details are below.<br><br></span><div><div><table style="font-size: 10pt; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; width: 100%; background-image: url(http://localhost:1978/assets/javascript/innovaeditor/scripts/saf/undefined); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(220, 220, 220); background-position: initial initial; background-repeat: initial initial; " width="" align="" cellpadding="12" cellspacing="1"><tbody><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; width: 100px; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Name:</span></td><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$member.f_name} {$member.l_name}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Email: </span></td><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$member.email}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Phone:</span></td><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$member.phone}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Company Name: </span></td><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$member.comp_name}<br></span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Website: </span></td><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$member.website}<br></span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Address: </span></td><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$member.primaryAddress.address}{if $member.primaryAddress.address_2}<br>{$member.primaryAddress.address_2}{/if}<br>{$member.primaryAddress.city}, {$member.primaryAddress.state} {$member.primaryAddress.postal_code}<br>{$member.primaryAddress.country}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Account Status: </span></td><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$member.status}<br></span></td></tr></tbody></table></div></div><div><br></div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(54, 'orderEmail', 4, 'Your {$config.settings.business_name} Order', 'Email that is sent to a customer when they place a new order.', '<span style="font-weight: bold; font-size: 18pt; ">Your {$config.settings.business_name} Order</span>\r\n<div><span style="font-weight: bold; font-size: 18pt; "><br />\r\n		</span></div>\r\n<div><span style="font-size: 18pt; font-weight: bold; ">\r\n		<table style="border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; width: 100%; margin-left: 0px; margin-right: 0px; margin-top: 0px; margin-bottom: 0px; background-image: url(http://localhost:1978/assets/javascript/innovaeditor/scripts/saf/undefined); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(240, 246, 253); border-collapse: collapse; background-position: initial initial; background-repeat: initial initial; " width="" align="">\r\n			<tbody>\r\n				<tr>\r\n					<td style="border-width: initial; border-color: initial; border-image: initial; padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px; letter-spacing: 0px; word-spacing: 0px; ">View your order details and download any digital files you have purchased at the following link:<br />\r\n						{linkto page=$order.orderLink}</td>\r\n				</tr>\r\n			</tbody>\r\n		</table><br />\r\n		</span>\r\n	<div><span style="text-align: left; line-height: 1.3; ">Invoice Number: </span><span style="text-align: left; line-height: 1.3; font-weight: bold; ">{$invoice.invoice_number}</span></div>\r\n	<div>\r\n		<div style="text-align: right; line-height: 1.3; ">\r\n			<div style="text-align: left;"><span style="line-height: 1.3; ">Order Number: </span><span style="line-height: 1.3; font-weight: bold; "><a href="{linkto page=$order.orderLink}">{$order.order_number}</a></span></div></div>\r\n		<div style="text-align: left;"><span style="line-height: 20px; ">Order Date: </span><span style="line-height: 20px; font-weight: bold; ">{$invoice.invoice_date_display}</span></div>\r\n		<div style="text-align: left;">Payment Status: <span style="font-weight: bold; ">{$invoice.payment_status_lang}</span></div>\r\n		<div style="text-align: left;">Order Status: <span style="font-weight: bold; ">{$order.order_status_lang}</span></div>\r\n		<div style="text-align: left;"><br />\r\n			</div>\r\n		<div style="text-align: left;">{if $invoice.bill_name or $invoice.ship_name}</div>\r\n		<div style="text-align: left; ">\r\n			<table style="width: 100%; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " width="" align="" cellspacing="2">\r\n				<tbody>\r\n					<tr>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px; background-image: initial; background-color: rgb(230, 230, 230); vertical-align: top; letter-spacing: 0px; word-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; "><span style="font-weight: bold; font-size: 14pt; ">Billing Address:<br />\r\n									</span><br />\r\n								<span style="font-weight: bold; ">{$invoice.bill_name}</span><br />\r\n								{$invoice.bill_address} {if $invoice.bill_address2}<br />\r\n								{$invoice.bill_address2}{/if}</span><br />\r\n							{$invoice.bill_city} {$invoice.bill_state} {$invoice.bill_zip}<br />\r\n							{$invoice.bill_country}</td>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; padding-left: 20px; padding-right: 20px; padding-top: 20px; padding-bottom: 20px; background-image: initial; background-color: rgb(230, 230, 230); vertical-align: top; letter-spacing: 0px; word-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; "><span style="font-weight: bold; font-size: 14pt; ">Shipping Address:<br />\r\n									</span><br />\r\n								<span style="font-weight: bold; ">{$invoice.ship_name}</span><br />\r\n								{$invoice.ship_address} {if $invoice.ship_address2}<br />\r\n								{$invoice.ship_address2}{/if}</span><br />\r\n							{$invoice.ship_city} {$invoice.ship_state} {$invoice.ship_zip}<br />\r\n							{$invoice.ship_country} </td>\r\n					</tr>\r\n				</tbody>\r\n			</table></div>\r\n		<div style="text-align: left; ">{/if}</div>\r\n		<div style="text-align: left; "><br />\r\n			</div>\r\n		<div style="text-align: left; ">{foreach $invoiceItems as $invoiceItem}</div>\r\n		<div style="text-align: left; ">\r\n			<table class="invoiceItems" style="width: 100%; margin-bottom: 2px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " width="" align="">\r\n				<tbody>\r\n					<tr>\r\n						<td style="text-align: center; width: 30px; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; ">{$invoiceItem.quantity}</td>\r\n						<td style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " rowspan="1">{$invoiceItem.thumbnail} </td>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; width: 100%; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; word-spacing: 0px; "><span style="letter-spacing: 0px; ">{$invoiceItem.name}<br />\r\n								<span style="background-color: rgb(240, 240, 240);">{if $invoiceItem.rm}<br />\r\n									</span><span style="font-weight: bold;">{$lang.mediaLicenseRM}<br />\r\n									</span></span><span style="letter-spacing: 0px;">{foreach $invoiceItem.rm as $rm}</span><span style="letter-spacing: 0px; font-weight: bold;">{$rm.grpName}:</span><span style="letter-spacing: 0px;"> {$rm.opName}<br />\r\n								{/foreach}&nbsp;<br />\r\n								{/if}</span></td>\r\n						<td style="text-align: right; width: 60px; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">{$invoiceItem.cost_value}<br />\r\n								</span><span style="font-size: 10pt; ">{if $invoiceItem.paytype==''cred''}credits{/if}</span></td>\r\n					</tr>\r\n				</tbody>\r\n			</table></div>\r\n		<div style="text-align: left; ">{/foreach}</div>\r\n		<div style="text-align: left; ">{if $invoice.credits_subtotal}</div>\r\n		<div style="text-align: left; "><br />\r\n			</div>\r\n		<div style="text-align: left; ">\r\n			<table style="width: 100%; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " width="" align="">\r\n				<tbody>\r\n					<tr>\r\n						<td style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-color: rgb(240, 240, 240); letter-spacing: 0px; word-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">Credits Subtotal:</span></td>\r\n						<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-color: rgb(220, 220, 220); letter-spacing: 0px; word-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">{$invoice.credits_subtotal}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; word-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">Credits Discounts:</span></td>\r\n						<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-color: rgb(220, 220, 220); letter-spacing: 0px; word-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">{$invoice.credits_discounts_total}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; word-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">Credits Total:</span></td>\r\n						<td style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; word-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">{$invoice.credits_total}</span></td>\r\n					</tr>\r\n				</tbody>\r\n			</table>{/if}</div>\r\n		<div style="text-align: left; "><br />\r\n			</div>\r\n		<div style="text-align: left; ">\r\n			<table width="" align="" style="width: 100%; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; ">\r\n				<tbody>\r\n					<tr>\r\n						<td colspan="1" style="border-width: initial; border-color: initial; border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">Sub Total:</span></td>\r\n						<td colspan="1" style="border-width: initial; border-color: initial; border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-color: rgb(220, 220, 220); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; ">{$invoice.subtotal}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">Shipping:</span></td>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; word-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; font-weight: bold; ">{$invoice.shipping_cost}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">Tax/VAT:</span></td>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; word-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; font-weight: bold; ">{$invoice.tax_total}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">Discounts:</span></td>\r\n						<td style="border-width: initial; border-color: initial; border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-color: rgb(220, 220, 220); letter-spacing: 0px; word-spacing: 0px; "><span style="border-collapse: separate; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; font-weight: bold; ">{$invoice.discounts_total}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td colspan="1" style="border-width: initial; border-color: initial; border-image: initial; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(240, 240, 240); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">Total:</span></td>\r\n						<td colspan="1" style="border-width: initial; border-color: initial; border-image: initial; width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; word-spacing: 0px; "><span style="font-weight: bold; ">{$invoice.total}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td colspan="1" style="text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">Payment:</span></td>\r\n						<td colspan="1" style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">{$invoice.payment}</span></td>\r\n					</tr>\r\n					<tr>\r\n						<td colspan="1" style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; text-align: right; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">Balance:</span></td>\r\n						<td colspan="1" style="width: 60px; text-align: right; padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; background-image: initial; background-color: rgb(220, 220, 220); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; ">{$invoice.balance}</span></td>\r\n					</tr>\r\n				</tbody>\r\n			</table></div>\r\n		<div style="text-align: left; "><br />\r\n			</div>\r\n		<div style="text-align: left; ">*Totals shown in <span style="font-weight: bold; ">{$adminCurrency.code}</span></div></div></div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(55, 'newRatingEmailAdmin', 6, 'New Rating', 'Email that the support email address receives when a new rating is posted.', '<span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">{$user}&nbsp;</span><span style="font-family: Arial; font-size: 10pt; ">has rated the following media:</span><span style="font-weight: bold; font-family: Arial; font-size: 10pt; "><br></span><div><br></div><div><div><table width="" align="" cellpadding="12" cellspacing="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 1205px; background-image: url(http://localhost:1978/assets/javascript/innovaeditor/scripts/saf/undefined); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(220, 220, 220); border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><tbody><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 100px; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-weight: bold; font-size: 10pt; ">Rating:</span></td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{$rating}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Media:</span></td><td colspan="1" style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{mediaImage mediaID=$media.encryptedID type=thumb folderID=$media.encryptedFID mode=imgtag}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 13px; font-weight: bold; ">Media ID:</span>\r\n</td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 10pt; ">{$media.media_id}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Link:</span></td><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$media.linkto}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Status:</span></td><td colspan="1" style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; "><span style="letter-spacing: 0px; ">{if $autoApprove}<span style="color: rgb(107, 142, 35); ">A</span></span><span style="color: rgb(107, 142, 35); ">pproved</span></span><span style="font-family: Arial; font-size: 10pt; "><span style="letter-spacing: 0px; ">{else}</span><span style="color: rgb(128, 0, 0); ">Pending Approval</span></span><span style="font-family: Arial; font-size: small; letter-spacing: 0px; ">{/if}</span></td></tr></tbody></table></div><div><br></div><br class="Apple-interchange-newline"></div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(56, 'newCommentEmailAdmin', 6, 'New Comment', 'Email that the support email address receives when a new comment is posted.', '<span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">{$user}&nbsp;</span><span style="font-family: Arial; font-size: 10pt; ">has submitted a new comment on the following media:</span><span style="font-weight: bold; font-family: Arial; font-size: 10pt; "><br></span><div><br></div><div><div><table width="" align="" cellpadding="12" cellspacing="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 1205px; background-image: url(http://localhost:1978/assets/javascript/innovaeditor/scripts/saf/undefined); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(220, 220, 220); border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><tbody><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 100px; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-weight: bold; font-size: 10pt; ">Comment:</span></td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{$comment}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Language:</span></td><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$language}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Media:</span></td><td colspan="1" style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{mediaImage mediaID=$media.encryptedID type=thumb folderID=$media.encryptedFID mode=imgtag}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 13px; font-weight: bold; ">Media ID:</span>\r\n</td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 10pt; ">{$media.media_id}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Link:</span></td><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$media.linkto}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Status:</span></td><td colspan="1" style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; "><span style="letter-spacing: 0px; ">{if $autoApprove}<span style="color: rgb(107, 142, 35); ">A</span></span><span style="color: rgb(107, 142, 35); ">pproved</span></span><span style="font-family: Arial; font-size: 10pt; "><span style="letter-spacing: 0px; ">{else}</span><span style="color: rgb(128, 0, 0); ">Pending Approval</span></span><span style="font-family: Arial; font-size: small; letter-spacing: 0px; ">{/if}</span></td></tr></tbody></table></div><div><br></div><br class="Apple-interchange-newline"></div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(57, 'newTagEmailAdmin', 6, 'New Tag', 'Email that the support email address receives when a new tag is posted.', '<span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">{$user}&nbsp;</span><span style="font-family: Arial; font-size: 10pt; ">has submitted a new tag for the following media:</span><span style="font-weight: bold; font-family: Arial; font-size: 10pt; "><br></span><div><br></div><div><div><table style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 100%; background-image: url(http://localhost:1978/assets/javascript/innovaeditor/scripts/saf/undefined); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(220, 220, 220); border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; background-position: initial initial; background-repeat: initial initial; " width="" align="" cellpadding="12" cellspacing="1"><tbody><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 100px; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-weight: bold; font-size: 10pt; ">Tag:</span></td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{$tag}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; " colspan="1"><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Language: </span></td><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; " colspan="1"><span style="font-family: Arial; font-size: 10pt; ">{$language}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Media:</span></td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 10pt; ">{mediaImage mediaID=$media.encryptedID type=thumb folderID=$media.encryptedFID mode=imgtag} </span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 13px; font-weight: bold; ">Media ID:</span>\r\n</td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 10pt; ">{$media.media_id}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; " colspan="1"><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Link:</span></td><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; " colspan="1"><span style="font-family: Arial; font-size: 10pt; ">{$media.linkto}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; word-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Status:</span></td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; word-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 10pt; "><span style="letter-spacing: 0px;">{if $autoApprove}<span style="color: rgb(107, 142, 35); ">A</span></span><span style="color: rgb(107, 142, 35); ">pproved</span></span><span style="font-family: Arial; font-size: 10pt; "><span style="letter-spacing: 0px;">{else}</span><span style="color: rgb(128, 0, 0); ">Pending Approval</span></span><span style="font-family: Arial; font-size: small; letter-spacing: 0px; ">{/if}</span></td></tr></tbody></table></div><div><br></div><div><br></div></div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(58, 'requestFileEmailAdmin', 6, 'Request For File', 'Email that gets sent to the support email address when a visitor or member requests a file.', '<span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">{$user}&nbsp;</span><span style="font-family: Arial; font-size: 10pt; ">has requested a file:</span>\r\n <div><div><br class="Apple-interchange-newline"><table width="" align="" cellpadding="12" cellspacing="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 1205px; background-image: url(http://localhost:1978/assets/javascript/innovaeditor/scripts/saf/undefined); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(220, 220, 220); border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><tbody><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 100px; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-weight: bold; font-size: 10pt; ">Member:</span></td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{$user}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Member ID:</span></td><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{if $member.mem_id}{$member.mem_id}{else}None{/if}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; " colspan="1"><span style="font-family: Arial; font-size: 13px; font-weight: bold; ">Email:</span>\r\n</td><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; " colspan="1"><span style="font-family: Arial; font-size: 10pt; ">{$email}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Media:</span></td><td colspan="1" style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{mediaImage mediaID=$media.encryptedID type=thumb folderID=$media.encryptedFID mode=imgtag}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 13px; font-weight: bold; ">Media ID:</span>\r\n</td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 10pt; ">{$media.media_id}</span></td></tr><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 13px; font-weight: bold; ">Digital Profile:</span>\r\n</td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " colspan="1"><span style="font-family: Arial; font-size: 10pt; ">{$dsp.name}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Link:</span></td><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$media.linkto}</span></td></tr></tbody></table></div><div><br></div><br class="Apple-interchange-newline"></div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(59, 'newLightboxEmailAdmin', 6, 'New Lightbox', 'Email that the support email address receives when a new lightbox is created.', '<span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">{$user}&nbsp;</span><span style="font-family: Arial; font-size: 10pt; ">has created a new lightbox:</span><span style="font-weight: bold; font-family: Arial; font-size: 10pt; "><br></span><div><br></div><div><div><table width="" align="" cellpadding="12" cellspacing="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 1205px; background-image: url(http://localhost:1978/assets/javascript/innovaeditor/scripts/saf/undefined); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(220, 220, 220); border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><tbody><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 100px; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-weight: bold; font-size: 10pt; ">Lightbox:</span></td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{$lightboxName}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Description:</span></td><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); "><span style="font-family: Arial; font-size: 10pt; ">{$description}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Link:</span></td><td colspan="1" style="background-image: initial; background-color: rgb(247, 247, 247); border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; letter-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{linkto page=$lightboxLink}</span></td></tr></tbody></table></div><div><br></div><br class="Apple-interchange-newline"></div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(61, 'memInfoUpdateEmailAdmin', 6, 'Member Info Updated', 'Email that the support email address receives when a member changes their contact or member info.', '<span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">{$member.f_name} {$member.l_name}&nbsp;</span><span style="font-family: Arial; font-size: 10pt; ">has updated their member info:</span><span style="font-weight: bold; font-family: Arial; font-size: 10pt; "><br></span><div><br></div><div><div><table width="" align="" cellpadding="12" cellspacing="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 1205px; background-image: url(http://localhost:1978/assets/javascript/innovaeditor/scripts/saf/undefined); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(220, 220, 220); border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><tbody><tr><td style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; width: 100px; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-weight: bold; font-size: 10pt; ">Member ID:</span></td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{$member.mem_id}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Email:</span></td><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$member.email}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Edit Mode:</span></td><td colspan="1" style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-width: initial; border-color: initial; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{$mode}</span></td></tr></tbody></table></div><div><br></div></div><br class="Apple-interchange-newline">', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(64, 'contactFormEmailAdmin', 6, 'Contact Form Question', 'Email that the sales email address receives when a visitor submits a question through the contact form.', '<span style="font-family: Arial; font-size: 10pt; ">Question from contact form:</span><span style="font-weight: bold; font-family: Arial; font-size: 10pt; "><br></span><div><br></div><div><div><table width="" align="" cellpadding="12" cellspacing="1" style="width: 1205px; background-image: url(http://localhost:1978/assets/javascript/innovaeditor/scripts/saf/undefined); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(220, 220, 220); border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><tbody><tr><td style="width: 100px; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-weight: bold; font-size: 10pt; ">Name:</span></td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{$form.name}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Email:</span></td><td colspan="1" style="border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$form.email}</span></td></tr><tr><td colspan="1" style="text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Question:</span></td><td colspan="1" style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{$form.question}</span></td></tr></tbody></table></div><div><br></div><br class="Apple-interchange-newline"></div>', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(65, 'emailFriendMedia', 4, 'Photo from {$form.fromName}', 'Email that gets sent from a visitor or member when they email someone a about a photo they have found on the site.', '<span style="font-family: Arial; font-size: 10pt; ">{$form.fromName} found the following media on {$config.settings.site_title} and wants to share it with you:</span><span style="font-weight: bold; font-family: Arial; font-size: 10pt; "><br></span><div><br></div><div><div><table width="" align="" cellpadding="12" cellspacing="1" style="width: 1205px; background-image: url(http://localhost:1978/assets/javascript/innovaeditor/scripts/saf/undefined); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: rgb(220, 220, 220); border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><tbody><tr><td style="width: 100px; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-weight: bold; font-size: 10pt; ">Message:</span></td><td style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{$form.message}</span></td></tr><tr><td colspan="1" style="text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Media:</span></td><td colspan="1" style="background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; "><span style="font-family: Arial; font-size: 10pt; ">{mediaImage mediaID=$media.encryptedID type=thumb folderID=$media.encryptedFID mode=imgtag}</span></td></tr><tr><td colspan="1" style="border-width: initial; border-color: initial; border-image: initial; text-align: right; background-image: initial; background-color: rgb(237, 237, 237); letter-spacing: 0px; "><span style="font-weight: bold; font-family: Arial; font-size: 10pt; ">Link:</span></td><td colspan="1" style="border-width: initial; border-color: initial; border-image: initial; background-image: initial; background-color: rgb(247, 247, 247); letter-spacing: 0px; "><span style="font-family: Arial; font-size: 10pt; ">{$media.linkto}</span></td></tr></tbody></table></div><div><br></div></div><br class="Apple-interchange-newline">', 1, '', '<br class="innova">', '', '<br class="innova">', '', '<br class="innova">', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(66, 'checkConfirmPage', 1, 'Check/Money Order Confirmation Page', '', 'Thank you. We have received your order. Please mail your check or money order for the following amount to the address listed. Once payment is received we will approve your order. <div><br></div><div>Total: <span class="price">{$cartTotals.cartGrandTotalLocal.display}</span></div><div><br></div><div>*Please reference the following number with your payment: <span style="font-weight: bold; ">{$cartInfo.orderNumber}</span></div><div><span style="font-weight: bold; "><br></span></div><div>Mail Payment To:</div><div><p><strong>{$config.settings.business_name}</strong><br>{$config.settings.business_address}<br>{if $config.settings.business_address2}{$config.settings.business_address2}<br>{/if} {$config.settings.business_city}, {$config.settings.business_state} {$config.settings.business_zip}<br>{$config.settings.business_country}<br></p></div>', 1, '', '', '', '', '', '', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(68, 'customBlock1', 2, 'Custom Block 1', '', '', 0, '', '', '', '', '', '', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(69, 'customBlock2', 2, 'Custom Block 2', '', '', 0, '', '', '', '', '', '', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(70, 'customBlock3', 2, 'Custom Block 3', '', '', 0, '', '', '', '', '', '', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(71, 'customBlockFooter', 2, 'Custom Footer Content Block', '', '', 0, '', '', '', '', '', '', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(72, 'emailForgottenPassword', 4, 'Your password for {$config.settings.business_name}', '', '<p>{$form.memberName},</p>\r\n<p>Here is your password:<br />{$form.password}</p>\r\n<p>You can log into our site at {$config.settings.site_url}.</p>\r\n<p>Thanks<br />{$config.settings.business_name}</p>', 0, '', '', '', '', '', '', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(73, 'orderApprovalMessage', 4, 'Your {$config.settings.business_name} order was approved', '', '<p>Your order {$order.order_number} has been approved. If you have made payment using a check or money order this means that your payment has cleared. You can always log into your account at our site {$config.settings.site_url} to view the status of your order. If you have any questions please contact us.</p><p>Order Number: {$order.order_number} <br>Order Details: <a href="{$order.orderLink}">{$order.orderLink}</a></p><p>Thanks {$config.settings.business_name}</p>', 1, '', '', '', '', '', '', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(74, 'newContrUploadEmailAdmin', 6, 'New Contributor Upload', 'Email that the support email address receives when a member uploads new media.', '{$member.f_name} {$member.l_name} has uploaded new media to their account.<div><br />	</div><div>{if $approvalStatus == 0}</div><div>Please log in to your management area to approve these new uploads.</div><div>{/if}</div>', 1, '', '', '', '', '', '', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(75, 'newMemberTicketResponse', 4, 'Ticket Response From {$config.settings.business_name}', 'Email sent to the member when a reply was posted to a ticket.', 'We have responded to a ticket that you have submitted to our site.<br />\r\nPlease log into your account to view the ticket response at <a href=''{$config.settings.site_url}''>{$config.settings.site_url}</a><br />\r\n<br />\r\nThanks<br />\r\n{$config.settings.business_name}', 1, '', '', '', '', '', '', '', '', '', '', 1, '');
INSERT INTO `ps4_content` VALUES(76, 'newAdminTicketResponse', 6, 'New Ticket or Ticket Response From {$config.settings.business_name}', 'Email sent to the store owner when a reply was posted to a ticket.', 'You either got a new ticket submitted, or a response to a previous ticket from a member of your store.<br>Please log into your store manager to view the new ticket response.', 1, '', '', '', '', '', '', '', '', '', '', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `ps4_content_areas`
--

CREATE TABLE `ps4_content_areas` (
  `ca_id` int(3) NOT NULL AUTO_INCREMENT,
  `langid` varchar(50) NOT NULL,
  `mgrarea` varchar(14) NOT NULL,
  `sortorder` int(3) NOT NULL,
  UNIQUE KEY `ca_id` (`ca_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ps4_content_areas`
--

INSERT INTO `ps4_content_areas` VALUES(1, 'pc_content', 'page_content', 1);
INSERT INTO `ps4_content_areas` VALUES(2, 'pc_custom_block', 'page_content', 100);
INSERT INTO `ps4_content_areas` VALUES(3, 'pc_custom_page', 'page_content', 99);
INSERT INTO `ps4_content_areas` VALUES(4, 'ec_public', 'email_content', 1);
INSERT INTO `ps4_content_areas` VALUES(5, 'ec_custom', 'email_content', 100);
INSERT INTO `ps4_content_areas` VALUES(6, 'ec_admin', 'email_content', 2);
INSERT INTO `ps4_content_areas` VALUES(7, 'agree_general', 'agreements', 1);
INSERT INTO `ps4_content_areas` VALUES(8, 'agree_custom', 'agreements', 100);

-- --------------------------------------------------------

--
-- Table structure for table `ps4_content_files`
--

CREATE TABLE `ps4_content_files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `content_id` varchar(250) CHARACTER SET utf8 NOT NULL,
  `file_name` text CHARACTER SET utf8 NOT NULL,
  `type` varchar(250) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ps4_content_files`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_countries`
--

CREATE TABLE `ps4_countries` (
  `country_id` int(5) NOT NULL AUTO_INCREMENT,
  `ucountry_id` varchar(40) NOT NULL,
  `name` varchar(100) NOT NULL,
  `code2` varchar(2) NOT NULL,
  `code3` varchar(3) NOT NULL,
  `numcode` int(5) NOT NULL,
  `longitude` varchar(4) NOT NULL,
  `latitude` varchar(4) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `region` varchar(8) NOT NULL,
  `ship_percentage` int(4) NOT NULL DEFAULT '100',
  `all_ship_methods` tinyint(1) NOT NULL DEFAULT '1',
  `name_dutch` varchar(255) NOT NULL,
  `name_french` varchar(255) NOT NULL,
  `name_german` varchar(255) NOT NULL,
  `name_spanish` varchar(255) NOT NULL,
  `name_english` varchar(255) NOT NULL,
  UNIQUE KEY `country_id` (`country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=252 ;

--
-- Dumping data for table `ps4_countries`
--

INSERT INTO `ps4_countries` VALUES(2, 'D8C814DB6C0BCD8305DA74183133EDAC', 'Aland Islands', 'AX', '', 0, '20', '60', 1, 0, '', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(3, 'D395B656E13856D5E92B52FD1D50CAF4', 'Albania', 'AL', 'ALB', 8, '20', '41', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(4, 'E50D080B32293AAC1E59C7D82FA93973', 'Algeria', 'DZ', 'DZA', 12, '3', '28', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(5, 'BCF7A337A759EBFF9D74F75B58A01F51', 'American Samoa', 'AS', 'ASM', 16, '-170', '-14.', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(6, '4233AC149C9F2501811E8ADF123E8863', 'Andorra', 'AD', 'AND', 20, '1.3', '42.3', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(7, 'A30A65900449B824659806615BC5DA79', 'Angola', 'AO', 'AGO', 24, '18.3', '-12.', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(8, '8D705C5B77B25D22BC4A4E79134B3A09', 'Anguilla', 'AI', 'AIA', 660, '-63.', '18.1', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(9, 'EFCF921D53D6AE95CB8B2686DF32464D', 'Antarctica', 'AQ', 'ATA', 10, '0', '-90', 1, 0, 'AN', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(10, 'B389ADA6129CB96B776A1F23E81045EB', 'Antigua and Barbuda', 'AG', 'ATG', 28, '-61.', '17.0', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(11, '8D3B97B7E875BBDF97B035A3FB760E27', 'Argentina', 'AR', 'ARG', 32, '-64', '-34', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(12, '4DC3E101EB5D7528FB38F677AF08E4AB', 'Armenia', 'AM', 'ARM', 51, '45', '40', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(13, 'D67A8F8684C52D2951E064732D3F3A71', 'Aruba', 'AW', 'ABW', 533, '-69.', '12.3', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(14, '0476CFF03A4EC4B985A014C7099C797A', 'Asia-Pacific', 'AP', '', 0, '128.', '-2.8', 1, 0, '', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(15, 'FED5698677387BF9470EA5F7667DD517', 'Australia', 'AU', 'AUS', 36, '133', '27', 1, 0, 'AU', 150, 0, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(16, 'B9B6959B72CC5C5B374B1882322B221D', 'Austria', 'AT', 'AUT', 40, '13.2', '47.2', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(17, '8FEF099BDFC03B52CBC0EA15F7870B6C', 'Azerbaijan', 'AZ', 'AZE', 31, '47.3', '40.3', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(18, '3DF9A05A5B7FEA4BB301C329242AC756', 'Bahamas', 'BS', 'BHS', 44, '-76', '24.1', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(19, 'E28E1F30EE39F7ABB5FFD7B2D86724A7', 'Bahrain', 'BH', 'BHR', 48, '50.3', '26', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(20, '8DA715B7918774A0C1B5FC785DD69256', 'Bangladesh', 'BD', 'BGD', 50, '90', '24', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(21, 'DA1A51D6317CD1D0603978C3459A7FCA', 'Barbados', 'BB', 'BRB', 52, '-59.', '13.1', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(22, 'E4C02641F63DD102C21DFED86D538967', 'Belarus', 'BY', 'BLR', 112, '28', '53', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(23, 'D611C5E0B99C9D266F360D97E047276D', 'Belgium', 'BE', 'BEL', 56, '4', '50.5', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(24, 'EE01BF63F9316BC9C5FE12D9BA2C08BD', 'Belize', 'BZ', 'BLZ', 84, '-88.', '17.1', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(25, 'F827E4E89A87114EE73E9AACD1294C7B', 'Benin', 'BJ', 'BEN', 204, '2.15', '9.3', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(26, 'A0BA7874B6277766108DCE38ADD4C511', 'Bermuda', 'BM', 'BMU', 60, '-64.', '32.2', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(27, '5A646D5FF0595FCB2D1A5FA6B632946F', 'Bhutan', 'BT', 'BTN', 64, '90.3', '27.3', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(28, '6BADE4B7CC213DC9FBF8C529767DEE8B', 'Bolivia', 'BO', 'BOL', 68, '-65', '-17', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(29, '0FB29D300241F875ECD3E437694D476A', 'Bosnia and Herzegowina', 'BA', 'BIH', 70, '18', '44', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(30, '055FB1615AD80679AFCA43C48BA4AF4E', 'Botswana', 'BW', 'BWA', 72, '24', '-22', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(31, 'F12AAE60BA468E8F0AF55F0932221981', 'Bouvet Island', 'BV', 'BVT', 74, '3.24', '-54.', 1, 0, 'AN', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(32, '2FCC27B93125F5590A9480942B0A35C3', 'Brazil', 'BR', 'BRA', 76, '-55', '-10', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(33, 'B04ED4C1EA0AFACFA54685D6FD1F1D07', 'British Indian Ocean Territory', 'IO', 'IOT', 86, '71.3', '-6', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(34, '8B6064064DC71EF12EFA9DC2703D6190', 'British Virgin Islands', 'VG', 'VGB', 92, '-64.', '18.2', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(35, 'E782A15B29DF48F7F257214C9E34EE9B', 'Brunei Darussalam', 'BN', 'BRN', 96, '114.', '4.3', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(36, 'C3577BD9AD630F7614CB99AD02AE546F', 'Bulgaria', 'BG', 'BGR', 100, '25', '43', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(37, 'FFCBB0EA58411D4234210615AC8F69A4', 'Burkina Faso', 'BF', 'BFA', 854, '-2', '13', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(38, 'EB24A6D6AF633F20A6C02C3FAB51149A', 'Burundi', 'BI', 'BDI', 108, '30', '-3.3', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(39, '663B8404C6897C0B9300F897AB38FB2E', 'Cambodia', 'KH', 'KHM', 116, '105', '13', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(40, '1CE06966272FB45EB250FBFE6578CA5B', 'Cameroon', 'CM', 'CMR', 120, '12', '6', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(41, 'AB2388A5D5573895BD11A17ED02E62A7', 'Canada', 'CA', 'CAN', 124, '-95', '60', 1, 0, 'NA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(42, 'AEB79CAE44D7CFABEF48094B3B13E1E3', 'Cape Verde', 'CV', 'CPV', 132, '-24', '16', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(43, '1BD9D92FB2559FDB0D4D53FF3F1FB2DC', 'Cayman Islands', 'KY', 'CYM', 136, '-80.', '19.3', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(44, '11F14EE3E325AD247FF68FBF8542159C', 'Central African Republic', 'CF', 'CAF', 140, '21', '7', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(45, '0A589B57AA641191C033BE7C610F0298', 'Chad', 'TD', 'TCD', 148, '19', '15', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(46, 'B4BA98D6E97258F131479E650837AD17', 'Chile', 'CL', 'CHL', 152, '-71', '-30', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(47, 'F5EDB445425F3225DFA01B8A5318381A', 'China', 'CN', 'CHN', 156, '105', '35', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(48, '23424389E1A6E8A3EB05D1D0D9093BE5', 'Christmas Island', 'CX', 'CXR', 162, '105.', '-10.', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(49, '2EFD50B2EADB2F6FAF520B1AA1777B50', 'Cocos (Keeling) Islands', 'CC', 'CCK', 166, '96.5', '-12.', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(50, '3B67CF92AD678D2DFC788976FE009045', 'Colombia', 'CO', 'COL', 170, '-72', '4', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(51, 'AC9E6DAACA160BBBA828BBDB6CABE4E6', 'Comoros', 'KM', 'COM', 174, '44.1', '-12.', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(52, '13E7034DEC367D61596ADB17455ED13D', 'Congo', 'CG', 'COG', 178, '25', '0', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(53, '9BAB0BA1F20BFCB2899E467AD6FFB5E3', 'Cook Islands', 'CK', 'COK', 184, '-159', '-21.', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(54, 'CA18F850E96C288D6793281521E225B1', 'Costa Rica', 'CR', 'CRI', 188, '-84', '10', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(55, 'B82C4CDB4E087DA9E5457DC4EE618E48', 'Croatia', 'HR', 'HRV', 191, '15.3', '45.1', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(56, 'A5E432B0CA5195B11871BBFFA01FCF39', 'Cuba', 'CU', 'CUB', 192, '-80', '21.3', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(57, 'F3F01FCB6FC7B9E4D2A295E375BFDDF7', 'Cyprus', 'CY', 'CYP', 196, '33', '35', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(58, '0192B21DAF94B14ED487DC4D192C4355', 'Czech Republic', 'CZ', 'CZE', 203, '15.3', '49.4', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(59, '83B22E9FC98E8E6A3F2D7E1A9A9BD460', 'Denmark', 'DK', 'DNK', 208, '10', '56', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(60, 'B0B95921DC6B9DBF5747F5EA5250E98E', 'Djibouti', 'DJ', 'DJI', 262, '43', '11.3', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(61, 'AC05B9B02AAE85D7509A013BDCC5A600', 'Dominica', 'DM', 'DMA', 212, '-61.', '15.2', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(62, 'B30EFFF6CE2DFFA9F314F02C20948F4F', 'Dominican Republic', 'DO', 'DOM', 214, '-70.', '19', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(63, '38EDBAA8D0A51CDB6CDC13F9C13DB54A', 'East Timor', 'TP', '', 0, '125.', '-8.5', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(64, '7A1726234143E1781319BF2BC8F400F8', 'Ecuador', 'EC', 'ECU', 218, '-77.', '-2', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(65, '95D4C619FB3BD5DBDE8DDF635EA55B6A', 'Egypt', 'EG', 'EGY', 818, '30', '27', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(66, '5A26855ADFA25AA150290AAF74DF9960', 'El Salvador', 'SV', 'SLV', 222, '-88.', '13.5', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(67, '8E18DC032C1E093F9025AF4AC7FBA605', 'Equatorial Guinea', 'GQ', 'GNQ', 226, '10', '2', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(68, 'DDCD527C32EB15D83083A3DBEBFE2141', 'Eritrea', 'ER', 'ERI', 232, '39', '15', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(69, 'F54474A3C1C664BA0675A5AD6DA6A8D8', 'Estonia', 'EE', 'EST', 233, '26', '59', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(70, '485E182493A6257D476CE479D21E7AA3', 'Ethiopia', 'ET', 'ETH', 210, '38', '8', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(71, '5836DB95EDC6BD0980A782ED1FD9CD47', 'Europe', 'EU', '', 0, '0', '0', 1, 0, '', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(72, '3372A980C66EBC12E2BF9C9AD1E0C602', 'Falkland Islands (Malvinas)', 'FK', 'FLK', 238, '-59', '-51.', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(73, '038DF51E5E0799EB366C8E5BC25C6910', 'Faroe Islands', 'FO', 'FRO', 234, '-7', '62', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(74, '9D1114B1699010EA48E423E29A5A2230', 'Fiji', 'FJ', 'FJI', 242, '175', '-18', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(75, '399B225FF2EB615CD135A9871B151E48', 'Finland', 'FI', 'FIN', 246, '26', '64', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(76, '2335C76D39FD4ED8B904A4F5CD8837AF', 'France', 'FR', 'FRA', 250, '2', '46', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(77, '12F1EBBB10C7C615406F63E1C178257E', 'France, Metropolitan', 'FX', 'FXX', 249, '0', '0', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(78, '0A0AD032987924CEA749296725B062F5', 'French Guiana', 'GF', 'GUF', 254, '-53', '4', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(79, '70D6CC6BDA0C88ACFEE918729412DA28', 'French Polynesia', 'PF', 'PYF', 258, '-140', '-15', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(80, '8D3B77F8B79D9E2B3E6B3269DD5737B0', 'French Southern Territories', 'TF', 'ATF', 260, '67', '-43', 1, 0, 'AN', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(81, 'DEE685F1ED7B68ABB015BB81F9B478A7', 'Gabon', 'GA', 'GAB', 266, '11.4', '-1', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(82, '3DB768D70D35554A1B6777AD5B0D3C8F', 'Gambia', 'GM', 'GMB', 270, '-16.', '13.2', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(83, 'B8D2C497EE5E56C8599D832712C8F149', 'Georgia', 'GE', 'GEO', 268, '43.3', '42', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(84, 'A04405123AE5424B23FB419B7BE9E04D', 'Germany', 'DE', 'DEU', 276, '9', '51', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(85, '385B886F4C48C8C3E7C5CA9ED4F8F4F4', 'Ghana', 'GH', 'GHA', 288, '-2', '8', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(86, '5D14C518D265CBD5A075DAE13844400D', 'Gibraltar', 'GI', 'GIB', 292, '-5.2', '36.8', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(87, '0671D5834C0283E3DE2FB1989019DB2E', 'Greece', 'GR', 'GRC', 300, '22', '39', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(88, 'BC384F2B6CCBD8F8DA1C6F749632E2A5', 'Greenland', 'GL', 'GRL', 304, '-40', '72', 1, 0, 'NA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(89, '649109CCB37BB1A38C786441339D68D5', 'Grenada', 'GD', 'GRD', 308, '-61.', '12.0', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(90, '2C81EE5657C9D9351195D610723A95FF', 'Guadeloupe', 'GP', 'GLP', 312, '-61.', '16.1', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(91, 'BDC4FD82834CFDAACB4C9B26FFAB53E0', 'Guam', 'GU', 'GUM', 316, '144.', '13.2', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(92, '4E5521458402ABEC2559AD5AF917F947', 'Guatemala', 'GT', 'GTM', 320, '-90.', '15.3', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(93, '23D009631BF369B786C75A74C10ACF8B', 'Guinea', 'GN', 'GIN', 324, '-10', '11', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(94, 'BD39A758154F2B50CB99A8A3ECAC7C54', 'Guinea-Bissau', 'GW', 'GNB', 624, '-15', '12', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(95, '47FE4BE1E9EAA03C36A612007148DD31', 'Guyana', 'GY', 'GUY', 328, '-59', '5', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(96, 'EA2DFACBD95CAC0C4BEB166FCC2C6C8B', 'Haiti', 'HT', 'HTI', 332, '-72.', '19', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(97, 'CC94AF5FB2086BBBD08627940E29E1FF', 'Heard and McDonald Islands', 'HM', 'HMD', 334, '72.3', '-53.', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(98, '829CA49D896100B726966EA2DCF98C7B', 'Honduras', 'HN', 'HND', 340, '-86.', '15', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(99, '3321BA336A7E7C93B320C33B491384B3', 'Hong Kong', 'HK', 'HKG', 344, '114.', '22.1', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(100, '71464F1ABB0F293936361DDECB0F0942', 'Hungary', 'HU', 'HUN', 348, '20', '47', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(101, 'F25FA1B462BEB14075D81416A8200635', 'Iceland', 'IS', 'ISL', 352, '-18', '65', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(102, '7D11227916B5CB9FC04C579CEC11B55B', 'India', 'IN', 'IND', 356, '77', '20', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(103, 'B529E6EFC18458CFEE4CDD8CD58D37E1', 'Indonesia', 'ID', 'IDN', 360, '120', '-5', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(104, '2171390EA3E9E154AE3F0ADB4041AC5B', 'Iraq', 'IQ', 'IRQ', 368, '44', '33', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(105, '468392868E6838ADDEBD2FFB02307ABA', 'Ireland', 'IE', 'IRL', 372, '-8', '53', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(106, '4D7C4B500CDCEF15C4C4CA82AF1B763D', 'Islamic Republic of Iran', 'IR', 'IRN', 364, '53', '32', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(107, '069AF35E7980AE6507B4540ACEB700C5', 'Israel', 'IL', 'ISR', 376, '34.4', '31.3', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(108, '08F2692DDDE3076B68F90C96BF9584CB', 'Italy', 'IT', 'ITA', 380, '12.5', '42.5', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(109, '1887801A01F8CED1142F47CBAF71384B', 'Jamaica', 'JM', 'JAM', 388, '-77.', '18.1', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(110, 'B443B94344A6D1FF45E64E9824B1021B', 'Japan', 'JP', 'JPN', 392, '138', '36', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(111, '085B69BC9ABB953BBC10938CCBBF7940', 'Jordan', 'JO', 'JOR', 400, '36', '31', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(112, '96C64336F2F66E68BD9AAA338BCF8CF2', 'Kazakhstan', 'KZ', 'KAZ', 398, '68', '48', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(113, 'D287458E65C454E9EBD8130E4699DB48', 'Kenya', 'KE', 'KEN', 404, '38', '1', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(114, 'BFDA8B1F7740CDAD2DB1E48494E604FE', 'Kiribati', 'KI', 'KIR', 296, '173', '1.25', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(115, '8F7086294C761E442B2D09DDA023D544', 'Korea', 'KP', 'PRK', 408, '127', '40', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(116, '496BD0743C13E13CA9C517A2DCB6B48E', 'Korea, Republic of', 'KR', 'KOR', 410, '127.', '37', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(117, '41E45D36FE41D86E1F1DFA0C5CE11DB6', 'Kuwait', 'KW', 'KWT', 414, '45.4', '29.3', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(118, 'F443B3BE3C5C50AF93EE73CD129291AA', 'Kyrgyzstan', 'KG', 'KGZ', 417, '75', '41', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(119, '5A8CE48411106454ED90886EB7867CF3', 'Laos', 'LA', 'LAO', 418, '105', '18', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(120, '9AD980EC1801259EC5CDECEAF4A055EB', 'Latvia', 'LV', 'LVA', 428, '25', '57', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(121, '25FF10ACB18D7104E8953C2DFBE1F89F', 'Lebanon', 'LB', 'LBN', 422, '35.5', '33.5', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(122, 'ABE73C346C9ACD71C5FAAE9ABC923EA0', 'Lesotho', 'LS', 'LSO', 426, '28.3', '-29.', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(123, '07AA3262227A6CEECD48D5D1414BC9A3', 'Liberia', 'LR', 'LBR', 430, '-9.3', '6.3', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(124, '0734F56BD7002D871BF934CD78F0E5CC', 'Libyan Arab Jamahiriya', 'LY', 'LBY', 434, '17', '25', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(125, '4B379B0C46516A0602B281FCDBB87F1F', 'Liechtenstein', 'LI', 'LIE', 438, '9.32', '47.1', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(126, 'C78DE8E32CF1830AEADB85B2954E8EB3', 'Lithuania', 'LT', 'LTU', 440, '24', '56', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(127, '6095C906B48CCAA5B0970908548B1A06', 'Luxembourg', 'LU', 'LUX', 442, '6.1', '49.4', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(128, 'FA885B990244FCFCE754C8E7B4CF2537', 'Macau', 'MO', 'MAC', 446, '113.', '22.1', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(129, '0A497942A81F1EBECD8186F5A46BB27E', 'Macedonia', 'MK', 'MKD', 807, '22', '41.5', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(130, '230FC669B077CC2FA7D2F3A83EFDE202', 'Madagascar', 'MG', 'MDG', 450, '47', '-20', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(131, '96F6E5CA7C82E02D83D3DC0F5A92F746', 'Malawi', 'MW', 'MWI', 454, '34', '-13.', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(132, '9A401DD2913FEC3A9E3C59F184D1BE51', 'Malaysia', 'MY', 'MYS', 458, '112.', '2.3', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(133, '7C5FAD4D3105A9D46C5BBEF8E9AFF304', 'Maldives', 'MV', 'MDV', 462, '73', '3.15', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(134, '2ECF21F4321210732A6A128A86733ABF', 'Mali', 'ML', 'MLI', 466, '-4', '17', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(135, 'DEC6B136FEE81BF47B32D6A0A8679E76', 'Malta', 'MT', 'MLT', 470, '14.3', '35.5', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(136, 'AEC0C6532048052E2168DE3B91053C06', 'Marshall Islands', 'MH', 'MHL', 584, '168', '9', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(137, '6E886E590934EEAA00929E8C3F22D4D5', 'Martinique', 'MQ', 'MTQ', 474, '-61', '14.4', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(138, '728C08F901FFAFDDB3049981B9623154', 'Mauritania', 'MR', 'MRT', 478, '-12', '20', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(139, 'B6D9E86352A8B6CF34AB64D4C9551858', 'Mauritius', 'MU', 'MUS', 480, '57.3', '-20.', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(140, '79A14E676B7AC2E8C5AF9666FFFA2EF0', 'Mayotte', 'YT', 'MYT', 175, '45.1', '-12.', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(141, 'ABEC08E22E96D82F9C5CE34C899F486B', 'Mexico', 'MX', 'MEX', 484, '-102', '23', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(142, '7859C20CCBB92C88523CB509A15F3067', 'Micronesia', 'FM', 'FSM', 583, '158.', '6.55', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(143, '4AE8BEC0BF337424E3ABB54AAE2532AF', 'Moldova, Republic of', 'MD', 'MDA', 498, '29', '47', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(144, '7CFB7C7C7A3A5B6CD080FACCFB38A0AD', 'Monaco', 'MC', 'MCO', 492, '7.24', '43.4', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(145, '3B9CF63CBB58B58023567E3C01BC79C6', 'Mongolia', 'MN', 'MNG', 496, '105', '46', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(146, 'E2221B0D4E3674B420E31D64ABEFFF0C', 'Montenegro', 'ME', '-', 0, '0', '0', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(147, 'BBD1F5D73966EBC856E40CEB1B7C05DE', 'Montserrat', 'MS', 'MSR', 500, '-62.', '16.4', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(148, 'CD2C86B9DB501370C17AE8A5A8504672', 'Morocco', 'MA', 'MAR', 504, '-5', '32', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(149, '98F8D119ACE19C04AF2420AFC3892F17', 'Mozambique', 'MZ', 'MOZ', 508, '35', '-18.', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(150, '8350721EE900ABA07770422E76693559', 'Myanmar', 'MM', 'MMR', 104, '98', '22', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(151, 'A43CC1A8191FD2FAD10927FB35FBCDE7', 'Namibia', 'NA', 'NAM', 516, '17', '-22', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(152, 'DFA2707B3EB3A63B23BB89FF598CF512', 'Nauru', 'NR', 'NRU', 520, '166.', '-0.3', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(153, '6BE8FE93D02DA69384B04F1ECD2F96BC', 'Nepal', 'NP', 'NPL', 524, '84', '28', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(154, '78B8E205591D0A4D0790F3C2495BC2B9', 'Netherlands', 'NL', 'NLD', 528, '5.45', '52.3', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(155, 'C345FF225E7AF5C4F1208CC19D7AA507', 'Netherlands Antilles', 'AN', 'ANT', 530, '-68.', '12.1', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(156, 'A89A1528BC36B6C7DD592428E06A5AE4', 'New Caledonia', 'NC', 'NCL', 540, '165.', '-21.', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(157, 'C212ED5C75CBA88A33704C76CC8E08D6', 'New Zealand', 'NZ', 'NZL', 554, '174', '-41', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(158, '0392948B7BB062B03FA0DBC28E534F70', 'Nicaragua', 'NI', 'NIC', 558, '-85', '13', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(159, '6B0D6A58902A29FA7E3194C4E1B47323', 'Niger', 'NE', 'NER', 562, '8', '16', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(160, '297AFAB455BD333DD88C6D1952543A7C', 'Nigeria', 'NG', 'NGA', 566, '8', '10', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(161, 'C69C36C5C69383820BA2F3C7C9707B73', 'Niue', 'NU', 'NIU', 570, '-169', '-19.', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(162, '5681BB77933EE2570359C6A46FC2FDB8', 'Norfolk Island', 'NF', 'NFK', 574, '167.', '-29.', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(163, '5F4FD45EACE409C2784D51A5347E6500', 'Northern Mariana Islands', 'MP', 'MNP', 580, '145.', '15.1', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(164, 'EF4F2155CDD3559BC1ADD9617A1F06DC', 'Norway', 'NO', 'NOR', 578, '10', '62', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(165, '86DF10AB1A43E8741C521A557FCA7DF5', 'Oman', 'OM', 'OMN', 512, '57', '21', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(166, 'F68E2705CD07AA9AD7C5700E56CFF3F0', 'Pakistan', 'PK', 'PAK', 586, '70', '30', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(167, 'BA049E018ED9DC1E0618B6BC32DABB07', 'Palau', 'PW', 'PLW', 585, '134.', '7.3', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(168, '3152BFACA846CFF62A6BCF404F79A86D', 'Palestine Authority', 'PS', 'PSE', 275, '34.9', '31.8', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(169, '9A62BD01939B1F18AF36589971DEFB21', 'Panama', 'PA', 'PAN', 591, '-80', '9', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(170, 'ACA00CC0B719DFA3717C3AB8FFC31A50', 'Papua New Guinea', 'PG', 'PNG', 598, '147', '-6', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(171, '09304BA4360FA6C0135E8F01F0F645E3', 'Paraguay', 'PY', 'PRY', 600, '-58', '-23', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(172, 'C2AC98FA90A68CC0EAB08FDBBA6F546C', 'Peru', 'PE', 'PER', 604, '-76', '-10', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(173, '84685A3C777FC6FEE60E42D26EDF1046', 'Philippines', 'PH', 'PHL', 608, '122', '13', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(174, '65FF6D1266F18B848A04B7C4535AEE50', 'Pitcairn', 'PN', 'PCN', 612, '-130', '-25.', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(175, 'FBAE44D85EFF324277179C2443B2BCE7', 'Poland', 'PL', 'POL', 616, '20', '52', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(176, 'F94440A64BE68C131D69FC0A226B83B1', 'Portugal', 'PT', 'PRT', 620, '-8', '39.3', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(177, 'AF3C80760364D71BAC0E5931B81E02C0', 'Puerto Rico', 'PR', 'PRI', 630, '-66.', '18.1', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(178, '82E87DB1E19DA0C2715E8787C2EF79E0', 'Qatar', 'QA', 'QAT', 634, '51.1', '25.3', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(179, 'C6F7CC1EE40FF5FAA22C253AC65ACF91', 'Republic of Serbia', 'RS', '', 0, '21.0', '44.0', 1, 0, '', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(180, '43866D9CCECE7598BF9B8F072F027339', 'Reunion', 'RE', 'REU', 638, '55.3', '-21.', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(181, '0D6866E752AC3487F8E5F2DE25B851AB', 'Romania', 'RO', 'ROU', 642, '25', '46', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(182, '0A327B31D2529A541FDC895D45E12C0C', 'Russian Federation', 'RU', 'RUS', 643, '100', '60', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(183, '1F6F0C01DAC630CE4FD4B5DAD27579A1', 'Rwanda', 'RW', 'RWA', 646, '30', '-2', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(184, 'E62340587333F128C00F0CBEE3590F7E', 'Saint Lucia', 'LC', 'LCA', 662, '-60.', '13.5', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(185, 'A5CAA6303A0B5BADFDCE1299F4322C37', 'Samoa', 'WS', 'WSM', 882, '-172', '-13.', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(186, '9E275CC9C0313413D7771122FD13C00E', 'San Marino', 'SM', 'SMR', 674, '12.2', '43.4', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(187, '34BC11137A5C03B91A413B652C72EF9B', 'Sao Tome and Principe', 'ST', 'STP', 678, '7', '1', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(188, 'A4E7060641BFF744CF641EEBDA88183A', 'Saudi Arabia', 'SA', 'SAU', 682, '45', '25', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(189, 'CD1FAA54E12FE3660B021A4BDB30B6DB', 'Senegal', 'SN', 'SEN', 686, '-14', '14', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(190, 'F21EB5E026F1ECDD516051FD52BCB97E', 'Serbia', 'CS', 'SCG', 891, '21.4', '43.5', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(191, '91FD72C9287B3CCF834AF20B4B55665B', 'Seychelles', 'SC', 'SYC', 690, '55.4', '-4.3', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(192, 'C0F19314646F4FE0829E8E5FFFC25279', 'Sierra Leone', 'SL', 'SLE', 694, '-11.', '8.3', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(193, '6275CE2C58DE4854E6EFACE811198755', 'Singapore', 'SG', 'SGP', 702, '103.', '1.22', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(194, '35EA9D24358D2A2FA0233BE3448E4D93', 'Slovakia', 'SK', 'SVK', 703, '19.3', '48.4', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(195, '3A8CD608B5AFBAA2CDEBB7A5B7DCB320', 'Slovenia', 'SI', 'SVN', 705, '14.4', '46.0', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(196, 'C116DBF2628432C38FF20462C22B6275', 'Solomon Islands', 'SB', 'SLB', 90, '159', '-8', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(197, '4AE2CA275A16F17E2B8249E32D4CD0FF', 'Somalia', 'SO', 'SOM', 706, '49', '10', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(198, 'F9628F7772799ABBFAC146179F3A2F6D', 'South Africa', 'ZA', 'ZAF', 710, '24', '-29', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(199, '138914B947E5F586E1DF9B1B53558D27', 'Spain', 'ES', 'ESP', 724, '-4', '40', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(200, 'D9F65A2A0C0CC5057E7F0A47A1FE6C9F', 'Sri Lanka', 'LK', 'LKA', 144, '81', '7', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(201, '7C5650A0A22700CF748194D03E565A21', 'St. Helena', 'SH', 'SHN', 654, '-5.4', '-15.', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(202, '3261E457409BF8EEB44F2449D1548264', 'St. Kitts and Nevis', 'KN', 'KNA', 659, '-62.', '17.2', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(203, 'B6D9DC026D9D010F40788804F5423A76', 'St. Pierre and Miquelon', 'PM', 'SPM', 666, '-56.', '46.5', 1, 0, 'NA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(204, '98AD1C0B7F0CAE72484863DF73470C42', 'St. Vincent and the Grenadines', 'VC', 'VCT', 670, '-61.', '13.1', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(205, 'F19534DCDCD45012267693789B43A6DB', 'Sudan', 'SD', 'SDN', 736, '30', '15', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(206, 'E121BCD2FBF70DB7864F69CB0825FB4D', 'Suriname', 'SR', 'SUR', 740, '-56', '4', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(207, 'F26B358484EAF2C6AA8CA12F2329852B', 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', 744, '20', '78', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(208, '6C0F1A8DD3F6FC7975CE662ABB74B79B', 'Swaziland', 'SZ', 'SWZ', 748, '31.3', '-26.', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(209, '70B40468BDCFDEB80A5F56F839879A2B', 'Sweden', 'SE', 'SWE', 752, '15', '62', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(210, '62E71FDB35BAB92BA02FC97B810CCC04', 'Switzerland', 'CH', 'CHE', 756, '8', '47', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(211, '3B964DDDC080C7F99283FA21FE656CA3', 'Syrian Arab Republic', 'SY', 'SYR', 760, '38', '35', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(212, '8E5EF2442FD623AB74AF3ECDBAA7E4C0', 'Taiwan', 'TW', 'TWN', 158, '121', '23.3', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(213, '1D55446B7151ED81A2CD8AABA8DF1BC0', 'Tajikistan', 'TJ', 'TJK', 762, '71', '39', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(214, 'B2FBAEF3D316A6340429AEDDB7D1AE48', 'Tanzania, United Republic of', 'TZ', 'TZA', 834, '35', '-6', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(215, '478AA95F82ACE2C413EB44045E0F7AFD', 'Thailand', 'TH', 'THA', 764, '100', '15', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(216, '41FF2206DCA6EB42FE262E1F925C6855', 'Togo', 'TG', 'TGO', 768, '1.1', '8', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(217, 'A172AC71068E5E5EDCE92837543C723F', 'Tokelau', 'TK', 'TKL', 772, '-172', '-9', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(218, 'C5901714134530D03D104790E76F82FE', 'Tonga', 'TO', 'TON', 776, '-175', '-20', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(219, '584AA4A5E4E61ABC79D0A32D28AC7D0F', 'Trinidad and Tobago', 'TT', 'TTO', 780, '-61', '11', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(220, '60F86CA44810E4489D363DDD1CD59DA1', 'Tunisia', 'TN', 'TUN', 788, '9', '34', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(221, '2285B8224605F1DAEC529C4D1C0AD5F9', 'Turkey', 'TR', 'TUR', 792, '35', '39', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(222, '3FE91C812E059FFED31FE3B42CFE2817', 'Turkmenistan', 'TM', 'TKM', 795, '60', '40', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(223, 'B07B62E73D11DE8EF157F9CDA7E47154', 'Turks and Caicos Islands', 'TC', 'TCA', 796, '-71.', '21.4', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(224, '6296DEF97C35AD61F01B9EF08DCD93E1', 'Tuvalu', 'TV', 'TUV', 798, '178', '-8', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(225, '5E868342D555FD4E52063B6D45CFF237', 'Uganda', 'UG', 'UGA', 800, '32', '1', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(226, '0E2A1E47381F38F909BD7A9F75A4ACB8', 'Ukraine', 'UA', 'UKR', 804, '32', '49', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(227, 'B3AC330BD007F5B3F3B702ED57E6E73A', 'United Arab Emirates', 'AE', 'ARE', 784, '54', '24', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(228, 'CB7568F59E01C23774DABAB934622205', 'United Kingdom (Great Britain)', 'GB', 'GBR', 826, '-2', '54', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(229, '8F681267B16436AD212D1F653A77925A', 'United States', 'US', 'USA', 840, '97', '38', 1, 0, 'NA', 100, 0, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(230, 'DFD81CDBE3236CA74E154913D3D9A2CF', 'United States Virgin Islands', 'VI', 'VIR', 850, '-64.', '18.2', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(231, 'F7B2E4B60007098BFCA6F1D20A56C3BE', 'Uruguay', 'UY', 'URY', 858, '-56', '-33', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(232, '16958EE169E9DB484BEE01CFD6385CD5', 'Uzbekistan', 'UZ', 'UZB', 860, '64', '41', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(233, 'B20B75F5029F578193C98E3A113B56EA', 'Vanuatu', 'VU', 'VUT', 548, '167', '-16', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(234, 'F4655B61BB306F58D38B0FF4DAC4388F', 'Vatican City State', 'VA', 'VAT', 336, '12.2', '41.5', 1, 0, 'EU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(235, '6EA092CFB1D7DB7CDAA88A26A5764873', 'Venezuela', 'VE', 'VEN', 862, '-66', '8', 1, 0, 'LA', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(236, 'C5E9546F24D98A304E6656F764A639B3', 'Viet Nam', 'VN', 'VNM', 704, '106', '16', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(237, '9E4BD76C54CCA9A393054C24E67BB774', 'Wallis And Futuna Islands', 'WF', 'WLF', 876, '-176', '-13.', 1, 0, 'AU', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(238, 'FEC383341D36168299E8B68D77179F84', 'Western Sahara', 'EH', 'ESH', 732, '-13', '24.3', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(239, '970496EEE90991C00AC21AE62B849ADC', 'Yemen', 'YE', 'YEM', 887, '48', '15', 1, 0, 'AS', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(240, '1894883ADDF47131C7CE08AD11802EFD', 'Zaire', 'ZR', 'ZAR', 180, '0', '0', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(241, '857E7A3AAE2AD6EC841C7BEF18C49B39', 'Zambia', 'ZM', 'ZMB', 894, '30', '-15', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(242, '98DA5F7A41E9AEBDC6DA3FDBB74EAC19', 'Zimbabwe', 'ZW', 'ZWE', 716, '30', '20', 1, 0, 'AF', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(249, 'E1B2387099D05981508436E29CC5EA51', 'aaaaaaaaaaaa', '', '', 0, '', '', 1, 1, '', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(250, '7F8C40ACEDBB6D36ADE06DDDC6187DBC', 'aaaaaaaaaaaa', '', '', 0, '', '', 1, 1, '', 100, 1, '', '', '', '', '');
INSERT INTO `ps4_countries` VALUES(251, '27CA4346D3252F47A7C60E955E3FA3EA', '1223333222', '', '', 0, '', '', 1, 1, '', 100, 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ps4_credits`
--

CREATE TABLE `ps4_credits` (
  `credit_id` int(6) NOT NULL AUTO_INCREMENT,
  `ucredit_id` varchar(40) NOT NULL,
  `name` varchar(250) NOT NULL,
  `price` decimal(14,4) NOT NULL,
  `credits` int(10) NOT NULL,
  `description` text NOT NULL,
  `item_code` varchar(200) NOT NULL,
  `notes` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `homepage` tinyint(1) NOT NULL DEFAULT '0',
  `everyone` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `sortorder` int(6) NOT NULL,
  `name_dutch` varchar(255) NOT NULL,
  `description_dutch` text NOT NULL,
  `name_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL,
  `name_german` varchar(255) NOT NULL,
  `description_german` text NOT NULL,
  `name_spanish` varchar(255) NOT NULL,
  `description_spanish` text NOT NULL,
  `name_english` varchar(255) NOT NULL,
  `description_english` text NOT NULL,
  UNIQUE KEY `credit_id` (`credit_id`),
  KEY `active` (`active`,`featured`,`homepage`,`everyone`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_credits`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_currencies`
--

CREATE TABLE `ps4_currencies` (
  `currency_id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `code` varchar(4) CHARACTER SET latin1 NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `defaultcur` tinyint(1) NOT NULL DEFAULT '0',
  `denotation` varchar(10) CHARACTER SET latin1 NOT NULL,
  `decimal_separator` varchar(2) CHARACTER SET latin1 NOT NULL,
  `thousands_separator` varchar(2) CHARACTER SET latin1 NOT NULL,
  `decimal_places` tinyint(1) NOT NULL DEFAULT '0',
  `pos_num_format` tinyint(1) NOT NULL DEFAULT '0',
  `neg_num_format` tinyint(1) NOT NULL DEFAULT '0',
  `exchange_rate` varchar(10) NOT NULL DEFAULT '1',
  `exchange_date` datetime NOT NULL,
  `exchange_updater` tinyint(1) NOT NULL DEFAULT '0',
  `exchange_autoupdate` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `currency_id` (`currency_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `ps4_currencies`
--

INSERT INTO `ps4_currencies` VALUES(1, 'United States Dollar', 'USD', 1, 1, '$', '.', ',', 2, 1, 7, '1', '2012-01-18 23:00:27', 1, 0, 0);
INSERT INTO `ps4_currencies` VALUES(3, 'Canadian Dollar', 'CAD', 0, 0, '$', '.', ',', 2, 1, 7, '1', '2012-01-18 23:01:53', 2, 1, 0);
INSERT INTO `ps4_currencies` VALUES(5, 'Australian Dollar', 'AUD', 0, 0, '$', '.', ',', 2, 1, 7, '1', '2012-01-18 23:01:54', 2, 0, 0);
INSERT INTO `ps4_currencies` VALUES(7, 'Danish Krone', 'DKK', 0, 0, 'DKK', '.', ',', 2, 1, 7, '1', '2012-01-18 23:01:54', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(8, 'European Euro', 'EUR', 0, 0, '&euro;', ',', '.', 2, 1, 6, '1', '2012-01-18 23:01:54', 1, 0, 0);
INSERT INTO `ps4_currencies` VALUES(9, 'British Pound', 'GBP', 0, 0, '&pound;', '.', ',', 2, 1, 7, '1', '2012-01-18 23:01:54', 1, 0, 0);
INSERT INTO `ps4_currencies` VALUES(10, 'Hong Kong Dollar', 'HKD', 0, 0, '$', '.', ',', 2, 1, 7, '1', '2012-01-18 23:01:54', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(11, 'Israeli Sheqel', 'ILS', 0, 0, '&#8362;', '.', ',', 2, 1, 7, '1', '2012-01-18 23:01:54', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(12, 'Japanese Yen', 'JPY', 0, 0, '&yen;', '.', ',', 0, 1, 7, '1', '2012-01-18 23:01:55', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(13, 'Norwegian Krone', 'NOK', 0, 0, 'NOK', '.', ',', 2, 1, 7, '1', '2012-01-18 23:01:59', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(14, 'New Zealand Dollar', 'NZD', 0, 0, '$', '.', ',', 2, 1, 7, '1', '2011-09-21 15:47:41', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(15, 'Swedish Krona', 'SEK', 0, 0, 'SEK', '.', ',', 2, 1, 7, '1', '2011-09-21 15:47:41', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(67, 'Mexican Peso', 'MXN', 0, 0, 'MXN', '.', ',', 2, 1, 7, '1', '0000-00-00 00:00:00', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(66, 'South African Rands', 'ZAR', 0, 0, 'R', '.', ',', 2, 1, 7, '1', '0000-00-00 00:00:00', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(65, 'Czech Koruna', 'CZK', 0, 0, 'CZK', '.', ',', 2, 1, 7, '1', '0000-00-00 00:00:00', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(64, 'Hungarian Forint', 'HUF', 0, 0, 'HUF', '.', ',', 2, 1, 7, '1', '0000-00-00 00:00:00', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(63, 'Polish Zloty', 'PLN', 0, 0, 'PLN', '.', ',', 2, 1, 7, '1', '0000-00-00 00:00:00', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(62, 'Singapore Dollar', 'SGD', 0, 0, '$', '.', ',', 2, 1, 7, '1', '0000-00-00 00:00:00', 0, 0, 0);
INSERT INTO `ps4_currencies` VALUES(61, 'Switzerland Franc', 'CHF', 0, 0, 'CHF', '.', ',', 2, 1, 7, '1', '0000-00-00 00:00:00', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ps4_digital_sizes`
--

CREATE TABLE `ps4_digital_sizes` (
  `ds_id` int(6) NOT NULL AUTO_INCREMENT,
  `uds_id` varchar(40) NOT NULL,
  `dsp_type` varchar(10) NOT NULL DEFAULT 'photo',
  `item_code` varchar(200) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dspgroups` varchar(255) NOT NULL,
  `perm` text NOT NULL,
  `size` int(6) NOT NULL,
  `price` varchar(10) NOT NULL,
  `price_calc` varchar(10) NOT NULL,
  `credits` varchar(10) NOT NULL,
  `credits_calc` varchar(10) NOT NULL,
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `license` varchar(10) NOT NULL DEFAULT 'rf',
  `rm_license` int(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `quantity` int(8) NOT NULL DEFAULT '9999',
  `sortorder` int(3) NOT NULL DEFAULT '0',
  `fps` varchar(10) NOT NULL,
  `hd` tinyint(1) NOT NULL DEFAULT '0',
  `format` varchar(250) NOT NULL,
  `running_time` varchar(250) NOT NULL,
  `width` int(7) NOT NULL,
  `height` int(7) NOT NULL,
  `real_sizes` tinyint(1) NOT NULL DEFAULT '0',
  `everyone` tinyint(1) NOT NULL DEFAULT '0',
  `commission` int(3) NOT NULL,
  `commission_type` tinyint(1) NOT NULL DEFAULT '1',
  `commission_dollar` float NOT NULL,
  `min_contr_price` float NOT NULL,
  `max_contr_price` float NOT NULL,
  `min_contr_credits` int(10) NOT NULL,
  `max_contr_credits` int(10) NOT NULL,
  `attachment` varchar(20) NOT NULL,
  `all_galleries` tinyint(1) NOT NULL DEFAULT '0',
  `force_list` tinyint(1) NOT NULL DEFAULT '0',
  `delivery_method` tinyint(1) NOT NULL DEFAULT '0',
  `contr_sell` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name_english` varchar(255) NOT NULL,
  `name_russian` varchar(255) NOT NULL,
  `name_spanish` varchar(255) NOT NULL,
  `name_dutch` varchar(255) NOT NULL,
  `name_french` varchar(255) NOT NULL,
  `name_german` varchar(255) NOT NULL,
  `name_` varchar(255) NOT NULL,
  `watermark` text NOT NULL,
  UNIQUE KEY `ds_id` (`ds_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_digital_sizes`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_discount_ranges`
--

CREATE TABLE `ps4_discount_ranges` (
  `dr_id` int(6) NOT NULL AUTO_INCREMENT,
  `item_type` varchar(16) NOT NULL,
  `item_id` int(6) NOT NULL,
  `start_discount_number` int(3) NOT NULL,
  `discount_percent` int(2) NOT NULL,
  UNIQUE KEY `dr_id` (`dr_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ps4_discount_ranges`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_downloads`
--

CREATE TABLE `ps4_downloads` (
  `dl_id` int(6) NOT NULL AUTO_INCREMENT,
  `mem_id` int(5) NOT NULL,
  `asset_id` int(6) NOT NULL,
  `customize_id` int(4) NOT NULL DEFAULT '0',
  `dsp_id` int(5) NOT NULL,
  `dl_date` datetime NOT NULL,
  `dl_type` varchar(20) NOT NULL,
  `dl_type_id` int(10) NOT NULL,
  `ip` varchar(30) NOT NULL,
  UNIQUE KEY `dl_id` (`dl_id`),
  KEY `mem_id` (`mem_id`),
  KEY `asset_id` (`asset_id`),
  KEY `dsp_id` (`dsp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_downloads`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_folders`
--

CREATE TABLE `ps4_folders` (
  `folder_id` int(8) NOT NULL AUTO_INCREMENT,
  `ufolder_id` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `storage_id` int(4) NOT NULL,
  `owner` int(6) NOT NULL DEFAULT '0',
  `folder_notes` text NOT NULL,
  `encrypted` tinyint(1) NOT NULL DEFAULT '0',
  `enc_name` varchar(40) NOT NULL,
  UNIQUE KEY `folder_id` (`folder_id`),
  KEY `storage_id` (`storage_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_folders`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_galleries`
--

CREATE TABLE `ps4_galleries` (
  `gallery_id` int(6) NOT NULL AUTO_INCREMENT,
  `ugallery_id` varchar(40) NOT NULL DEFAULT '',
  `name` varchar(250) NOT NULL DEFAULT '',
  `owner` int(8) NOT NULL DEFAULT '0',
  `album` tinyint(1) NOT NULL DEFAULT '0',
  `publicgal` tinyint(1) NOT NULL DEFAULT '0',
  `parent_gal` int(6) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `pass_protected` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `edited` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active_type` tinyint(1) NOT NULL DEFAULT '0',
  `active_date` datetime NOT NULL,
  `expire_type` tinyint(1) NOT NULL DEFAULT '0',
  `expire_date` datetime NOT NULL,
  `event_details` tinyint(1) NOT NULL DEFAULT '0',
  `event_date` datetime NOT NULL,
  `event_location` text NOT NULL,
  `event_code` varchar(250) NOT NULL,
  `client_name` varchar(250) NOT NULL,
  `allow_uploads` tinyint(1) NOT NULL DEFAULT '0',
  `dsorting` varchar(20) NOT NULL,
  `dsorting2` varchar(10) NOT NULL,
  `everyone` tinyint(1) NOT NULL DEFAULT '0',
  `icon` int(6) NOT NULL,
  `password` varchar(250) NOT NULL,
  `nowatermark` tinyint(1) NOT NULL DEFAULT '0',
  `icon_id` int(7) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `gallery_count` int(6) NOT NULL DEFAULT '0',
  `name_english` varchar(255) NOT NULL DEFAULT '',
  `description_english` text NOT NULL,
  `name_spanish` varchar(255) NOT NULL DEFAULT '',
  `description_spanish` text NOT NULL,
  `folder_name` varchar(255) NOT NULL,
  `folder_path` text NOT NULL,
  `storage_path` int(2) NOT NULL DEFAULT '0',
  `name_russian` varchar(255) NOT NULL,
  `description_russian` text NOT NULL,
  `name_dutch` varchar(255) NOT NULL,
  `description_dutch` text NOT NULL,
  `name_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL,
  `name_german` varchar(255) NOT NULL,
  `description_german` text NOT NULL,
  `sort_number` int(7) NOT NULL DEFAULT '0',
  `feature` tinyint(1) NOT NULL,
  UNIQUE KEY `gallery_id` (`gallery_id`),
  KEY `storage_path` (`storage_path`),
  KEY `owner` (`owner`),
  KEY `parent_gal` (`parent_gal`),
  KEY `icon_id` (`icon_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_galleries`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_groupids`
--

CREATE TABLE `ps4_groupids` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `mgrarea` varchar(30) NOT NULL,
  `item_id` int(6) NOT NULL,
  `group_id` int(6) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `item_id` (`item_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_groupids`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_groups`
--

CREATE TABLE `ps4_groups` (
  `gr_id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `flagtype` varchar(15) NOT NULL DEFAULT 'flag.none.gif',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `mgrarea` varchar(50) NOT NULL,
  UNIQUE KEY `id` (`gr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_groups`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_invoices`
--

CREATE TABLE `ps4_invoices` (
  `invoice_id` int(8) NOT NULL AUTO_INCREMENT,
  `invoice_number` varchar(200) NOT NULL,
  `order_id` int(8) NOT NULL,
  `bill_id` int(8) NOT NULL,
  `invoice_mem_id` int(8) NOT NULL,
  `inv_f_name` varchar(100) NOT NULL,
  `inv_l_name` varchar(100) NOT NULL,
  `invoice_date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `ship_id` int(4) NOT NULL,
  `incstats` tinyint(1) NOT NULL DEFAULT '1',
  `exchange_rate` decimal(8,8) NOT NULL,
  `total` decimal(10,4) NOT NULL,
  `credits_total` int(8) NOT NULL,
  `subtotal` decimal(10,4) NOT NULL,
  `taxa_cost` decimal(10,4) NOT NULL,
  `taxb_cost` decimal(10,4) NOT NULL,
  `taxc_cost` decimal(10,4) NOT NULL,
  `tax_ratea` decimal(3,3) NOT NULL,
  `tax_rateb` decimal(3,3) NOT NULL,
  `tax_ratec` decimal(3,3) NOT NULL,
  `tax_summary` text NOT NULL,
  `discounts_total` decimal(10,4) NOT NULL,
  `discounts_credits_total` int(5) NOT NULL,
  `tracking_number` varchar(100) NOT NULL,
  `shipping_cost` decimal(10,4) NOT NULL,
  `ship_name` varchar(250) NOT NULL,
  `ship_email` varchar(200) NOT NULL,
  `ship_status` tinyint(1) NOT NULL DEFAULT '0',
  `ship_address` varchar(255) NOT NULL,
  `bill_email` varchar(200) NOT NULL,
  `bill_name` varchar(250) NOT NULL,
  `bill_address` varchar(255) NOT NULL,
  `bill_address2` varchar(255) NOT NULL,
  `bill_city` varchar(255) NOT NULL,
  `bill_country` int(6) NOT NULL,
  `bill_state` int(6) NOT NULL,
  `bill_zip` varchar(100) NOT NULL,
  `bill_phone` varchar(255) NOT NULL,
  `ship_address2` varchar(255) NOT NULL,
  `ship_city` varchar(255) NOT NULL,
  `ship_country` int(6) NOT NULL,
  `ship_state` int(6) NOT NULL,
  `ship_country_readable` varchar(50) NOT NULL,
  `ship_state_readable` varchar(50) NOT NULL,
  `bill_country_readable` varchar(50) NOT NULL,
  `bill_state_readable` varchar(50) NOT NULL,
  `ship_zip` varchar(100) NOT NULL,
  `ship_phone` varchar(255) NOT NULL,
  `payment_status` tinyint(1) NOT NULL DEFAULT '2',
  `payment_date` datetime NOT NULL,
  `shipping_status` tinyint(1) NOT NULL DEFAULT '0',
  `discount_ids_used` varchar(100) NOT NULL,
  `customer_currency` int(4) NOT NULL,
  `shippable` tinyint(1) NOT NULL DEFAULT '0',
  `post_vars` text NOT NULL,
  `shipping_summary` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `taxid` varchar(100) NOT NULL,
  `cart_notes` text NOT NULL,
  `tax_digratea` decimal(3,3) NOT NULL,
  `tax_digrateb` decimal(3,3) NOT NULL,
  `tax_digratec` decimal(3,3) NOT NULL,
  UNIQUE KEY `invoice_id` (`invoice_id`),
  KEY `order_id` (`order_id`),
  KEY `bill_id` (`bill_id`),
  KEY `invoice_mem_id` (`invoice_mem_id`),
  KEY `ship_id` (`ship_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_invoices`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_invoice_items`
--

CREATE TABLE `ps4_invoice_items` (
  `oi_id` int(14) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(14) NOT NULL,
  `item_added` datetime NOT NULL,
  `asset_id` int(8) NOT NULL,
  `item_id` int(14) NOT NULL,
  `item_type` varchar(16) NOT NULL,
  `physical_item` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL,
  `taxed` tinyint(1) NOT NULL DEFAULT '0',
  `quantity` int(4) NOT NULL,
  `price_total` decimal(10,4) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `credits_total` int(8) NOT NULL DEFAULT '0',
  `credits` int(8) NOT NULL,
  `downloads` int(8) NOT NULL DEFAULT '0',
  `expires` datetime NOT NULL,
  `tracking_number` varchar(100) NOT NULL,
  `shipping_status` tinyint(1) NOT NULL DEFAULT '0',
  `contributor_id` int(8) NOT NULL,
  `has_options` tinyint(1) NOT NULL DEFAULT '0',
  `pack_invoice_id` int(6) NOT NULL DEFAULT '0',
  `item_list_number` int(4) NOT NULL DEFAULT '0',
  `package_media_needed` int(3) NOT NULL DEFAULT '0',
  `package_media_filled` int(3) NOT NULL DEFAULT '0',
  `paytype` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `cart_item_notes` text NOT NULL,
  `rm_selections` text NOT NULL,
  UNIQUE KEY `oi_id` (`oi_id`),
  KEY `invoice_id` (`invoice_id`),
  KEY `asset_id` (`asset_id`),
  KEY `item_id` (`item_id`),
  KEY `pack_invoice_id` (`pack_invoice_id`),
  KEY `contributor_id` (`contributor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_invoice_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_invoice_options`
--

CREATE TABLE `ps4_invoice_options` (
  `io_id` int(10) NOT NULL AUTO_INCREMENT,
  `option_id` int(6) NOT NULL,
  `option_gid` int(6) NOT NULL,
  `invoice_item_id` int(6) NOT NULL,
  `item_list_number` int(8) NOT NULL DEFAULT '0',
  `option_price` decimal(10,4) NOT NULL,
  `option_price_calc` varchar(6) NOT NULL,
  `option_credits` int(8) NOT NULL,
  `option_credits_calc` varchar(6) NOT NULL,
  UNIQUE KEY `io_id` (`io_id`),
  KEY `option_id` (`option_id`),
  KEY `option_gid` (`option_gid`),
  KEY `invoice_item_id` (`invoice_item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_invoice_options`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_item_galleries`
--

CREATE TABLE `ps4_item_galleries` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `mgrarea` varchar(30) NOT NULL,
  `item_id` int(6) NOT NULL,
  `gallery_id` int(6) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `item_id` (`item_id`),
  KEY `gallery_id` (`gallery_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_item_galleries`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_item_photos`
--

CREATE TABLE `ps4_item_photos` (
  `ip_id` int(6) NOT NULL AUTO_INCREMENT,
  `item_id` int(6) NOT NULL DEFAULT '0',
  `mgrarea` varchar(14) NOT NULL,
  UNIQUE KEY `ip_id` (`ip_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_item_photos`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_keywords`
--

CREATE TABLE `ps4_keywords` (
  `key_id` int(14) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  `media_id` int(8) NOT NULL,
  `language` varchar(30) NOT NULL,
  `member_id` int(6) NOT NULL DEFAULT '0',
  `posted` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `memtag` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `key_id` (`key_id`),
  KEY `media_id` (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_keywords`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_lab_contacts`
--

CREATE TABLE `ps4_lab_contacts` (
  `lab_id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  UNIQUE KEY `lab_id` (`lab_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_lab_contacts`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_licenses`
--

CREATE TABLE `ps4_licenses` (
  `license_id` int(6) NOT NULL AUTO_INCREMENT,
  `lic_name` varchar(250) NOT NULL,
  `lic_purchase_type` varchar(10) NOT NULL,
  `lic_description` text NOT NULL,
  `lic_locked` tinyint(1) NOT NULL DEFAULT '0',
  `rm_base_price` decimal(10,4) NOT NULL,
  `rm_base_credits` int(7) NOT NULL DEFAULT '0',
  `rm_base_type` varchar(2) NOT NULL DEFAULT 'mp',
  `attachlicense` int(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`license_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `ps4_licenses`
--

INSERT INTO `ps4_licenses` VALUES(1, 'Royalty Free', 'rf', '', 1, '0.0000', 0, 'mp', 0);
INSERT INTO `ps4_licenses` VALUES(3, 'Contact Us', 'cu', '', 1, '0.0000', 0, 'mp', 0);
INSERT INTO `ps4_licenses` VALUES(4, 'Free', 'fr', '', 1, '0.0000', 0, 'mp', 0);
INSERT INTO `ps4_licenses` VALUES(5, 'Editorial Use', 'rf', '', 1, '0.0000', 0, 'mp', 0);
INSERT INTO `ps4_licenses` VALUES(6, 'Extended', 'rf', '', 1, '0.0000', 0, 'mp', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ps4_lightboxes`
--

CREATE TABLE `ps4_lightboxes` (
  `lightbox_id` int(7) NOT NULL AUTO_INCREMENT,
  `ulightbox_id` varchar(40) NOT NULL,
  `member_id` int(7) NOT NULL DEFAULT '0',
  `umember_id` varchar(40) NOT NULL,
  `name` varchar(250) NOT NULL,
  `nest` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `notes` text NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `guest` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `lbx_id` (`lightbox_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_lightboxes`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_lightbox_items`
--

CREATE TABLE `ps4_lightbox_items` (
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `lb_id` int(10) NOT NULL DEFAULT '0',
  `media_id` int(10) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `notes` text NOT NULL,
  UNIQUE KEY `item_id` (`item_id`),
  KEY `lb_id` (`lb_id`),
  KEY `media_id` (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_lightbox_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media`
--

CREATE TABLE `ps4_media` (
  `media_id` int(14) NOT NULL AUTO_INCREMENT,
  `umedia_id` varchar(40) NOT NULL,
  `owner` int(8) NOT NULL DEFAULT '0',
  `width` int(6) NOT NULL,
  `height` int(6) NOT NULL,
  `dsp_type` varchar(16) NOT NULL,
  `hd` tinyint(1) NOT NULL DEFAULT '0',
  `fps` decimal(10,4) NOT NULL,
  `format` varchar(250) NOT NULL,
  `running_time` varchar(20) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `filesize` int(20) NOT NULL,
  `filename` varchar(200) NOT NULL,
  `ofilename` varchar(200) NOT NULL,
  `external_link` text NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `file_ext` varchar(10) NOT NULL,
  `folder_id` int(8) NOT NULL DEFAULT '0',
  `batch_id` int(14) NOT NULL,
  `license` varchar(4) NOT NULL,
  `rm_license` int(4) NOT NULL,
  `quantity` varchar(10) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `credits` int(10) NOT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `sortorder` int(8) NOT NULL DEFAULT '0',
  `views` int(10) NOT NULL DEFAULT '0',
  `downloads` int(10) NOT NULL,
  `approval_status` tinyint(1) NOT NULL DEFAULT '1',
  `approval_message` text NOT NULL,
  `approval_date` datetime NOT NULL,
  `model_release_status` tinyint(1) NOT NULL DEFAULT '0',
  `model_release_form` varchar(50) NOT NULL,
  `prop_release_status` tinyint(1) NOT NULL DEFAULT '0',
  `prop_release_form` varchar(100) NOT NULL,
  `copyright` text NOT NULL,
  `usage_restrictions` text NOT NULL,
  `title_dutch` varchar(250) NOT NULL,
  `description_dutch` text NOT NULL,
  `title_french` varchar(250) NOT NULL,
  `description_french` text NOT NULL,
  `title_german` varchar(250) NOT NULL,
  `description_german` text NOT NULL,
  `title_spanish` varchar(250) NOT NULL,
  `description_spanish` text NOT NULL,
  `title_english` varchar(250) NOT NULL,
  `description_english` text NOT NULL,
  UNIQUE KEY `media_id` (`media_id`),
  KEY `owner` (`owner`),
  KEY `folder_id` (`folder_id`),
  KEY `batch_id` (`batch_id`),
  KEY `featured` (`featured`,`active`,`approval_status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_collections`
--

CREATE TABLE `ps4_media_collections` (
  `mc_id` int(10) NOT NULL AUTO_INCREMENT,
  `cmedia_id` int(10) NOT NULL,
  `coll_id` int(4) NOT NULL,
  UNIQUE KEY `mc_id` (`mc_id`),
  KEY `cmedia_id` (`cmedia_id`),
  KEY `coll_id` (`coll_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_collections`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_comments`
--

CREATE TABLE `ps4_media_comments` (
  `mc_id` int(6) NOT NULL AUTO_INCREMENT,
  `member_id` int(6) NOT NULL,
  `media_id` int(6) NOT NULL,
  `posted` datetime NOT NULL,
  `comment` text NOT NULL,
  `language` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `mc_id` (`mc_id`),
  KEY `member_id` (`member_id`),
  KEY `media_id` (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_comments`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_container`
--

CREATE TABLE `ps4_media_container` (
  `mc_id` int(8) NOT NULL AUTO_INCREMENT,
  `umc_id` varchar(40) NOT NULL,
  `title` varchar(255) NOT NULL,
  `views` int(7) NOT NULL,
  `storage_id` int(4) NOT NULL,
  `folder_id` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  `land_port` varchar(4) NOT NULL COMMENT 'Landscape of portrait',
  UNIQUE KEY `mc_id` (`mc_id`),
  KEY `storage_id` (`storage_id`),
  KEY `folder_id` (`folder_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_container`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_digital_sizes`
--

CREATE TABLE `ps4_media_digital_sizes` (
  `mds_id` int(8) NOT NULL AUTO_INCREMENT,
  `ds_id` int(8) NOT NULL,
  `dsgrp_id` int(4) NOT NULL,
  `media_id` int(8) NOT NULL,
  `license` varchar(10) NOT NULL,
  `rm_license` int(4) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `credits` int(10) NOT NULL,
  `price_calc` varchar(6) NOT NULL,
  `credits_calc` varchar(6) NOT NULL,
  `customized` tinyint(1) NOT NULL DEFAULT '0',
  `external_link` text NOT NULL,
  `quantity` varchar(10) NOT NULL,
  `width` varchar(20) NOT NULL,
  `height` varchar(20) NOT NULL,
  `format` varchar(20) NOT NULL,
  `hd` tinyint(1) NOT NULL DEFAULT '0',
  `running_time` varchar(20) NOT NULL,
  `fps` varchar(20) NOT NULL,
  `auto_create` tinyint(1) NOT NULL DEFAULT '0',
  `filename` varchar(250) NOT NULL,
  `ofilename` varchar(200) NOT NULL,
  `dsp_downloads` int(10) NOT NULL,
  UNIQUE KEY `mds_id` (`mds_id`),
  KEY `ds_id` (`ds_id`),
  KEY `media_id` (`media_id`),
  KEY `dsgrp_id` (`dsgrp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_digital_sizes`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_dsp_temp`
--

CREATE TABLE `ps4_media_dsp_temp` (
  `tmpid` int(8) NOT NULL AUTO_INCREMENT,
  `media_id` int(8) NOT NULL,
  `dsp_id` int(8) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `ofilename` varchar(200) NOT NULL,
  UNIQUE KEY `tmpid` (`tmpid`),
  KEY `media_id` (`media_id`),
  KEY `dsp_id` (`dsp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `ps4_media_dsp_temp`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_exif`
--

CREATE TABLE `ps4_media_exif` (
  `exif_id` int(10) NOT NULL AUTO_INCREMENT,
  `media_id` int(10) NOT NULL,
  `FileName` varchar(255) NOT NULL,
  `FileDateTime` varchar(255) NOT NULL,
  `FileSize` varchar(255) NOT NULL,
  `FileType` varchar(255) NOT NULL,
  `MimeType` varchar(255) NOT NULL,
  `SectionsFound` varchar(255) NOT NULL,
  `ImageDescription` varchar(255) NOT NULL,
  `Make` varchar(255) NOT NULL,
  `Model` varchar(255) NOT NULL,
  `Orientation` varchar(255) NOT NULL,
  `XResolution` varchar(255) NOT NULL,
  `YResolution` varchar(255) NOT NULL,
  `ResolutionUnit` varchar(255) NOT NULL,
  `Software` varchar(255) NOT NULL,
  `DateTime` varchar(255) NOT NULL,
  `YCbCrPositioning` varchar(255) NOT NULL,
  `Exif_IFD_Pointer` varchar(255) NOT NULL,
  `GPS_IFD_Pointer` varchar(255) NOT NULL,
  `ExposureTime` varchar(255) NOT NULL,
  `FNumber` varchar(255) NOT NULL,
  `ExposureProgram` varchar(255) NOT NULL,
  `ISOSpeedRatings` varchar(255) NOT NULL,
  `ExifVersion` varchar(255) NOT NULL,
  `DateTimeOriginal` varchar(255) NOT NULL,
  `DateTimeDigitized` varchar(255) NOT NULL,
  `ComponentsConfiguration` varchar(255) NOT NULL,
  `ShutterSpeedValue` varchar(255) NOT NULL,
  `ApertureValue` varchar(255) NOT NULL,
  `MeteringMode` varchar(255) NOT NULL,
  `Flash` varchar(255) NOT NULL,
  `FocalLength` varchar(255) NOT NULL,
  `FlashPixVersion` varchar(255) NOT NULL,
  `ColorSpace` varchar(255) NOT NULL,
  `ExifImageWidth` varchar(255) NOT NULL,
  `ExifImageLength` varchar(255) NOT NULL,
  `SensingMethod` varchar(255) NOT NULL,
  `ExposureMode` varchar(255) NOT NULL,
  `WhiteBalance` varchar(255) NOT NULL,
  `SceneCaptureType` varchar(255) NOT NULL,
  `Sharpness` varchar(255) NOT NULL,
  `GPSLatitudeRef` varchar(255) NOT NULL,
  `GPSLatitude_0` varchar(255) NOT NULL,
  `GPSLatitude_1` varchar(255) NOT NULL,
  `GPSLatitude_2` varchar(255) NOT NULL,
  `GPSLongitudeRef` varchar(255) NOT NULL,
  `GPSLongitude_0` varchar(255) NOT NULL,
  `GPSLongitude_1` varchar(255) NOT NULL,
  `GPSLongitude_2` varchar(255) NOT NULL,
  `GPSTimeStamp_0` varchar(255) NOT NULL,
  `GPSTimeStamp_1` varchar(255) NOT NULL,
  `GPSTimeStamp_2` varchar(255) NOT NULL,
  `GPSImgDirectionRef` varchar(255) NOT NULL,
  `GPSImgDirection` varchar(255) NOT NULL,
  UNIQUE KEY `exif_id` (`exif_id`),
  KEY `media_id` (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_exif`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_galleries`
--

CREATE TABLE `ps4_media_galleries` (
  `mg_id` int(20) NOT NULL AUTO_INCREMENT,
  `gmedia_id` int(10) NOT NULL,
  `gallery_id` int(10) NOT NULL,
  UNIQUE KEY `mg_id` (`mg_id`),
  KEY `gmedia_id` (`gmedia_id`),
  KEY `gallery_id` (`gallery_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_galleries`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_iptc`
--

CREATE TABLE `ps4_media_iptc` (
  `iptc_id` int(14) NOT NULL AUTO_INCREMENT,
  `media_id` int(14) NOT NULL,
  `description` text NOT NULL,
  `title` varchar(250) NOT NULL,
  `instructions` text NOT NULL,
  `date_created` datetime NOT NULL,
  `author` varchar(250) NOT NULL,
  `creator_title` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `country` varchar(250) NOT NULL,
  `job_identifier` varchar(250) NOT NULL,
  `headline` varchar(250) NOT NULL,
  `provider` varchar(250) NOT NULL,
  `source` varchar(250) NOT NULL,
  `description_writer` varchar(250) NOT NULL,
  `urgency` varchar(250) NOT NULL,
  `copyright_notice` text NOT NULL,
  UNIQUE KEY `iptc_id` (`iptc_id`),
  KEY `media_id` (`media_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `ps4_media_iptc`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_packages`
--

CREATE TABLE `ps4_media_packages` (
  `mp_id` int(10) NOT NULL AUTO_INCREMENT,
  `pack_id` int(4) NOT NULL,
  `media_id` int(10) NOT NULL,
  `packgrp_id` int(4) NOT NULL,
  UNIQUE KEY `mp_id` (`mp_id`),
  KEY `pack_id` (`pack_id`),
  KEY `media_id` (`media_id`),
  KEY `packgrp_id` (`packgrp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `ps4_media_packages`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_prints`
--

CREATE TABLE `ps4_media_prints` (
  `mp_id` int(10) NOT NULL AUTO_INCREMENT,
  `media_id` int(10) NOT NULL,
  `print_id` int(6) NOT NULL,
  `printgrp_id` int(4) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `price_calc` varchar(10) NOT NULL,
  `credits` int(10) NOT NULL,
  `credits_calc` varchar(10) NOT NULL,
  `customized` tinyint(1) NOT NULL,
  `quantity` varchar(10) NOT NULL,
  UNIQUE KEY `mp_id` (`mp_id`),
  KEY `media_id` (`media_id`),
  KEY `print_id` (`print_id`),
  KEY `printgrp_id` (`printgrp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_prints`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_products`
--

CREATE TABLE `ps4_media_products` (
  `mp_id` int(10) NOT NULL AUTO_INCREMENT,
  `media_id` int(10) NOT NULL,
  `prod_id` int(6) NOT NULL,
  `prodgrp_id` int(4) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `price_calc` varchar(10) NOT NULL,
  `credits` int(10) NOT NULL,
  `credits_calc` varchar(10) NOT NULL,
  `customized` tinyint(1) NOT NULL,
  `quantity` varchar(10) NOT NULL,
  UNIQUE KEY `mp_id` (`mp_id`),
  KEY `media_id` (`media_id`),
  KEY `prod_id` (`prod_id`),
  KEY `prodgrp_id` (`prodgrp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `ps4_media_products`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_ratings`
--

CREATE TABLE `ps4_media_ratings` (
  `mr_id` int(6) NOT NULL AUTO_INCREMENT,
  `member_id` int(6) NOT NULL,
  `media_id` int(6) NOT NULL,
  `posted` datetime NOT NULL,
  `rating` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `mr_id` (`mr_id`),
  KEY `member_id` (`member_id`),
  KEY `media_id` (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_ratings`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_samples`
--

CREATE TABLE `ps4_media_samples` (
  `ms_id` int(3) NOT NULL AUTO_INCREMENT,
  `sample_width` int(4) NOT NULL,
  `sample_height` int(4) NOT NULL,
  `sample_filesize` int(10) NOT NULL,
  `media_id` int(14) NOT NULL,
  `sample_filename` varchar(255) NOT NULL,
  UNIQUE KEY `ms_id` (`ms_id`),
  KEY `media_id` (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_samples`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_tags`
--

CREATE TABLE `ps4_media_tags` (
  `mt_id` int(14) NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) NOT NULL,
  `media_id` int(8) NOT NULL,
  `language` varchar(40) NOT NULL,
  `member_id` int(6) NOT NULL,
  `posted` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `mt_id` (`mt_id`),
  KEY `media_id` (`media_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_tags`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_thumbnails`
--

CREATE TABLE `ps4_media_thumbnails` (
  `mt_id` int(3) NOT NULL AUTO_INCREMENT,
  `media_id` int(14) NOT NULL,
  `thumbtype` varchar(10) NOT NULL,
  `thumb_filename` varchar(255) NOT NULL,
  `thumb_width` int(4) NOT NULL,
  `thumb_height` int(4) NOT NULL,
  `thumb_filesize` int(10) NOT NULL,
  UNIQUE KEY `mt_id` (`mt_id`),
  KEY `media_id` (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_thumbnails`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_types`
--

CREATE TABLE `ps4_media_types` (
  `mt_id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `name_dutch` varchar(200) NOT NULL,
  `name_french` varchar(200) NOT NULL,
  `name_german` varchar(200) NOT NULL,
  `name_spanish` varchar(200) NOT NULL,
  `name_english` varchar(200) NOT NULL,
  UNIQUE KEY `mt_id` (`mt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `ps4_media_types`
--

INSERT INTO `ps4_media_types` VALUES(3, 'Videos', 1, '', '', '', '', '');
INSERT INTO `ps4_media_types` VALUES(4, 'Photos', 1, '', '', '', '', '');
INSERT INTO `ps4_media_types` VALUES(5, 'Vectors', 1, '', '', '', '', '');
INSERT INTO `ps4_media_types` VALUES(12, 'Other Files', 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_types_ref`
--

CREATE TABLE `ps4_media_types_ref` (
  `mtr_id` int(10) NOT NULL AUTO_INCREMENT,
  `media_id` int(10) NOT NULL,
  `mt_id` int(4) NOT NULL,
  UNIQUE KEY `mtr_id` (`mtr_id`),
  KEY `media_id` (`media_id`),
  KEY `mt_id` (`mt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=142 ;

--
-- Dumping data for table `ps4_media_types_ref`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_vidsamples`
--

CREATE TABLE `ps4_media_vidsamples` (
  `mvs_id` int(6) NOT NULL AUTO_INCREMENT,
  `media_id` int(6) NOT NULL,
  `vidsampletype` varchar(10) NOT NULL,
  `vidsample_filename` varchar(250) NOT NULL,
  `vidsample_width` int(6) NOT NULL,
  `vidsample_height` int(6) NOT NULL,
  `vidsample_filesize` int(20) NOT NULL,
  `vidsample_extension` varchar(10) NOT NULL,
  UNIQUE KEY `mvs_id` (`mvs_id`),
  KEY `media_id` (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_vidsamples`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_media_xmp`
--

CREATE TABLE `ps4_media_xmp` (
  `xmp_id` int(14) NOT NULL AUTO_INCREMENT,
  `mc_id` int(14) NOT NULL,
  UNIQUE KEY `xmp_id` (`xmp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_media_xmp`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_members`
--

CREATE TABLE `ps4_members` (
  `mem_id` int(6) NOT NULL AUTO_INCREMENT,
  `membership` int(3) NOT NULL DEFAULT '1',
  `umem_id` varchar(40) NOT NULL DEFAULT '',
  `f_name` varchar(50) NOT NULL DEFAULT '',
  `l_name` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(250) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `website` varchar(250) NOT NULL DEFAULT '',
  `notes` text NOT NULL,
  `comp_name` varchar(250) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL DEFAULT '',
  `signup_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_activity` datetime NOT NULL,
  `credits` int(6) NOT NULL DEFAULT '0',
  `avatar` tinyint(1) NOT NULL DEFAULT '0',
  `avatar_status` tinyint(1) NOT NULL DEFAULT '0',
  `avatar_updated` datetime NOT NULL,
  `ip_signup` varchar(40) NOT NULL,
  `ip_login` varchar(40) NOT NULL,
  `referrer` varchar(250) NOT NULL,
  `compay` tinyint(1) NOT NULL DEFAULT '0',
  `paypal_email` varchar(200) NOT NULL,
  `bill_me_later` tinyint(1) NOT NULL DEFAULT '0',
  `com_source` tinyint(1) NOT NULL DEFAULT '1',
  `com_level` int(3) NOT NULL,
  `ms_end_date` datetime NOT NULL,
  `bio_content` text NOT NULL,
  `bio_updated` datetime NOT NULL,
  `bio_status` tinyint(1) NOT NULL DEFAULT '0',
  `language` varchar(20) NOT NULL,
  `currency` int(4) NOT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `customized_date` tinyint(1) NOT NULL DEFAULT '0',
  `time_zone` varchar(4) NOT NULL,
  `daylight_savings` tinyint(1) NOT NULL DEFAULT '0',
  `date_format` varchar(8) NOT NULL,
  `date_display` varchar(8) NOT NULL,
  `clock_format` varchar(8) NOT NULL,
  `number_date_sep` varchar(8) NOT NULL,
  `trialed_memberships` text NOT NULL,
  `used_memberships` text NOT NULL,
  `fee_memberships` text NOT NULL,
  `display_name` varchar(100) NOT NULL,
  `showcase` tinyint(1) NOT NULL DEFAULT '0',
  `profile_views` int(8) NOT NULL DEFAULT '0',
  `taxid` varchar(100) NOT NULL,
  `uploader` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `mem_id` (`mem_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_members`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_memberships`
--

CREATE TABLE `ps4_memberships` (
  `ms_id` int(4) NOT NULL AUTO_INCREMENT,
  `allow_uploads` tinyint(1) NOT NULL DEFAULT '0',
  `msfeatured` tinyint(1) NOT NULL DEFAULT '0',
  `disk_space` int(8) NOT NULL DEFAULT '9999',
  `personal_galleries` tinyint(1) NOT NULL DEFAULT '0',
  `admin_galleries` tinyint(1) NOT NULL DEFAULT '0',
  `editing` tinyint(1) NOT NULL DEFAULT '0',
  `editing_approval` tinyint(1) NOT NULL DEFAULT '0',
  `deleting` tinyint(1) NOT NULL DEFAULT '0',
  `deleting_approval` tinyint(1) NOT NULL DEFAULT '0',
  `fs_min` int(6) NOT NULL DEFAULT '0',
  `fs_max` int(6) NOT NULL DEFAULT '100',
  `res_min` int(6) NOT NULL DEFAULT '0',
  `res_max` int(6) NOT NULL DEFAULT '10000',
  `file_types` varchar(255) NOT NULL,
  `portfolio` tinyint(1) NOT NULL DEFAULT '0',
  `searches` tinyint(1) NOT NULL DEFAULT '0',
  `ums_id` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `notes` text NOT NULL,
  `mstype` varchar(10) NOT NULL,
  `trail_status` tinyint(1) NOT NULL DEFAULT '0',
  `trial_length_num` int(6) NOT NULL,
  `trial_length_period` varchar(10) NOT NULL,
  `setupfee` varchar(8) NOT NULL,
  `price` varchar(8) NOT NULL,
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `period` varchar(14) NOT NULL,
  `sortorder` int(5) NOT NULL,
  `flagtype` varchar(15) NOT NULL DEFAULT 'flag.none.gif',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `everyone` tinyint(1) NOT NULL DEFAULT '0',
  `gallery_suggestions` tinyint(1) NOT NULL DEFAULT '0',
  `media_requests` tinyint(1) NOT NULL DEFAULT '0',
  `bio` tinyint(1) NOT NULL DEFAULT '0',
  `bio_approval` tinyint(1) NOT NULL DEFAULT '0',
  `avatar` tinyint(1) NOT NULL DEFAULT '0',
  `avatar_approval` tinyint(1) NOT NULL DEFAULT '0',
  `lightboxes` tinyint(1) NOT NULL DEFAULT '0',
  `lightboxes_num` int(6) NOT NULL DEFAULT '10',
  `comments` tinyint(1) NOT NULL DEFAULT '0',
  `comment_approval` tinyint(1) NOT NULL DEFAULT '0',
  `rating` tinyint(1) NOT NULL DEFAULT '0',
  `rating_approval` tinyint(1) NOT NULL DEFAULT '0',
  `tagging` tinyint(1) NOT NULL DEFAULT '0',
  `tagging_approval` tinyint(1) NOT NULL DEFAULT '0',
  `allow_selling` tinyint(1) NOT NULL DEFAULT '0',
  `commission` int(3) NOT NULL DEFAULT '0',
  `contr_digital` tinyint(1) NOT NULL DEFAULT '0',
  `admin_products` tinyint(1) NOT NULL DEFAULT '0',
  `admin_prints` tinyint(1) NOT NULL DEFAULT '0',
  `additional_sizes` tinyint(1) NOT NULL DEFAULT '0',
  `collections` tinyint(1) NOT NULL DEFAULT '0',
  `approval` tinyint(1) NOT NULL DEFAULT '0',
  `contr_col` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `signin_groups` varchar(255) NOT NULL,
  `name_english` varchar(255) NOT NULL,
  `name_russian` varchar(255) NOT NULL,
  `name_spanish` varchar(255) NOT NULL,
  `name_dutch` varchar(255) NOT NULL,
  `name_french` varchar(255) NOT NULL,
  `name_german` varchar(255) NOT NULL,
  `description_dutch` text NOT NULL,
  `description_french` text NOT NULL,
  `description_german` text NOT NULL,
  `description_spanish` text NOT NULL,
  `description_english` text NOT NULL,
  UNIQUE KEY `ms_id` (`ms_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ps4_memberships`
--

INSERT INTO `ps4_memberships` VALUES(1, 0, 0, 9999, 0, 0, 0, 0, 0, 0, 0, 100, 0, 10000, '', 0, 0, '08909AC3B2F181653766BC5B1D9D6E56', 'Basic Membership', '', '', 'free', 0, 0, 'days', '0.00', '0.00', 0, 'weekly', 0, 'icon.none.gif', 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ps4_members_address`
--

CREATE TABLE `ps4_members_address` (
  `add_id` int(8) NOT NULL AUTO_INCREMENT,
  `member_id` int(8) NOT NULL,
  `address` varchar(250) NOT NULL,
  `address_2` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` int(4) NOT NULL,
  `postal_code` varchar(20) NOT NULL,
  `country` int(4) NOT NULL,
  `primary1` tinyint(1) NOT NULL DEFAULT '0',
  `primary2` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `add_id` (`add_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_members_address`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_memsubs`
--

CREATE TABLE `ps4_memsubs` (
  `msub_id` int(5) NOT NULL AUTO_INCREMENT,
  `mem_id` int(5) NOT NULL,
  `sub_id` int(5) NOT NULL,
  `expires` datetime NOT NULL,
  `start_date` datetime NOT NULL,
  `perday` varchar(5) NOT NULL,
  `total_downloads` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  UNIQUE KEY `msub_id` (`msub_id`),
  KEY `mem_id` (`mem_id`),
  KEY `sub_id` (`sub_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_memsubs`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_news`
--

CREATE TABLE `ps4_news` (
  `news_id` int(6) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `short` text NOT NULL,
  `article` text NOT NULL,
  `homepage` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `add_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expire_type` tinyint(1) NOT NULL DEFAULT '0',
  `expire_date` datetime NOT NULL,
  `views` int(6) NOT NULL DEFAULT '0',
  `newsgroups` varchar(255) NOT NULL,
  `sortorder` int(5) NOT NULL,
  `title_english` varchar(255) NOT NULL,
  `short_english` text NOT NULL,
  `article_english` text NOT NULL,
  `title_russian` varchar(255) NOT NULL,
  `short_russian` text NOT NULL,
  `article_russian` text NOT NULL,
  `title_spanish` varchar(255) NOT NULL,
  `short_spanish` text NOT NULL,
  `article_spanish` text NOT NULL,
  `title_dutch` varchar(255) NOT NULL,
  `short_dutch` text NOT NULL,
  `article_dutch` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `short_french` text NOT NULL,
  `article_french` text NOT NULL,
  `title_german` varchar(255) NOT NULL,
  `short_german` text NOT NULL,
  `article_german` text NOT NULL,
  `title_` varchar(255) NOT NULL,
  `short_` text NOT NULL,
  `article_` text NOT NULL,
  UNIQUE KEY `news_id` (`news_id`),
  KEY `homepage` (`homepage`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_news`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_options`
--

CREATE TABLE `ps4_options` (
  `op_id` int(5) NOT NULL AUTO_INCREMENT,
  `uop_id` varchar(40) NOT NULL,
  `parent_id` int(5) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `price_mod` varchar(5) NOT NULL,
  `credits` int(10) NOT NULL,
  `credits_mod` varchar(5) NOT NULL,
  `my_cost` varchar(10) NOT NULL,
  `add_weight` float NOT NULL,
  `sortorder` int(3) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name_english` varchar(255) NOT NULL,
  `name_russian` varchar(255) NOT NULL,
  `name_spanish` varchar(255) NOT NULL,
  `name_dutch` varchar(255) NOT NULL,
  `name_french` varchar(255) NOT NULL,
  `name_german` varchar(255) NOT NULL,
  `name_` varchar(255) NOT NULL,
  UNIQUE KEY `op_id` (`op_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_options`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_option_grp`
--

CREATE TABLE `ps4_option_grp` (
  `og_id` int(5) NOT NULL AUTO_INCREMENT,
  `uog_id` varchar(40) NOT NULL,
  `parent_id` int(5) NOT NULL DEFAULT '0',
  `parent_type` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ltype` tinyint(1) NOT NULL,
  `sortorder` int(3) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name_english` varchar(255) NOT NULL,
  `name_russian` varchar(255) NOT NULL,
  `name_spanish` varchar(255) NOT NULL,
  `name_dutch` varchar(255) NOT NULL,
  `name_french` varchar(255) NOT NULL,
  `name_german` varchar(255) NOT NULL,
  UNIQUE KEY `og_id` (`og_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_option_grp`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_orders`
--

CREATE TABLE `ps4_orders` (
  `order_id` int(7) NOT NULL AUTO_INCREMENT,
  `uorder_id` varchar(50) NOT NULL DEFAULT '',
  `order_number` int(20) NOT NULL,
  `member_id` int(8) NOT NULL,
  `bill_id` int(8) NOT NULL DEFAULT '0',
  `order_f_name` varchar(200) NOT NULL,
  `order_l_name` varchar(200) NOT NULL,
  `order_date` datetime NOT NULL,
  `order_status` int(1) NOT NULL DEFAULT '2',
  `admin_notes` text NOT NULL,
  `mem_notes` text NOT NULL,
  `checkout_lang` varchar(20) NOT NULL,
  `processed_credits_used` tinyint(1) NOT NULL DEFAULT '0',
  `processed_credits_gained` tinyint(1) NOT NULL DEFAULT '0',
  `processed_quantities` tinyint(1) NOT NULL DEFAULT '0',
  `processed_coupons` tinyint(1) NOT NULL DEFAULT '0',
  `processed_subs` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `order_id` (`order_id`),
  KEY `member_id` (`member_id`),
  KEY `bill_id` (`bill_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_orders`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_packages`
--

CREATE TABLE `ps4_packages` (
  `pack_id` int(4) NOT NULL AUTO_INCREMENT,
  `upack_id` varchar(40) NOT NULL,
  `description` text NOT NULL,
  `discount` float NOT NULL,
  `discountc` int(8) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `pack_code` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `homepage` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `sortorder` int(3) NOT NULL DEFAULT '0',
  `weight` float NOT NULL,
  `price` varchar(10) NOT NULL,
  `my_cost` varchar(10) NOT NULL,
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `all_galleries` tinyint(1) NOT NULL DEFAULT '0',
  `credits` int(10) NOT NULL,
  `quantity` varchar(10) NOT NULL,
  `shipping` int(3) NOT NULL,
  `everyone` tinyint(1) NOT NULL DEFAULT '0',
  `notify_lab` int(6) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  `addshipping` decimal(10,4) NOT NULL,
  `allowoptions` tinyint(1) NOT NULL DEFAULT '0',
  `attachment` varchar(20) NOT NULL,
  `item_name_dutch` varchar(255) NOT NULL,
  `item_name_english` varchar(255) NOT NULL,
  `item_name_german` varchar(255) NOT NULL,
  `item_name_spanish` varchar(255) NOT NULL,
  `item_name_french` varchar(255) NOT NULL,
  `name_dutch` varchar(255) NOT NULL,
  `name_english` varchar(255) NOT NULL,
  `name_german` varchar(255) NOT NULL,
  `name_spanish` varchar(255) NOT NULL,
  `name_french` varchar(255) NOT NULL,
  `description_dutch` text NOT NULL,
  `description_english` text NOT NULL,
  `description_german` text NOT NULL,
  `description_spanish` text NOT NULL,
  `description_french` text NOT NULL,
  UNIQUE KEY `pack_id` (`pack_id`),
  KEY `active` (`active`,`homepage`,`featured`,`deleted`,`everyone`),
  KEY `quantity` (`quantity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_packages`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_package_items`
--

CREATE TABLE `ps4_package_items` (
  `pi_id` int(6) NOT NULL AUTO_INCREMENT,
  `pack_id` int(6) NOT NULL,
  `item_type` varchar(6) NOT NULL,
  `item_id` int(6) NOT NULL,
  `iquantity` int(6) NOT NULL,
  `groupmult` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `pi_id` (`pi_id`),
  KEY `pack_id` (`pack_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_package_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_paymentgateways`
--

CREATE TABLE `ps4_paymentgateways` (
  `gateway` varchar(250) NOT NULL,
  `setting` varchar(250) NOT NULL,
  `value` varchar(250) NOT NULL,
  `langvar` varchar(250) NOT NULL,
  `sortorder` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ps4_paymentgateways`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_perms`
--

CREATE TABLE `ps4_perms` (
  `perm_id` int(6) NOT NULL AUTO_INCREMENT,
  `perm_area` varchar(20) NOT NULL,
  `perm_value` varchar(20) NOT NULL,
  `item_id` int(6) NOT NULL,
  UNIQUE KEY `perm_id` (`perm_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_perms`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_prints`
--

CREATE TABLE `ps4_prints` (
  `print_id` int(5) NOT NULL AUTO_INCREMENT,
  `uprint_id` varchar(40) NOT NULL,
  `description` text NOT NULL,
  `sortorder` int(3) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_code` varchar(255) NOT NULL,
  `lab_code` varchar(255) NOT NULL,
  `price` varchar(10) NOT NULL DEFAULT '0',
  `price_calc` varchar(4) NOT NULL,
  `discount_calc` varchar(4) NOT NULL,
  `discountc_calc` varchar(4) NOT NULL,
  `my_cost` varchar(10) NOT NULL,
  `credits` int(10) NOT NULL,
  `credits_calc` varchar(4) NOT NULL,
  `weight` varchar(6) NOT NULL,
  `addshipping` decimal(10,4) NOT NULL,
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `multiple` tinyint(1) NOT NULL DEFAULT '0',
  `shipping` int(3) NOT NULL,
  `discount` varchar(10) NOT NULL,
  `discountc` int(8) NOT NULL,
  `commission` int(3) NOT NULL DEFAULT '0',
  `contr_sell` tinyint(1) NOT NULL DEFAULT '0',
  `commission_type` tinyint(1) NOT NULL DEFAULT '1',
  `commission_dollar` float NOT NULL,
  `ptype` varchar(4) NOT NULL DEFAULT 'ps',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `homepage` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  `notify_lab` int(6) NOT NULL DEFAULT '0',
  `everyone` tinyint(1) NOT NULL DEFAULT '0',
  `attachment` varchar(20) NOT NULL,
  `all_galleries` tinyint(1) NOT NULL DEFAULT '0',
  `min_contr_price` float NOT NULL,
  `max_contr_price` float NOT NULL,
  `min_contr_credits` int(10) NOT NULL,
  `max_contr_credits` int(10) NOT NULL,
  `item_name_english` varchar(255) NOT NULL,
  `item_name_russian` varchar(255) NOT NULL,
  `item_name_spanish` varchar(255) NOT NULL,
  `item_name_dutch` varchar(255) NOT NULL,
  `item_name_french` varchar(255) NOT NULL,
  `item_name_german` varchar(255) NOT NULL,
  `item_name_` varchar(255) NOT NULL,
  `description_dutch` text NOT NULL,
  `description_english` text NOT NULL,
  `description_german` text NOT NULL,
  `description_spanish` text NOT NULL,
  `description_french` text NOT NULL,
  UNIQUE KEY `print_id` (`print_id`),
  KEY `active` (`active`,`homepage`,`featured`,`deleted`,`everyone`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_prints`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_products`
--

CREATE TABLE `ps4_products` (
  `prod_id` int(5) NOT NULL AUTO_INCREMENT,
  `uprod_id` varchar(40) NOT NULL,
  `description` text NOT NULL,
  `sortorder` int(3) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_code` varchar(255) NOT NULL,
  `lab_code` varchar(255) NOT NULL,
  `price` varchar(10) NOT NULL DEFAULT '0',
  `price_calc` varchar(4) NOT NULL,
  `discount_calc` varchar(4) NOT NULL,
  `discountc_calc` varchar(4) NOT NULL,
  `my_cost` varchar(10) NOT NULL,
  `credits` int(10) NOT NULL,
  `credits_calc` varchar(4) NOT NULL,
  `weight` varchar(6) NOT NULL,
  `addshipping` decimal(10,4) NOT NULL,
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `multiple` tinyint(1) NOT NULL DEFAULT '0',
  `shipping` int(3) NOT NULL,
  `discount` varchar(10) NOT NULL,
  `discountc` int(8) NOT NULL,
  `contr_sell` tinyint(1) NOT NULL DEFAULT '0',
  `commission` int(3) NOT NULL DEFAULT '0',
  `commission_type` tinyint(1) NOT NULL DEFAULT '1',
  `commission_dollar` float NOT NULL,
  `ptype` varchar(4) NOT NULL DEFAULT 'ps',
  `quantity` varchar(8) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `homepage` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `notify_lab` int(6) NOT NULL DEFAULT '0',
  `everyone` tinyint(1) NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  `min_contr_price` float NOT NULL,
  `max_contr_price` float NOT NULL,
  `min_contr_credits` int(10) NOT NULL,
  `max_contr_credits` int(10) NOT NULL,
  `product_type` tinyint(1) NOT NULL DEFAULT '1',
  `all_galleries` tinyint(1) NOT NULL DEFAULT '0',
  `attachment` varchar(20) NOT NULL,
  `item_name_dutch` varchar(255) NOT NULL,
  `description_dutch` text NOT NULL,
  `item_name_english` varchar(255) NOT NULL,
  `description_english` text NOT NULL,
  `item_name_german` varchar(255) NOT NULL,
  `description_german` text NOT NULL,
  `item_name_spanish` varchar(255) NOT NULL,
  `description_spanish` text NOT NULL,
  `item_name_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL,
  UNIQUE KEY `prod_id` (`prod_id`),
  KEY `active` (`active`,`homepage`,`featured`,`deleted`,`everyone`),
  KEY `quantity` (`quantity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_products`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_promotions`
--

CREATE TABLE `ps4_promotions` (
  `promo_id` int(5) NOT NULL AUTO_INCREMENT,
  `upromo_id` varchar(40) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `sortorder` int(3) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  `everyone` tinyint(1) NOT NULL DEFAULT '0',
  `promo_code` varchar(50) NOT NULL,
  `oneuse` tinyint(1) NOT NULL DEFAULT '0',
  `autoapply` tinyint(1) NOT NULL DEFAULT '0',
  `minpurchase` decimal(10,4) NOT NULL,
  `quantity` int(6) NOT NULL,
  `promotype` varchar(14) NOT NULL,
  `peroff` int(3) NOT NULL,
  `dollaroff` decimal(10,4) NOT NULL,
  `bulkbuy` int(5) NOT NULL,
  `bulktype` varchar(10) NOT NULL,
  `bulkfree` int(5) NOT NULL,
  `homepage` tinyint(1) NOT NULL DEFAULT '0',
  `cartpage` tinyint(1) NOT NULL DEFAULT '0',
  `promopage` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `name_dutch` varchar(255) NOT NULL,
  `name_french` varchar(255) NOT NULL,
  `name_german` varchar(255) NOT NULL,
  `name_spanish` varchar(255) NOT NULL,
  `name_english` varchar(255) NOT NULL,
  `description_dutch` text NOT NULL,
  `description_french` text NOT NULL,
  `description_german` text NOT NULL,
  `description_spanish` text NOT NULL,
  `description_english` text NOT NULL,
  UNIQUE KEY `promo_id` (`promo_id`),
  KEY `active` (`active`,`everyone`,`quantity`,`homepage`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_promotions`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_regionids`
--

CREATE TABLE `ps4_regionids` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `mgrarea` varchar(50) NOT NULL,
  `reg_type` varchar(5) NOT NULL,
  `reg_id` int(5) NOT NULL,
  `item_id` int(5) NOT NULL,
  UNIQUE KEY `sr_id` (`id`),
  KEY `item_id` (`item_id`),
  KEY `reg_id` (`reg_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `ps4_regionids`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_registration_form`
--

CREATE TABLE `ps4_registration_form` (
  `rf_id` int(4) NOT NULL AUTO_INCREMENT,
  `field_id` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `custom` tinyint(1) NOT NULL DEFAULT '0',
  `field_name` varchar(255) NOT NULL,
  UNIQUE KEY `rf_id` (`rf_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ps4_registration_form`
--

INSERT INTO `ps4_registration_form` VALUES(1, 'formAddress', 1, 0, '');
INSERT INTO `ps4_registration_form` VALUES(2, 'formPhone', 1, 0, '');
INSERT INTO `ps4_registration_form` VALUES(3, 'formCompanyName', 1, 0, '');
INSERT INTO `ps4_registration_form` VALUES(4, 'formWebsite', 1, 0, '');
INSERT INTO `ps4_registration_form` VALUES(5, 'formSignupAgreement', 2, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ps4_rm_options`
--

CREATE TABLE `ps4_rm_options` (
  `op_id` int(6) NOT NULL AUTO_INCREMENT,
  `og_id` int(4) NOT NULL,
  `license_id` int(4) NOT NULL,
  `op_name` varchar(250) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `credits` int(6) NOT NULL DEFAULT '0',
  `price_mod` varchar(4) NOT NULL,
  `op_name_german` varchar(250) NOT NULL,
  `op_name_english` varchar(250) NOT NULL,
  PRIMARY KEY (`op_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_rm_options`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_rm_option_grp`
--

CREATE TABLE `ps4_rm_option_grp` (
  `og_id` int(6) NOT NULL AUTO_INCREMENT,
  `license_id` int(4) NOT NULL,
  `og_name` varchar(250) NOT NULL,
  `og_name_german` varchar(250) NOT NULL,
  `og_name_english` varchar(250) NOT NULL,
  PRIMARY KEY (`og_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_rm_option_grp`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_rm_ref`
--

CREATE TABLE `ps4_rm_ref` (
  `ref_id` int(4) NOT NULL AUTO_INCREMENT,
  `group_id` int(4) NOT NULL,
  `option_id` int(4) NOT NULL,
  PRIMARY KEY (`ref_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_rm_ref`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_rm_schemes`
--

CREATE TABLE `ps4_rm_schemes` (
  `rm_id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `rm_id` (`rm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_rm_schemes`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_saved_sorting`
--

CREATE TABLE `ps4_saved_sorting` (
  `so_id` int(4) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(20) NOT NULL,
  `listby` varchar(20) NOT NULL,
  `listtype` varchar(4) NOT NULL,
  `perpage` int(4) NOT NULL DEFAULT '25',
  UNIQUE KEY `so_id` (`so_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `ps4_saved_sorting`
--

INSERT INTO `ps4_saved_sorting` VALUES(1, 'order_groups', 'name', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(2, 'news', 'title', 'asc', 10);
INSERT INTO `ps4_saved_sorting` VALUES(3, 'asset_groups', 'name', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(4, 'member_groups', 'name', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(5, 'product_groups', 'name', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(6, 'toolslinks', 'tl_name', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(7, 'regions', 'region_id', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(8, 'galleries', 'name', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(9, 'membership_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(10, 'members_sub', 'mem_id', 'desc', 3);
INSERT INTO `ps4_saved_sorting` VALUES(11, 'digital_ps', 'dps_id', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(12, 'print_ps', 'name', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(39, 'packages', 'pack_id', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(14, 'memberships', 'ms_id', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(15, 'digital_ps_groups', 'name', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(16, 'print_ps_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(17, 'news_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(22, 'digital_sp', 'sortorder', 'asc', 10);
INSERT INTO `ps4_saved_sorting` VALUES(21, 'groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(23, 'digital_sp_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(45, 'prints', 'print_id', 'asc', 50);
INSERT INTO `ps4_saved_sorting` VALUES(24, 'members', 'mem_id', 'desc', 50);
INSERT INTO `ps4_saved_sorting` VALUES(25, 'package_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(27, 'print_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(31, 'countries', 'name', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(28, 'orders', 'order_id', 'desc', 50);
INSERT INTO `ps4_saved_sorting` VALUES(29, 'shipping', 'ship_id', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(30, 'shipping_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(32, 'states', 'name', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(33, 'zipcodes', 'zipcode_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(34, 'currencies', 'defaultcur', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(44, 'products', 'sortorder', 'desc', 50);
INSERT INTO `ps4_saved_sorting` VALUES(35, 'countries_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(38, 'storage', 'storage_id', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(37, 'storage_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(40, 'folders', 'folder_id', 'desc', 100);
INSERT INTO `ps4_saved_sorting` VALUES(41, 'folders_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(42, 'media_types', 'mt_id', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(43, 'media_types_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(46, 'collections', 'coll_id', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(47, 'collection_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(48, 'subscriptions', 'sortorder', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(49, 'subscription_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(50, 'lightboxes', 'lightbox_id', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(51, 'lightboxes_groups', 'gr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(52, 'media_comments', 'mc_id', 'desc', 50);
INSERT INTO `ps4_saved_sorting` VALUES(55, 'media_tags', 'status', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(54, 'media_ratings', 'mr_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(58, 'member_bios', 'bio_approved', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(57, 'support_tickets', 'status', 'desc', 10);
INSERT INTO `ps4_saved_sorting` VALUES(59, 'member_avatars', 'mem_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(61, 'billings', 'invoice_number', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(62, 'promotions', 'sortorder', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(63, 'page_content', 'name', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(64, 'agreements', 'content_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(65, 'email_content', 'content_id', 'asc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(66, 'media', 'media_id', 'desc', 25);
INSERT INTO `ps4_saved_sorting` VALUES(67, 'credits', 'credit_id', 'asc', 25);

-- --------------------------------------------------------

--
-- Table structure for table `ps4_searches`
--

CREATE TABLE `ps4_searches` (
  `search_id` int(6) NOT NULL AUTO_INCREMENT,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `area` varchar(20) NOT NULL DEFAULT '',
  `term` varchar(50) NOT NULL DEFAULT '',
  UNIQUE KEY `search_id` (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_searches`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_servers`
--

CREATE TABLE `ps4_servers` (
  `server_id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `url_path` varchar(250) NOT NULL DEFAULT '',
  `passkey` varchar(100) NOT NULL DEFAULT '',
  `serlocked` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `server_id` (`server_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `ps4_servers`
--

INSERT INTO `ps4_servers` VALUES(1, 'Local Server', 'localhost', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ps4_settings`
--

CREATE TABLE `ps4_settings` (
  `settings_id` int(1) NOT NULL AUTO_INCREMENT,
  `site_visits` int(10) NOT NULL DEFAULT '0',
  `credits_digital` tinyint(1) NOT NULL DEFAULT '0',
  `credits_prof` tinyint(1) NOT NULL DEFAULT '0',
  `credits_print` tinyint(1) NOT NULL DEFAULT '0',
  `credits_prod` tinyint(1) NOT NULL DEFAULT '0',
  `credits_pack` tinyint(1) NOT NULL DEFAULT '0',
  `credits_coll` tinyint(1) NOT NULL DEFAULT '0',
  `credits_sub` tinyint(1) NOT NULL DEFAULT '0',
  `credit_com` decimal(10,4) NOT NULL,
  `sub_com` decimal(10,4) NOT NULL,
  `site_title` text NOT NULL,
  `meta_desc` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `site_status` tinyint(1) NOT NULL DEFAULT '1',
  `status_message` text NOT NULL,
  `support_email` text NOT NULL,
  `sales_email` text NOT NULL,
  `site_url` text NOT NULL,
  `allow_debug_access` tinyint(1) NOT NULL DEFAULT '0',
  `infoshare` tinyint(1) NOT NULL DEFAULT '0',
  `subscriptions` tinyint(1) NOT NULL,
  `admin_activity` tinyint(1) NOT NULL DEFAULT '0',
  `member_activity` tinyint(1) NOT NULL DEFAULT '0',
  `debug_pass` varchar(40) NOT NULL DEFAULT '',
  `order_num_type` tinyint(1) NOT NULL,
  `order_num_next` int(10) NOT NULL,
  `api_pass` varchar(55) NOT NULL,
  `time_zone` varchar(5) NOT NULL DEFAULT '0',
  `daylight_savings` tinyint(1) NOT NULL DEFAULT '0',
  `date_format` varchar(4) NOT NULL DEFAULT '',
  `date_display` varchar(6) NOT NULL DEFAULT '',
  `clock_format` char(2) NOT NULL DEFAULT '',
  `date_sep` char(1) NOT NULL DEFAULT '',
  `dt_member_override` tinyint(1) NOT NULL DEFAULT '0',
  `dt_lang_override` tinyint(1) NOT NULL DEFAULT '0',
  `verify_before_delete` tinyint(1) NOT NULL DEFAULT '1',
  `demo_mode` tinyint(1) NOT NULL DEFAULT '0',
  `mod_rewrite` tinyint(1) NOT NULL DEFAULT '0',
  `auto_folders` tinyint(1) NOT NULL DEFAULT '0',
  `enc_folders` tinyint(1) NOT NULL DEFAULT '0',
  `lang_file_mgr` text NOT NULL,
  `lang_file_pub` text NOT NULL,
  `default_lang` varchar(100) NOT NULL,
  `serial_number` varchar(50) NOT NULL DEFAULT '0',
  `newkey` varchar(50) NOT NULL DEFAULT '0',
  `lockkey` varchar(40) NOT NULL,
  `tax_inc` tinyint(1) NOT NULL DEFAULT '0',
  `tax_type` tinyint(1) NOT NULL DEFAULT '0',
  `tax_optout` tinyint(1) NOT NULL DEFAULT '0',
  `tax_stm` text NOT NULL,
  `tax_a_default` decimal(6,3) NOT NULL,
  `tax_b_default` decimal(6,3) NOT NULL,
  `tax_c_default` decimal(6,3) NOT NULL,
  `tax_a_digital` decimal(6,3) NOT NULL,
  `tax_b_digital` decimal(6,3) NOT NULL,
  `tax_c_digital` decimal(6,3) NOT NULL,
  `taxa_name` text NOT NULL,
  `taxb_name` text NOT NULL,
  `taxc_name` text NOT NULL,
  `tax_ms` tinyint(1) NOT NULL DEFAULT '0',
  `tax_prints` tinyint(1) NOT NULL DEFAULT '0',
  `tax_digital` tinyint(1) NOT NULL DEFAULT '0',
  `tax_subs` tinyint(1) NOT NULL DEFAULT '0',
  `tax_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `tax_credits` tinyint(1) NOT NULL DEFAULT '0',
  `debugpanel` tinyint(1) NOT NULL DEFAULT '0',
  `currency_symbol_left` varchar(10) NOT NULL DEFAULT '',
  `currency_symbol_right` varchar(10) NOT NULL DEFAULT '',
  `currency_div` char(2) NOT NULL DEFAULT '',
  `defaultcur` int(3) NOT NULL DEFAULT '0',
  `incoming_path` text NOT NULL,
  `library_path` text NOT NULL,
  `last_backup` datetime NOT NULL,
  `backup_days` int(3) NOT NULL DEFAULT '0',
  `display_alerts` tinyint(1) NOT NULL DEFAULT '0',
  `enable_credits` tinyint(1) NOT NULL DEFAULT '0',
  `enable_cbp` tinyint(1) NOT NULL DEFAULT '0',
  `mem_headers` text NOT NULL,
  `odr_headers` text NOT NULL,
  `billings_filters` text NOT NULL,
  `commission_filters` text NOT NULL,
  `billings_headers` text NOT NULL,
  `odr_filters` text NOT NULL,
  `blockips` text NOT NULL,
  `blockreferrer` text NOT NULL,
  `blockemails` text NOT NULL,
  `blockwords` text NOT NULL,
  `imageproc` tinyint(1) NOT NULL DEFAULT '1',
  `mailproc` tinyint(1) NOT NULL DEFAULT '1',
  `flexpricing` tinyint(1) NOT NULL DEFAULT '0',
  `customizer` tinyint(1) NOT NULL DEFAULT '0',
  `readiptc` tinyint(1) NOT NULL DEFAULT '0',
  `readexif` tinyint(1) NOT NULL DEFAULT '0',
  `smtp_port` int(8) NOT NULL,
  `smtp_host` text NOT NULL,
  `smtp_username` text NOT NULL,
  `smtp_password` varchar(30) NOT NULL,
  `copy_messages` tinyint(1) NOT NULL DEFAULT '0',
  `disable_right_click` tinyint(1) NOT NULL DEFAULT '0',
  `disable_copy_paste` tinyint(1) NOT NULL DEFAULT '0',
  `disable_printing` tinyint(1) NOT NULL DEFAULT '0',
  `disable_linking` tinyint(1) NOT NULL DEFAULT '0',
  `site_title_english` text NOT NULL,
  `meta_desc_english` text NOT NULL,
  `meta_keywords_english` text NOT NULL,
  `use_gpi` tinyint(1) NOT NULL DEFAULT '1',
  `accounts_required` tinyint(1) NOT NULL DEFAULT '0',
  `rating_system` tinyint(1) NOT NULL DEFAULT '1',
  `rating_system_lr` tinyint(1) NOT NULL DEFAULT '1',
  `comment_system` tinyint(1) NOT NULL DEFAULT '0',
  `comment_system_lr` tinyint(1) NOT NULL DEFAULT '1',
  `comment_system_aa` tinyint(1) NOT NULL DEFAULT '1',
  `tagging_system` tinyint(1) NOT NULL DEFAULT '0',
  `tagging_system_lr` tinyint(1) NOT NULL DEFAULT '0',
  `tagging_system_aa` tinyint(1) NOT NULL DEFAULT '0',
  `request_photo` tinyint(1) NOT NULL DEFAULT '0',
  `email_friend` tinyint(1) NOT NULL DEFAULT '0',
  `watch_lists` tinyint(1) NOT NULL DEFAULT '0',
  `lightbox` tinyint(1) NOT NULL DEFAULT '1',
  `glightbox` tinyint(1) NOT NULL DEFAULT '0',
  `delete_carts` int(3) NOT NULL DEFAULT '5',
  `expire_download` int(3) NOT NULL DEFAULT '7',
  `dl_attempts` int(3) NOT NULL,
  `cart` tinyint(1) NOT NULL DEFAULT '1',
  `download_extensions` tinyint(1) NOT NULL DEFAULT '1',
  `default_price` decimal(10,4) NOT NULL,
  `default_credits` int(10) NOT NULL DEFAULT '0',
  `auto_orders` tinyint(1) NOT NULL,
  `min_total` decimal(10,4) NOT NULL,
  `business_name` varchar(50) NOT NULL,
  `business_address2` text NOT NULL,
  `business_city` varchar(250) NOT NULL,
  `business_state` int(4) NOT NULL,
  `business_zip` varchar(50) NOT NULL,
  `business_address` text NOT NULL,
  `business_country` int(5) NOT NULL,
  `notify_sale` tinyint(1) NOT NULL DEFAULT '0',
  `notify_account` tinyint(1) NOT NULL DEFAULT '0',
  `notify_rating` tinyint(1) NOT NULL DEFAULT '0',
  `notify_comment` tinyint(1) NOT NULL DEFAULT '0',
  `notify_lightbox` tinyint(1) NOT NULL DEFAULT '0',
  `notify_profile` tinyint(1) NOT NULL DEFAULT '0',
  `notify_contrup` tinyint(1) NOT NULL DEFAULT '0',
  `notify_tags` tinyint(1) NOT NULL,
  `login_groups` text NOT NULL,
  `signup_groups` text NOT NULL,
  `style` varchar(100) NOT NULL,
  `stats_html` text NOT NULL,
  `captcha` tinyint(1) NOT NULL DEFAULT '0',
  `news` tinyint(1) NOT NULL DEFAULT '0',
  `search` tinyint(1) NOT NULL DEFAULT '0',
  `esearch` tinyint(1) NOT NULL DEFAULT '0',
  `contact` tinyint(1) NOT NULL DEFAULT '0',
  `email_conf` tinyint(1) NOT NULL DEFAULT '0',
  `forum_link` text NOT NULL,
  `rec_backup` datetime NOT NULL,
  `rec_version` varchar(6) NOT NULL,
  `rec_prod` varchar(4) NOT NULL,
  `rec_name` varchar(20) NOT NULL,
  `auto_rp` tinyint(1) NOT NULL DEFAULT '0',
  `purge_logs` int(4) NOT NULL,
  `member_avatars` tinyint(1) NOT NULL DEFAULT '0',
  `contr_portfolios` tinyint(1) NOT NULL DEFAULT '0',
  `contr_showcase` tinyint(1) NOT NULL DEFAULT '0',
  `contr_num` int(2) NOT NULL,
  `contr_fm` int(1) NOT NULL,
  `contr_metatags` tinyint(1) NOT NULL DEFAULT '0',
  `contr_samples` tinyint(1) NOT NULL DEFAULT '0',
  `contr_cd` tinyint(1) NOT NULL DEFAULT '0',
  `contr_cd2` tinyint(1) NOT NULL DEFAULT '0',
  `contr_cd2_mes` text NOT NULL,
  `contr_dvp` tinyint(1) NOT NULL DEFAULT '0',
  `contr_col` tinyint(1) NOT NULL DEFAULT '0',
  `com_calc` tinyint(1) NOT NULL DEFAULT '0',
  `print_orders_email` varchar(200) NOT NULL,
  `avatar_size` int(4) NOT NULL DEFAULT '100',
  `avatar_filetypes` varchar(100) NOT NULL DEFAULT '100',
  `avatar_approval` tinyint(1) NOT NULL DEFAULT '0',
  `avatar_filesize` int(4) NOT NULL,
  `rss_news` tinyint(1) NOT NULL,
  `rss_featured_media` tinyint(1) NOT NULL,
  `rss_galleries` tinyint(1) NOT NULL,
  `rss_search` tinyint(1) NOT NULL,
  `rss_newest` tinyint(1) NOT NULL,
  `rss_popular` tinyint(1) NOT NULL,
  `rss_records` int(4) NOT NULL,
  `mgrpath` varchar(200) NOT NULL,
  `color_scheme` varchar(20) NOT NULL,
  `invoice_prefix` varchar(20) NOT NULL,
  `invoice_suffix` varchar(20) NOT NULL,
  `invoice_next` int(20) NOT NULL,
  `weight_tag` varchar(50) NOT NULL,
  `compay` varchar(10) NOT NULL,
  `compay_other` varchar(200) NOT NULL,
  `content_editor` int(1) NOT NULL,
  `mgr_gal_display` tinyint(1) NOT NULL,
  `decimal_separator` varchar(2) NOT NULL,
  `thousands_separator` varchar(10) NOT NULL,
  `decimal_places` tinyint(1) NOT NULL,
  `neg_num_format` tinyint(1) NOT NULL,
  `lang_num_override` tinyint(1) NOT NULL DEFAULT '0',
  `mem_num_override` tinyint(1) NOT NULL DEFAULT '0',
  `thumb_size` int(3) NOT NULL,
  `thumb_quality` int(3) NOT NULL,
  `thumbcrop_height` int(5) NOT NULL,
  `thumbDetailsRating` tinyint(1) NOT NULL DEFAULT '0',
  `thumbDetailsCart` tinyint(1) NOT NULL DEFAULT '0',
  `thumbDetailsLightbox` tinyint(1) NOT NULL DEFAULT '0',
  `thumbDetailsPackage` tinyint(1) NOT NULL DEFAULT '0',
  `thumbDetailsEmail` tinyint(1) NOT NULL DEFAULT '0',
  `rollover_size` int(3) NOT NULL,
  `rollover_quality` int(3) NOT NULL,
  `rollover_status` tinyint(1) NOT NULL DEFAULT '0',
  `rollovercrop_height` int(5) NOT NULL,
  `rolloverDetailsRating` tinyint(1) NOT NULL DEFAULT '0',
  `previewDetailsRating` tinyint(1) NOT NULL DEFAULT '0',
  `gallery_thumb_size` int(4) NOT NULL,
  `gallerythumbcrop` tinyint(1) NOT NULL,
  `gallerythumbcrop_height` int(4) NOT NULL,
  `gallery_perpage` int(4) NOT NULL,
  `preview_size` int(4) NOT NULL,
  `preview_quality` int(3) NOT NULL,
  `thumb_wm` text NOT NULL,
  `rollover_wm` text NOT NULL,
  `preview_wm` text NOT NULL,
  `thumb_wmr` tinyint(1) NOT NULL DEFAULT '0',
  `rollover_wmr` tinyint(1) NOT NULL DEFAULT '0',
  `preview_wmr` tinyint(1) NOT NULL DEFAULT '0',
  `preview_image` text NOT NULL,
  `thumb_details` text NOT NULL,
  `rollover_details` text NOT NULL,
  `preview_details` text NOT NULL,
  `thumb_sharpen` tinyint(1) NOT NULL DEFAULT '0',
  `rollover_sharpen` tinyint(1) NOT NULL DEFAULT '0',
  `preview_sharpen` tinyint(1) NOT NULL DEFAULT '0',
  `mainlogo` varchar(20) NOT NULL,
  `dsorting` varchar(20) NOT NULL,
  `media_perpage` int(3) NOT NULL,
  `dsorting2` varchar(10) NOT NULL,
  `gallery_count` tinyint(1) NOT NULL DEFAULT '0',
  `site_stats` tinyint(1) NOT NULL DEFAULT '0',
  `members_online` tinyint(1) NOT NULL DEFAULT '0',
  `new_media` tinyint(1) NOT NULL DEFAULT '0',
  `new_media_count` int(2) NOT NULL,
  `popular_media` tinyint(1) NOT NULL DEFAULT '0',
  `popular_media_count` int(2) NOT NULL,
  `random_media_count` int(2) NOT NULL,
  `menubuild` tinyint(1) NOT NULL DEFAULT '0',
  `promopage` tinyint(1) NOT NULL,
  `featuredpage` tinyint(1) NOT NULL DEFAULT '0',
  `creditpage` tinyint(1) NOT NULL DEFAULT '0',
  `aboutpage` tinyint(1) NOT NULL DEFAULT '0',
  `newestpage` tinyint(1) NOT NULL,
  `popularpage` tinyint(1) NOT NULL,
  `printpage` tinyint(1) NOT NULL,
  `prodpage` tinyint(1) NOT NULL,
  `packpage` tinyint(1) NOT NULL,
  `collpage` tinyint(1) NOT NULL,
  `subpage` tinyint(1) NOT NULL,
  `thumbcrop` tinyint(1) NOT NULL DEFAULT '0',
  `rollovercrop` tinyint(1) NOT NULL DEFAULT '0',
  `db_version` varchar(6) NOT NULL,
  `hpnews` tinyint(4) NOT NULL DEFAULT '0',
  `hppromos` tinyint(4) NOT NULL DEFAULT '0',
  `hpnewestmedia` tinyint(4) NOT NULL DEFAULT '0',
  `hpfeaturedmedia` tinyint(4) NOT NULL DEFAULT '0',
  `hppopularmedia` tinyint(4) NOT NULL DEFAULT '0',
  `hprandmedia` tinyint(1) NOT NULL DEFAULT '0',
  `hpprints` tinyint(4) NOT NULL DEFAULT '0',
  `hpprods` tinyint(4) NOT NULL DEFAULT '0',
  `hppacks` tinyint(4) NOT NULL DEFAULT '0',
  `hpcolls` tinyint(4) NOT NULL DEFAULT '0',
  `hpsubs` tinyint(4) NOT NULL DEFAULT '0',
  `hpcredits` tinyint(4) NOT NULL DEFAULT '0',
  `uploader` tinyint(1) NOT NULL DEFAULT '1',
  `ticketsystem` tinyint(1) NOT NULL DEFAULT '0',
  `reg_memberships` tinyint(1) NOT NULL DEFAULT '0',
  `related_media` tinyint(1) NOT NULL DEFAULT '0',
  `display_iptc` tinyint(1) NOT NULL DEFAULT '0',
  `display_exif` tinyint(1) NOT NULL DEFAULT '0',
  `video_controls` varchar(12) NOT NULL,
  `video_autoplay` tinyint(1) NOT NULL DEFAULT '0',
  `video_rollover_width` int(6) NOT NULL,
  `video_rollover_height` int(6) NOT NULL,
  `video_sample_width` int(6) NOT NULL,
  `video_sample_height` int(6) NOT NULL,
  `video_autorepeat` tinyint(1) NOT NULL DEFAULT '0',
  `video_bg_color` varchar(12) NOT NULL DEFAULT '000000',
  `video_skin` varchar(30) NOT NULL,
  `vidrollover_wm` text NOT NULL,
  `vidpreview_wm` text NOT NULL,
  `video_autoresize` tinyint(1) NOT NULL DEFAULT '0',
  `video_wmpos` varchar(20) NOT NULL,
  `sn_code` text NOT NULL,
  `skip_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `cache_pages` tinyint(1) NOT NULL DEFAULT '0',
  `cache_pages_time` int(8) NOT NULL,
  `purchase_agreement` tinyint(1) NOT NULL DEFAULT '0',
  `search_fields` tinyint(1) NOT NULL,
  `search_media_types` tinyint(1) NOT NULL,
  `search_orientation` tinyint(1) NOT NULL,
  `search_color` tinyint(1) NOT NULL,
  `search_dates` tinyint(1) NOT NULL,
  `search_license_type` tinyint(1) NOT NULL,
  `search_galleries` tinyint(1) NOT NULL,
  `gal_version` int(11) NOT NULL,
  `display_login` tinyint(1) NOT NULL DEFAULT '1',
  `pppage` tinyint(1) NOT NULL DEFAULT '1',
  `papage` tinyint(1) NOT NULL DEFAULT '1',
  `tospage` tinyint(1) NOT NULL DEFAULT '1',
  `iptc_utf8` tinyint(1) NOT NULL DEFAULT '0',
  `hpf_width` int(8) NOT NULL,
  `hpf_crop_to` int(8) NOT NULL,
  `hpf_fade_speed` int(8) NOT NULL,
  `hpf_inverval` int(8) NOT NULL,
  `hpf_details_delay` int(8) NOT NULL,
  `hpf_details_distime` int(8) NOT NULL,
  `measurement` char(1) NOT NULL DEFAULT 'i',
  `gpsonoff` tinyint(1) NOT NULL DEFAULT '0',
  `gpszoom` int(3) NOT NULL DEFAULT '10',
  `gpscolor` varchar(20) NOT NULL DEFAULT 'black',
  `gpswidth` int(4) NOT NULL DEFAULT '280',
  `gpsheight` int(4) NOT NULL DEFAULT '155',
  `gpsmaptype` varchar(20) NOT NULL DEFAULT 'terrain',
  `zoomlenssize` int(4) NOT NULL DEFAULT '300',
  `zoomonoff` tinyint(1) NOT NULL DEFAULT '0',
  `zoombordersize` int(3) NOT NULL DEFAULT '2',
  `zoombordercolor` varchar(20) NOT NULL DEFAULT '888888',
  `featured_wm` tinyint(1) NOT NULL DEFAULT '1',
  `contactCaptcha` tinyint(1) NOT NULL,
  `customer_taxid` tinyint(1) NOT NULL DEFAULT '0',
  `display_license` tinyint(1) NOT NULL DEFAULT '1',
  `cart_notes` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `settings_id` (`settings_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1769 ;

--
-- Dumping data for table `ps4_settings`
--

INSERT INTO `ps4_settings` VALUES(1, 0, 1, 0, 1, 1, 1, 1, 1, '0.0000', '0.0000', 'Site Title', '', '', 1, '', '', '', 'http://localhost:1978', 1, 1, 1, 1, 1, '', 1, 10000, '', '-5', 0, 'US', 'short', '12', '.', 1, 1, 1, 0, 0, 1, 0, 'english', 'english', 'english', '', '', '', 0, 1, 1, '', '0.000', '0.000', '0.000', '0.000', '0.000', '0.000', 'Tax', '', '', 1, 1, 0, 1, 0, 1, 1, '', '', '', 1, 'C:\\UniServer\\www\\assets\\incoming', 'C:\\UniServer\\www\\assets\\library', '2012-04-09 18:57:01', 0, 1, 0, 0, 'l_name,mem_id,email,status,signup_date,notes,avatar_status,', 'l_name,order_id,order_number,invoice_number,order_status,order_date,payment_status,total', '0,1,2,4,5,6', '', '', '1,0,3,4', '', '', '', '', 1, 1, 0, 0, 1, 1, 25, '', '', '', 0, 0, 0, 0, 1, '', '', '', 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 5, 7, 3, 1, 0, '2.0000', 100, 0, '0.0100', '', '', '', 0, '', '', 229, 1, 1, 1, 1, 1, 1, 0, 1, '', '', 'modern', '', 0, 1, 1, 0, 1, 0, '', '2008-09-27 17:01:42', '4.0.1', 'ps', '', 1, 90, 0, 0, 0, 6, 1, 1, 1, 0, 0, 'Please send your photos on CD/DVD to the following address and we will load them on the site for you.', 0, 0, 0, '', 0, 'jpg,gif,png', 0, 400, 0, 0, 0, 0, 0, 0, 30, 'manager/', '', 'inv2012-', '', 10318, 'lbs', '', '', 1, 1, '.', ',', 2, 1, 1, 1, 190, 90, 140, 1, 0, 1, 1, 1, 350, 95, 1, 75, 1, 1, 190, 1, 75, 10, 583, 95, '', 'photostore.png', 'photostore.png', 1, 1, 1, 'images/samples/mgr.sample.photo9.1024px.jpg', 'views,date,title,filename,owner,id,created', 'resolution,title,price,description,date,owner,id,views', 'title,description,owner,views,id,keywords,usage_restrictions,copyright,model_release,date,created,filename,colorPalette', 1, 0, 0, 'main.logo.jpg', 'sortorder', 10, 'DESC', 1, 0, 0, 1, 3, 1, 3, 3, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, '4.7.5', 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 4, 0, 1, 1, 1, 1, 'none', 0, 320, 180, 512, 288, 1, '000000', 'nacht', '', '', 0, 'bottom-left', '', 0, 1, 3600, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 694, 340, 1000, 10000, 1000, 4000, 'i', 0, 10, 'black', 280, 155, 'terrain', 300, 0, 2, '888888', 1, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ps4_settings2`
--

CREATE TABLE `ps4_settings2` (
  `settings_id` int(6) NOT NULL AUTO_INCREMENT,
  `save2_id` int(6) NOT NULL,
  `contrib_link` tinyint(1) NOT NULL DEFAULT '1',
  `share` tinyint(1) NOT NULL DEFAULT '1',
  `thumbDetailsDownloads` tinyint(1) NOT NULL DEFAULT '1',
  `minicart` tinyint(1) NOT NULL DEFAULT '1',
  `tagCloudOn` tinyint(1) NOT NULL DEFAULT '1',
  `tagCloudSort` text NOT NULL,
  `gallerySortBy` text NOT NULL,
  `gallerySortOrder` text NOT NULL,
  `pubuploader` tinyint(1) NOT NULL DEFAULT '1',
  `facebook_link` varchar(250) NOT NULL,
  `twitter_link` varchar(250) NOT NULL,
  `fotomoto` varchar(250) NOT NULL,
  PRIMARY KEY (`settings_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ps4_settings2`
--

INSERT INTO `ps4_settings2` VALUES(1, 0, 1, 0, 1, 1, 1, 'default', 'sort_number', '', 1, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ps4_shipping`
--

CREATE TABLE `ps4_shipping` (
  `ship_id` int(4) NOT NULL AUTO_INCREMENT,
  `uship_id` varchar(40) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `sortorder` int(3) NOT NULL,
  `calc_type` tinyint(1) NOT NULL DEFAULT '1',
  `cost_type` tinyint(1) NOT NULL DEFAULT '1',
  `flat_rate` varchar(6) NOT NULL,
  `calc_module` varchar(10) NOT NULL DEFAULT 'custom',
  `ship_notes` text NOT NULL,
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `day1` varchar(4) NOT NULL,
  `day2` varchar(4) NOT NULL,
  `region` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `title_dutch` varchar(255) NOT NULL,
  `title_english` varchar(255) NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `title_german` varchar(255) NOT NULL,
  `description_dutch` text NOT NULL,
  `description_english` text NOT NULL,
  `description_french` text NOT NULL,
  `description_german` text NOT NULL,
  `title_spanish` varchar(255) NOT NULL,
  `description_spanish` text NOT NULL,
  UNIQUE KEY `ship_id` (`ship_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_shipping`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_shipping_ranges`
--

CREATE TABLE `ps4_shipping_ranges` (
  `sr_id` int(3) NOT NULL AUTO_INCREMENT,
  `ship_id` int(4) NOT NULL,
  `fromrange` decimal(10,4) NOT NULL,
  `torange` decimal(10,4) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  UNIQUE KEY `sr_id` (`sr_id`),
  KEY `ship_id` (`ship_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_shipping_ranges`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_shipping_regions`
--

CREATE TABLE `ps4_shipping_regions` (
  `sr_id` int(5) NOT NULL AUTO_INCREMENT,
  `shipping_id` int(5) NOT NULL,
  `region_id` int(5) NOT NULL,
  `region_type` varchar(10) NOT NULL,
  UNIQUE KEY `sr_id` (`sr_id`),
  KEY `shipping_id` (`shipping_id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ps4_shipping_regions`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_states`
--

CREATE TABLE `ps4_states` (
  `state_id` int(5) NOT NULL AUTO_INCREMENT,
  `ustate_id` varchar(40) NOT NULL,
  `name` varchar(100) NOT NULL,
  `scode` varchar(10) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `country_id` int(5) NOT NULL,
  `all_ship_methods` tinyint(1) NOT NULL DEFAULT '1',
  `ship_percentage` int(4) NOT NULL DEFAULT '100',
  `name_dutch` varchar(255) NOT NULL,
  `name_french` varchar(255) NOT NULL,
  `name_german` varchar(255) NOT NULL,
  `name_spanish` varchar(255) NOT NULL,
  `name_english` varchar(255) NOT NULL,
  UNIQUE KEY `state_id` (`state_id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=352 ;

--
-- Dumping data for table `ps4_states`
--

INSERT INTO `ps4_states` VALUES(1, '4E88A772C03D4AD999BC79414EC33695', 'Australian Capital Territory', 'ACT', 1, 0, 15, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(3, '0254CE456081C552A3FF8C410E29DAF5', 'Northern Territory', 'NT', 1, 0, 15, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(4, '418CFAF085507B56F3EC11AEAB885F8B', 'Queensland', 'QLD', 1, 0, 15, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(5, '59012D3710A6A407A301934F2B31006A', 'South Australia', 'SA', 1, 0, 15, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(6, '3B567F44B9A1E2E2B70228C75E1381B0', 'Tasmania', 'TAS', 1, 0, 15, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(7, 'F3998D29C470C7144E10BC459DCCF33F', 'Victoria', 'VIC', 1, 0, 15, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(8, '6E556E8968D2228B27C4CB7C4935B43B', 'Western Australia', 'WA', 1, 0, 15, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(9, 'DFCD1396724B539A9594FC60BEBDC6F9', 'Alberta', 'AB', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(10, '422D8EEAC88362A072B3063E144B9A52', 'British Columbia', 'BC', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(11, 'BA1DE0FD0B6B9FE0064A608B6B2FEBBD', 'Manitoba', 'MB', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(12, '5FBDCA93FB648A3604F828D0AE11E548', 'New Brunswick', 'NB', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(13, 'EBD20AC0FDED0A0DFC2C86EC6DBAD314', 'Newfoundland and Labrador', 'NL', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(14, 'E3E44089EE8F02DF23B9E516363CD36D', 'Northwest Territories', 'NT', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(15, '73308D21E7E11A80C825F9D95DADD24E', 'Nova Scotia', 'NS', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(16, '8037DF5A215C9E1694F750268821DE60', 'Nunavut', 'NU', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(17, 'D5240E4FE2059A51FBF43397ECEA9BBC', 'Ontario', 'ON', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(18, 'A892F84E205571E8B42E84BEB8DC2B47', 'Prince Edward Island', 'PE', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(19, '113E3FE848F5253F9305979A79A26142', 'Qu', 'QC', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(20, '5FC110AFDF1BDE9CC4B5BB33C5385A7D', 'Saskatchewan', 'SK', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(21, '62A1300DD081791D73DF998729B8F2B7', 'Yukon Territory', 'YT', 1, 0, 41, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(22, '253D7E89065A870E1A1061E239D8275B', 'Ain', '01', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(23, '86DAA9A292B0DEEEF2581C223A86909C', 'Aisne', '02', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(24, '961B487D4BEF86A41720FBA8C3DA6267', 'Allier', '03', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(25, 'C43AE3840B347C709DFE61D48C660080', 'Alpes-de-Haute-Provence', '04', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(26, '5565EA8A20D7F03EC625A04D4412DF37', 'Alpes-Maritimes', '06', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(27, 'CF172C8047E5B297028C5858F69D7455', 'Ardeche', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(28, '3CFE0F471FFAE88EED941F0A910AD179', 'Ardennes', '08', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(29, '4965836AE43413C6BB251D5AF2793A8D', 'Ariege', '09', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(30, 'FE6D2A181CC20E4B3C526C2B0D232012', 'Aube', '10', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(31, '53149B008BAEB4684358CCDEC7C34EF0', 'Aude', '11', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(32, '6EFDF12FD84562B11380915DFF598BA4', 'Aveyron', '12', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(33, 'A1742B93005697238AAC2A3C5313C2EA', 'Bouches-du-rhone', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(34, '080C609F1960A72AD8C443EE072ED6FE', 'Cote-d-or', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(36, '0EA747376BAA280ED368AF71C8072ACA', 'Calvados', '14', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(37, 'A4D9A1C1E245CE1E78391A89ECEBBC19', 'Cantal', '15', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(38, '519C4BFB7CBCCC6D8C1BCADAC34C64D2', 'Charente', '16', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(39, 'F742AEF622A42DFC6B31A64DF1053A5C', 'Charente-Maritime', '17', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(40, '7E44CF2FB7DC742DD7D92D8A04BDC596', 'Cher', '18', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(41, '772848969B00C2FD63E96834FAEB464F', 'Correze', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(42, '41D0E757FEEC38FFBFAD79005D8625A0', 'Corse-du-Sud', '2A', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(43, 'F8C47403BADA85E5BAAE5E4E85127197', 'Creuse', '23', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(44, '79CDB6305B54C99D5A811B550F235C73', 'Dordogne', '24', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(45, '6DF6F58A9D2C7905BCAD32DB62912411', 'Doubs', '25', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(46, 'BFC096D030A5006CF4555637A2763D16', 'Drome', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(48, '5A168E4A245A1F314E134B26969BE657', 'Eure', '27', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(49, 'C746EF4807D883E0B2C1358A245BF464', 'Eure-et-Loir', '28', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(50, '7E5C5A60CEF7660F87E75B13B8CC40B5', 'Finistere', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(51, '9E1AC7960782EE93DAA67B06546E504C', 'Gard', '30', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(52, 'CF8359CDFC511C518CDF446563712A81', 'Gers', '32', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(53, 'E6B48A1AD6C8BE786A94E510DABD90D4', 'Gironde', '33', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(321, '0FB7857A7D0499516AA20CC976F7579C', 'Meurthe-et-moselle', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(55, 'A19E15C0E4D877B516036360783ACA7D', 'Haute-Corse', '2B', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(56, 'D69924F48E207E75179C0E647F69BAAF', 'Haute-Garonne', '31', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(325, 'CD8E86D84245F4B8A394E7A8F71061E2', 'Nievre', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(319, '70677615E03220D1AF89A16EA4B9B27B', 'Mayenne', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(318, 'DAD9189E7D7461AC2D623503845C47DD', 'Haute-marne', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(317, 'A19E15CB51603630E4D87760783ACA7D', 'Haute-loire', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(61, '37CDADAD9D0F7B10FF1E646E41997AC1', 'Herault', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(62, '84DA58C8AE65360D264F1EDABC093CB7', 'Ille-et-Vilaine', '35', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(63, 'D5C9A2A5635E0AED064D02B67B225862', 'Indre', '36', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(64, '70677615E032209A16EA4D1AF8B9B27B', 'Indre-et-Loire', '37', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(65, 'DAD9189E7D7623503845C461AC2D47DD', 'Isere', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(66, 'D69463205D81170B036A2F0C8C5B39FB', 'Jura', '39', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(67, '9CFF5C1B191816C335153C5C545DF478', 'Landes', '40', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(68, 'B49E7AC545088A93EE0A45D550DB3B07', 'Loir-et-Cher', '41', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(69, '37FFB6771B78B3D289057DC774C1F7D7', 'Loire', '42', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(70, '3D0B390C7670AF5FB8124A87BEFED4F2', 'Loire-Atlantique', '44', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(71, 'CD86D84245F461E8EB8A394E7A8F7102', 'Loiret', '45', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(72, '0E61D5AAAAF30BBB8E262E43AF64D7DF', 'Lot', '46', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(73, '8EEC43B2486DAD0AE48F685ECBD18D37', 'Lot-et-Garonne', '47', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(74, '713589A6BC227083CA880BEEAEBBD753', 'Lozere', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(75, '0E988E538BE70E41D3A91A41BBCEBC90', 'Maine-et-Loire', '49', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(76, '0FA20CC976FB7857A7D0499516A7579C', 'Manche', '50', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(77, '0AFA5D737FFE61B0EAFA945EF28FDFB8', 'Marne', '51', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(78, 'F3F527D05268624A22548E34FCD6E39F', 'Paris', '75', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(79, 'E5FD5FDF6550E4BB25AD88F9D238620D', 'Seine-Saint-Denis', '93', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(80, '64EE3DD677175E583F8A5DF5860D7186', 'Somme', '80', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(81, '22D7FFAC82DCE8F5C3CCA3D52F5DBF2E', 'Tarn', '81', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(82, '7DAC16CAF5DBFA14DBE1182262D1022E', 'Tarn-et-Garonne', '82', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(83, '00808A2089E07541155A8B18FA50DBD5', 'Territoire de Belfort', '90', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(84, '4D8D88B7548D0E083D51B147F954BE17', 'Val-de-Marne', '94', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(85, '2FFB0D1E2348FE745EA8021DD4C2300B', 'Var', '83', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(86, '1597385E8C37C110DE4CBF224BE8CEA7', 'Vaucluse', '84', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(338, '22D7FFACC3CCA82DCE8F53D52F5DBF2E', 'Deux-sevres', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(88, '00441136150F6EB14549490EDDDC84AB', 'Vienne', '86', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(89, '5D13BC70D92370DAEACD858F64733A86', 'Vosges', '88', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(90, 'B0A1CB590B18A67D71E98DBB8DDF04EC', 'Yonne', '89', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(91, 'CFF75DF4332A0E48ED0761F56AB4056B', 'Baden-W', 'BAW', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(92, '4CCE4E7DA0F34C837572E79088F92B34', 'Bayern', 'BAY', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(93, '6114CEB893FB7B66E68366DFC4FF91D3', 'Berlin', 'BER', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(94, 'DD133F4E2BC9128B30BF7711CD35130B', 'Branderburg', 'BRG', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(95, '71C81F8986F90546E1D512F720828A7E', 'Bremen', 'BRE', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(96, '119E5963C4C0D0F327977800823956C0', 'Hamburg', 'HAM', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(97, '73828FED50337E0CB44D9DED2FE10A01', 'Hessen', 'HES', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(98, '87DA16A8A1B17F4E4C060D6A3F26740D', 'Mecklenburg-Vorpommern', 'MEC', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(99, '06A1F371D6A2397776B686B157400F47', 'Niedersachsen', 'NDS', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(100, '2B29DB51D389A2F08275EDA004464918', 'Nordrhein-Westfalen', 'NRW', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(101, '20B10D4E145925F24CBB9EB6698D9DEF', 'Rheinland-Pfalz', 'RHE', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(102, '61A6D3434F72D5F06A8D3EC5E2EF8B3D', 'Saarland', 'SAR', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(103, '093C994766816F6DFED419F172F243EB', 'Sachsen', 'SAS', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(104, 'A19FBEC40F423FE272C0734F073446BE', 'Sachsen-Anhalt', 'SAC', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(105, '2E1133851C91729683B1D4C837515B36', 'Schleswig-Holstein', 'SCN', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(106, '15F05A8838A450082A4E89B55E587322', 'Th', 'THE', 1, 0, 84, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(107, '3905A26F5995719A8DC33EC19C2CF30F', 'Drenthe', 'DR', 1, 0, 154, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(108, 'D5670E8C6C4F67612C47879CB5242F28', 'Flevoland', 'FL', 1, 0, 154, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(109, 'BA102574C4045E0079718EBA0AC9C642', 'Friesland', 'FR', 1, 0, 154, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(110, '2863B4222118E3C61BB4799993E4E8C1', 'Gelderland', 'GE', 1, 0, 154, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(111, 'D0AFF696F21AA97D8089DD622D05FE7C', 'Groningen', 'GR', 1, 0, 154, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(112, 'F7FF14EA0D7F7BA576A170C9011FD0E0', 'Limburg', 'LI', 1, 0, 154, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(113, '0A5353CC3DCB05EA36FAD30779923241', 'Noord Brabant', 'NB', 1, 0, 154, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(114, '4AA03252EC105CAC8199F49B2AA416D2', 'Noord Holland', 'NH', 1, 0, 154, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(115, '503178BFAAE2C297344215C82B5215CD', 'Overijssel', 'OV', 1, 0, 154, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(116, '418E2A240E27BE62A993BFA78561452C', 'Utrecht', 'UT', 1, 0, 154, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(117, '8B8913F2119EA648A7358B88BABA6B5B', 'Zeeland', 'ZE', 1, 0, 154, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(118, '824AD13EF34F98A2A211FF99235D28A1', 'Zuid Holland', 'ZH', 1, 0, 154, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(119, 'C5EA97EDC99C54B962E889748A35A19B', 'Zürich', 'ZH', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(120, '2FB3BAC578F295936B05DA48DDA4DFA7', 'Bern', 'BE', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(121, '2ECC5B5A097EB7A9E551E3AEBD8A20B2', 'Luzern', 'LU', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(122, '5846BC5F94EA4CC14BEABFC23D392AAE', 'Uri', 'UR', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(123, '97F595FAB6F8BC007FFC22AC9CEB9733', 'Schwyz', 'SZ', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(124, '6B4437F16B947D839FE338B69FF890C2', 'Obwalden', 'OW', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(125, '1A2F93423CBB1568F364B0EE42B3C781', 'Nidwalden', 'NW', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(126, 'C5FBB4BF5F7C7C2507556D983F303146', 'Glarus', 'GL', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(127, '5FE709C7DD3F6C03EA0FB8797619FF69', 'Zug', 'ZG', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(128, '1693D9C2BD8E159122E4B2C7E6864E0C', 'Freiburg', 'FR', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(129, '9BAB67CCCB5ACDC888B9E05E9400F664', 'Solothurn', 'SO', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(130, '89C8483C1572258D6E792EA08167EE98', 'Basel-Stadt', 'BS', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(131, '68E1C6A68054520EBC27548F2447B779', 'Basel-Landschaft', 'BL', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(132, 'B99EA19D50B30E0DAE598596FC7E72C7', 'Schaffhausen', 'SH', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(133, '7902DD70B64C6EB40ED97E74B42AFE07', 'Appenzell A. Rh.', 'AR', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(134, '04AE76A72776E58C81E71BFB8C70F10C', 'Appenzell I. Rh.', 'AI', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(135, '6CD181CF0E51424D5AF46FE51E0AB0EA', 'St. Gallen', 'SG', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(136, 'B6488D6E7CC8501ECB822660933645CD', 'Graubünden', 'GR', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(137, 'B54F03C64192B9CE11B2182054BBFF19', 'Aargau', 'AG', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(138, '755FBB7D6FD9845F54AD942E4131EC11', 'Thurgau', 'TG', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(139, '09195FCB9DF13568E000F5BC5598253A', 'Tessin', 'TI', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(140, 'B64B0B7FF5580D119B212C1D371EBE2A', 'Waadt', 'VD', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(141, '7D1944E0C032234E6F7FFAAA44231E52', 'Wallis', 'VS', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(142, '18A195F2A048F7C5AC4366D7C29D440A', 'Neuenburg', 'NE', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(143, '3072779F04CAD5B337519692E2A2013C', 'Genf', 'GE', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(144, 'D6CEB7C904B97EB3FF3E0D10DCF1F26E', 'Jura', 'JU', 1, 0, 210, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(145, 'B2B90F2D39E37AD98FABE356A98B3CAE', 'Aberdeen', 'ABN', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(146, 'E5D81EB5527D2511F32532481DB48855', 'Aberdeenshire', 'ABNS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(147, '5E90013CF527BD6929DEC9B185E4E37E', 'Anglesey', 'ANG', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(148, '82721D0382257A264459116CFBC39EF2', 'Angus', 'AGS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(149, '0DBBD7AD584B423A783833188A12FB02', 'Argyll and Bute', 'ARY', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(150, '0E9AEB16D3698B0CC5A108AA6C863A2E', 'Bedfordshire', 'BEDS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(151, 'CDF6A8B7E25335C83BE223001624CE9D', 'Berkshire', 'BERKS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(152, 'EAD8337D375D2554C61C95BD6D8B138D', 'Blaenau Gwent', 'BLA', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(153, 'E54B23E320366145CD2378568FD1B111', 'Bridgend', 'BRI', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(154, '3A20299452A4F5D9A5FD0DD041CBA05B', 'Bristol', 'BSTL', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(155, '2D3B26EAA758E5A2A42C542282F54C9D', 'Buckinghamshire', 'BUCKS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(156, '0745C6352396DEEEF071121B60310DC6', 'Caerphilly', 'CAE', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(157, 'F6DBD53AD193D888516EEC182A7AE8D4', 'Cambridgeshire', 'CAMBS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(158, 'E563AE804402DA5ED9FF42B878E6248D', 'Cardiff', 'CDF', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(159, '50631FE7C33580554B70257C55D7F302', 'Carmarthenshire', 'CARM', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(160, 'D536F69F51A4FFF894B6D5B047D5EF70', 'Ceredigion', 'CDGN', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(161, 'D4A4F6640637A6811292FFDAEFDE562E', 'Cheshire', 'CHES', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(162, 'E06837885CB58FD90DC67E0926B3DBAC', 'Clackmannanshire', 'CLACK', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(163, '5F3D941526DD8B8237A838D03451E04E', 'Conwy', 'CON', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(164, 'E51687E78AC8205E290B21194506609D', 'Cornwall', 'CORN', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(165, 'F16F8B81CC76BACBA2F1C2F1830A7FC9', 'Denbighshire', 'DNBG', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(166, '63E2B4C1B0F6C06C18599BF489E04B68', 'Derbyshire', 'DERBY', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(167, 'D51825CF0EC49AAEDE4FE500120D305E', 'Devon', 'DVN', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(168, '7A1C09BA1369146701B9011AD6059436', 'Dorset', 'DOR', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(169, 'D2345F1526BD85481867380F11198D70', 'Dumfries and Galloway', 'DGL', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(170, 'F56D9CC045651D9936D2EC01D9E34D1F', 'Dundee', 'DUND', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(171, '1D86F1749452D42E9458C207B03F3D7C', 'Durham', 'DHM', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(172, 'D09B3407B4001625ADD6332AFB86107C', 'East Ayrshire', 'ARYE', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(173, '2DA3CC656D6B475D4C9B8D535093A2E9', 'East Dunbartonshire', 'DUNBE', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(174, '87592F06AD2E4EC0081759F7968FA5F0', 'East Lothian', 'LOTE', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(175, '21461543959BB32C75C5A602740D4AC7', 'East Renfrewshire', 'RENE', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(176, 'CFFB3A064025C0B600C96445A40D833D', 'East Riding of Yorkshire', 'ERYS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(177, '28EAE6BC3AF109241D85359C0F0297F9', 'East Sussex', 'SXE', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(178, 'FDD963736164E4F5EFD167A52DD6EB3F', 'Edinburgh', 'EDIN', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(179, '43BBECD12ED1F990A34996982806D6A8', 'Essex', 'ESX', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(180, '65BBE3C729AEB4154602D011282A2CB6', 'Falkirk', 'FALK', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(181, '239C3D3E207A758A834622565F119C35', 'Fife', 'FFE', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(182, 'B77A162D3506CFCFF2EFEB630D94AB80', 'Flintshire', 'FLINT', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(183, '36368AFDF928787AD98104D425AD7ACD', 'Glasgow', 'GLAS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(184, 'D328D4A0FB05DF3EF145C80AC45F1B89', 'Gloucestershire', 'GLOS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(185, 'D2F5D4128CA6BCAF34C4E6C65CADB357', 'Greater London', 'LDN', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(186, 'E73275385DB19266AE60E69520AA52A7', 'Greater Manchester', 'MCH', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(187, '8C0C1FB07EC3024678E52DC98DC1912B', 'Gwynedd', 'GDD', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(188, '41513840012B09E7C54311B1EB2B5DF4', 'Hampshire', 'HANTS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(189, 'D57B4232E9B0409ECF15118CFB419BB8', 'Herefordshire', 'HWR', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(190, '88727FABC1C8AB71082759775167E360', 'Hertfordshire', 'HERTS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(191, 'C498B6CEFE110274BDBA4E623631B63E', 'Highlands', 'HLD', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(192, 'C025806AAF59B44379B1084304D2217F', 'Inverclyde', 'IVER', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(193, '7BABA1F54550FCE1684BF9D5E6327EEA', 'Isle of Wight', 'IOW', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(194, '078BA257CF9BDBAB7C20345A2EB63C38', 'Kent', 'KNT', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(195, 'C325944C8BA9034F033C68B7134FC8AC', 'Lancashire', 'LANCS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(196, '0A94BFA1AD5490C86D67E43A3953E354', 'Leicestershire', 'LEICS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(197, '3BD61D5AF034308003A7A448AC1B9A2C', 'Lincolnshire', 'LINCS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(198, 'E3A54F172ED48351E49801C9C126F26F', 'Merseyside', 'MSY', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(199, '1D6EEC46574705F2A8F19AEA84DDDB4B', 'Merthyr Tydfil', 'MERT', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(200, 'D524C81BCE9B6A1BF1D1AD0799C9996A', 'Midlothian', 'MLOT', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(201, 'DDD7D5936BBEE1C16A5508B00E8294B6', 'Monmouthshire', 'MMOUTH', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(202, 'B664F0421704B547405B5F6F9E52A72E', 'Moray', 'MORAY', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(203, '759428644F245C0207115A3FEC348569', 'Neath Port Talbot', 'NPRTAL', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(204, 'FB35AF256FEA6381D3C8CB70884D7213', 'Newport', 'NEWPT', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(205, 'ECCEDE2B07FB928333CF78B7CECD86B6', 'Norfolk', 'NOR', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(206, 'C5AD15A84566F456E042C5C6FE07FA90', 'North Ayrshire', 'ARYN', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(207, 'B6DE9503360C16926FEAD611EDE3A66F', 'North Lanarkshire', 'LANN', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(208, '5A6EEBF5C814F0BD3522D5A8ECFDD3BC', 'North Yorkshire', 'YSN', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(209, '28BDC31B70C52F57D35BF7F7D6E6ADD9', 'Northamptonshire', 'NHM', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(210, 'E24141FB52BA069A982C2DDADDF8983D', 'Northumberland', 'NLD', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(211, '037D90C3E21EA734199BD37163192D63', 'Nottinghamshire', 'NOT', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(212, '2B695BBCE01B230F1D0A591DD39039B6', 'Orkney Islands', 'ORK', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(213, '808844CB3ADD11F90B5241A13623E89E', 'Oxfordshire', 'OFE', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(214, 'FA7562E92416317BE69DEEBFD55A3E29', 'Pembrokeshire', 'PEM', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(215, 'A44846288609B570AC67BA921BC03529', 'Perth and Kinross', 'PERTH', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(216, 'B2B3E50AC91A3281A7F63203D815B2A6', 'Powys', 'PWS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(217, 'FB1D43217C4BA9E50F6C7C324B3BFA59', 'Renfrewshire', 'REN', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(218, 'E52170A73A45D7036CDA8920C229F594', 'Rhondda Cynon Taff', 'RHON', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(219, '503FFE3C8423A97999937374040CA19B', 'Rutland', 'RUT', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(220, '9DE9ACCBA99F7B781E8E00FDDB19ABF6', 'Scottish Borders', 'BOR', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(221, '4A5112F93501D11AD974B6A47C5F5A78', 'Shetland Islands', 'SHET', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(222, '92868461F86E23573A1E4F2FD6217B82', 'Shropshire', 'SPE', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(223, 'E01CDDDCCD06583BD30A725780DD0EA2', 'Somerset', 'SOM', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(224, '91D2BE11BAF720890AFAC4F43EE7A8DF', 'South Ayrshire', 'ARYS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(225, '4134D204D585DAA0FF6E84D930CCA049', 'South Lanarkshire', 'LANS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(226, '517A2EEBAFAD3513E98F571AAC2CE588', 'South Yorkshire', 'YSS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(227, '1489A9D9807B2E260D3C8398EEA19606', 'Staffordshire', 'SFD', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(228, '4F4F201A72C34CDCDD29F242A2182F93', 'Stirling', 'STIR', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(229, '4F288A6BE44C36E3E7A2DB6A2CA8979A', 'Suffolk', 'SFK', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(230, '61AEDBEB105F2719774BF41D8D3D11DF', 'Surrey', 'SRY', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(231, '8E33E47CECDF8937FFA0DAD7253A8AE7', 'Swansea', 'SWAN', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(232, '8A529ED5881173E3C81DB24AA4AFD1A1', 'Torfaen', 'TORF', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(233, '56D463954209B3948AAB37A403CA7CB9', 'Tyne and Wear', 'TWR', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(234, '329E65E0099ECC81A6F8CD090B585D2F', 'Vale of Glamorgan', 'VGLAM', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(235, '67B744BB864006BF82190D89DC68421D', 'Warwickshire', 'WARKS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(236, '731A191A704D60C3694F5AE3BCC2481E', 'West Dunbartonshire', 'WDUN', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(237, 'EEEFBAE200E64A0B7A0621F6FB6EE4BE', 'West Lothian', 'WLOT', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(238, '888087A0EF030D0F203AD3AE70DEE2F8', 'West Midlands', 'WMD', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(239, 'A997BD84E280F5267EC5B209C2E2BCEB', 'West Sussex', 'SXW', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(240, '08FAF03CCA09621FC12A4C3D8C225B08', 'West Yorkshire', 'YSW', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(241, 'A0AB6A6A5796C6770C9A799EA659B6E1', 'Western Isles', 'WIL', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(242, '665217C852F364603AB51E392AC51ED4', 'Wiltshire', 'WLT', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(243, '71789AB17613FD85F7BF4F1AD4C93544', 'Worcestershire', 'WORCS', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(244, 'EDE3926E454D096B360062116D243607', 'Wrexham', 'WRX', 1, 0, 228, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(245, '786D3A7421F1C4CFF86167310BEBA00A', 'Alabama', 'AL', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(246, '3DEC1B60342F58F6DAF3EAC85520F07A', 'Alaska', 'AK', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(247, '41DEA2DD100958C404827280F71E0C82', 'Arizona', 'AZ', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(248, '1ACF51D6EF2A63CA7AD5D056FCE30AA3', 'Arkansas', 'AR', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(249, '9F3123CDCF6BEBD71248F7E89BBD72E7', 'California', 'CA', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(250, '5E58E13ED4A3FF5B0AFB9E1E5745B4B6', 'Colorado', 'CO', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(251, '9D7D28EAEDCA78E07DB21A18785DF4C8', 'Connecticut', 'CT', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(252, '12B19FC08B82A09C43481250FC3B9F8E', 'Delaware', 'DE', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(253, '80FD8431674EF954CAD93D63BA0DE775', 'District of Columbia', 'DC', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(254, '219A522B6D6221DA8E90D789FFAD1E04', 'Florida', 'FL', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(255, 'B5EB67E7A73A92011424988816602509', 'Georgia', 'GA', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(256, 'C48E05E630D8E83ACD528A951CE0CC35', 'Guam', 'GU', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(257, 'BEEDA73DE18A8AB730D87A702608D525', 'Hawaii', 'HI', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(258, '67C25613518FDAA76AC1F13A325C4A4C', 'Idaho', 'ID', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(259, 'F135ECB14BAAF4AF92CACD3013BCA166', 'Illinois', 'IL', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(260, '3C22ABCCC4AE3E664E47B4D68EC54DFF', 'Indiana', 'IN', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(261, 'E3686EB7B31009770CF1C5063B5ABA49', 'Iowa', 'IA', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(262, 'CB30DD3E2E0A586CCEC03666E5C6A932', 'Kansas', 'KS', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(263, '8FB5E7371A93A547E9FF23A17DCA9EF8', 'Kentucky', 'KY', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(264, '860A5C150CEEB2C88629BE611F82BF58', 'Louisiana', 'LA', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(265, '655C301ED77CA1329AC65829235F8AC2', 'Maine', 'ME', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(266, '3DA533B4CFEB8009CCC665C9FE409EFB', 'Maryland', 'MD', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(267, '9F9FBDCFF37CA6FC762FA2E398542EAB', 'Massachusetts', 'MA', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(268, 'F03848C5A7BE50E525CDC9355C99B427', 'Michigan', 'MI', 1, 0, 229, 0, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(269, 'EFD342755DC8B604CC4064599D663314', 'Minnesota', 'MN', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(270, '1E04501AEE489901D495F2DBAF0AB4D6', 'Mississippi', 'MS', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(271, 'DD4F433FF7D24489E0CF52106E8DADFB', 'Missouri', 'MO', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(272, 'F2458E23E6E0969E29AF420373B3D424', 'Montana', 'MT', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(273, 'B82D34ADE8ED88B3EC01F98E4B4CB648', 'Nebraska', 'NE', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(274, '1CA9F3C1815E30775C9921CE59AA2FB2', 'Nevada', 'NV', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(275, '34495A98B85FCFDACFE9A47BB5C92E27', 'New Hampshire', 'NH', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(276, 'BAEE6FCD4AFDE527C064D62B282B2C22', 'New Jersey', 'NJ', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(277, '8F9D7E1A9113BB53C94CC402056A97DB', 'New Mexico', 'NM', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(278, '8ED6BC9CA98A4BB81D71310B24372076', 'New York', 'NY', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(279, '187DDADD798179AC48FC2007F8842E2C', 'North Carolina', 'NC', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(280, '86FB711603A51E2F88E9F8DD3111D071', 'North Dakota', 'ND', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(281, '73E301E57C9126742B4EB203E06793AD', 'Ohio', 'OH', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(282, '40E52C18C201D06FCA7427CDED493151', 'Oklahoma', 'OK', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(283, 'F52F2BD73BE18A26D8D78BB38DDB4D5C', 'Oregon', 'OR', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(284, 'C3027F33B2487CA5F1F407E4AAF886E7', 'Pennsylvania', 'PA', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(285, 'DFECFB214C2D35A909686DFD4DF2A890', 'Puerto Rico', 'PR', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(286, '8AD59C2B840A935D58A1D37CA5FAE19F', 'Rhode Island', 'RI', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(287, '93D56F847C123103634F173CB6DCA7A2', 'South Carolina', 'SC', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(288, '5555E35EBBC987B4F54E262B70D721FF', 'South Dakota', 'SD', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(289, 'F2E83DF3F007E3CA312290A596B01407', 'Tennessee', 'TN', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(290, 'FBF516A32BBCA950EFCBDE319EF2E0C3', 'Texas', 'TX', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(291, 'B6A0F95C674A14AB722D48B98DFF8F93', 'Utah', 'UT', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(292, 'AD50A0D29D8FA6A7DBA2C9DB8E71B014', 'Vermont', 'VT', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(293, '5E29C78E4619734F91FF4B7D7697101C', 'Virgin Islands', 'VI', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(294, '5F3016D7CCE2558B42575007ABFCD22E', 'Virginia', 'VA', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(295, '8F3B8FEDD32BE5F30A71FF9BAB905323', 'Washington', 'WA', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(296, '2877A8D59B26404B11040A4509AF3D71', 'West Virginia', 'WV', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(297, '19A72E1E5A1210C3FC81994EA12B7C3D', 'Wisconsin', 'WI', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(298, '6D0C2154BBB7188B8E36619E4CD2FE62', 'Wyoming', 'WY', 1, 0, 229, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(314, '72EF3C6A6C47358FA757A475FF436227', 'New South Wales', 'NSW', 1, 0, 15, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(315, '86D0DEEEF2581C223AAA9A292B86909C', 'Hautes-alpes', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(316, 'FE6D2A18B3C526C2B0D21CC20E432012', 'Cotes-d-armor', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(322, '0E9883A91A41BBCE538BE70E41DEBC90', 'Meuse', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(323, '71358980BEEAA6BC227083CA8EBBD753', 'Morbihan', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(324, 'CD8EB8A394E7A8F71086D84245F461E2', 'Moselle', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(326, '7135898027083CA8EBBEEAA6BC2BD753', 'Nord', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(327, ' 0E9BBCE53883A91A418BE70E41DEBC90 ', 'Oise', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(328, '0FB785AA20CC976F7A7D04995167579C', 'Orne', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(329, '70671AF89A16EA4B7615E03220D9B27B', 'Pas-de-calais', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(330, '0AFA50EAFA945ED737FFE61BF28FDFB8', 'Puy-de-dome', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(331, '0F857A7D049951A20CC976FB76A7579C', 'Pyrenees-atlantiques', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(332, '0E988E41D3A91A41BE538BE70BCEBC90', 'Hautes-pyrenees', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(333, '71357083CA8889A6BC220BEEAEBBD753', 'Pyrenees-orientales', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(334, 'F8C47AAE5E403BADA85E5B4E85127197', 'Bas-rhin', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(335, '79CDB63811B5505B54C99D5A0F235C73', 'Haut-rhin', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(336, 'F3F52722548E3D05268624A4FCD6E39F', 'Rhone', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(337, '0FB78A7D049951675AA20CC976F7579C', 'Haute-saone', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(339, '00808A41152089E0755A8B18FA50DBD5', 'Essonne', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(340, '4D8D8083D51B8B7548D0E147F954BE17', 'Haute-savoie', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(341, '7E5C5A60CE5B13B8CCF7660F87E740B5', 'Haute-vienne', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(342, 'C746EF4802C1358A7D883E0B245BF464', 'Hauts-de-seine ', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(343, '5A168E4F314EA245A1134B26969BE657', 'Saone-et-loire', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(344, 'BFC096D03006CF455563A507A2763D16', 'Sarthe', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(345, '79CDB6305A811B55B54C99D50F235C73', 'Savoie', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(346, '6DF6F58A9DCA2C7905BD32DB62912411', 'Seine-maritime', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(347, 'CF835C5189CDFC511CDF446563712A81', 'Seine-et-marne', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(348, 'F8C47403BAAAE5E4EDA85E5B85127197', 'Yvelines', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(349, 'FE6D2A18BB0D3C526C221CC20E432012', 'Vendee', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(350, '080C6AD8C443EE09F1960A72072ED6FE', 'Vosges', '', 1, 0, 76, 1, 100, '', '', '', '', '');
INSERT INTO `ps4_states` VALUES(351, '41D0EAD79005D86757FEEC38FFBF25A0', 'Val-d-oise', '', 1, 0, 76, 1, 100, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ps4_storage`
--

CREATE TABLE `ps4_storage` (
  `storage_id` int(3) NOT NULL AUTO_INCREMENT,
  `ustorage_id` varchar(40) NOT NULL,
  `name` varchar(200) NOT NULL,
  `storage_type` varchar(20) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `host` varchar(100) NOT NULL,
  `port` int(5) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `path` varchar(255) NOT NULL,
  UNIQUE KEY `storage_id` (`storage_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

--
-- Dumping data for table `ps4_storage`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_subscriptions`
--

CREATE TABLE `ps4_subscriptions` (
  `sub_id` int(6) NOT NULL AUTO_INCREMENT,
  `usub_id` varchar(40) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `homepage` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `notes` text NOT NULL,
  `price` float NOT NULL,
  `credits` int(6) NOT NULL,
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `everyone` tinyint(1) NOT NULL DEFAULT '0',
  `sortorder` int(3) NOT NULL,
  `downloads` int(5) NOT NULL DEFAULT '0',
  `tdownloads` int(6) NOT NULL,
  `durvalue` int(5) NOT NULL DEFAULT '0',
  `durrange` varchar(10) NOT NULL,
  `downitems` varchar(100) NOT NULL,
  `item_name_dutch` varchar(255) NOT NULL,
  `description_dutch` text NOT NULL,
  `item_name_english` varchar(255) NOT NULL,
  `description_english` text NOT NULL,
  `item_name_german` varchar(255) NOT NULL,
  `description_german` text NOT NULL,
  `item_name_spanish` varchar(255) NOT NULL,
  `description_spanish` text NOT NULL,
  `item_name_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL,
  `item_code` varchar(250) NOT NULL,
  UNIQUE KEY `sub_id` (`sub_id`),
  KEY `active` (`active`,`homepage`,`featured`,`deleted`,`everyone`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_subscriptions`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_taxes`
--

CREATE TABLE `ps4_taxes` (
  `tax_id` int(6) NOT NULL AUTO_INCREMENT,
  `region_type` tinyint(1) NOT NULL,
  `region_id` int(6) NOT NULL,
  `tax_a` decimal(6,3) NOT NULL,
  `tax_b` decimal(6,3) NOT NULL,
  `tax_c` decimal(6,3) NOT NULL,
  `tax_inc` tinyint(1) NOT NULL DEFAULT '0',
  `tax_prints` tinyint(1) NOT NULL DEFAULT '0',
  `tax_digital` tinyint(1) NOT NULL DEFAULT '0',
  `tax_subs` tinyint(1) NOT NULL DEFAULT '0',
  `tax_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `tax_credits` tinyint(1) NOT NULL DEFAULT '0',
  `tax_ms` tinyint(1) NOT NULL DEFAULT '0',
  `tax_a_digital` decimal(6,3) NOT NULL,
  `tax_b_digital` decimal(6,3) NOT NULL,
  `tax_c_digital` decimal(6,3) NOT NULL,
  UNIQUE KEY `tax_id` (`tax_id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_taxes`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_tickets`
--

CREATE TABLE `ps4_tickets` (
  `ticket_id` int(8) NOT NULL AUTO_INCREMENT,
  `member_id` int(8) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `opened` datetime NOT NULL,
  `lastupdated` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `updatedby` int(4) NOT NULL,
  `lastread` datetime NOT NULL,
  `viewed` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `ticket_id` (`ticket_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_tickets`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_ticket_files`
--

CREATE TABLE `ps4_ticket_files` (
  `file_id` int(6) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(6) NOT NULL,
  `original_name` varchar(200) NOT NULL,
  `saved_name` varchar(40) NOT NULL,
  `uploaddate` datetime NOT NULL,
  `admin_id` int(4) NOT NULL DEFAULT '0',
  UNIQUE KEY `file_id` (`file_id`),
  KEY `ticket_id` (`ticket_id`),
  KEY `admin_id` (`admin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_ticket_files`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_ticket_messages`
--

CREATE TABLE `ps4_ticket_messages` (
  `message_id` int(8) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(8) NOT NULL,
  `message` text NOT NULL,
  `submit_date` datetime NOT NULL,
  `admin_response` tinyint(1) NOT NULL DEFAULT '0',
  `admin_id` int(4) NOT NULL,
  UNIQUE KEY `message_id` (`message_id`),
  KEY `ticket_id` (`ticket_id`),
  KEY `admin_id` (`admin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_ticket_messages`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_toolslinks`
--

CREATE TABLE `ps4_toolslinks` (
  `tl_id` int(3) NOT NULL AUTO_INCREMENT,
  `tl_name` varchar(250) NOT NULL DEFAULT '',
  `tl_link` text NOT NULL,
  `tl_target` varchar(10) NOT NULL DEFAULT '',
  `tl_owner` int(4) NOT NULL DEFAULT '0',
  UNIQUE KEY `tl_id` (`tl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `ps4_toolslinks`
--

INSERT INTO `ps4_toolslinks` VALUES(30, 'Ktools.net Website', 'http://www.ktools.net', '_blank', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ps4_transactions`
--

CREATE TABLE `ps4_transactions` (
  `trans_id` int(6) NOT NULL AUTO_INCREMENT,
  UNIQUE KEY `trans_id` (`trans_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_transactions`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_usedcoupons`
--

CREATE TABLE `ps4_usedcoupons` (
  `uc_id` int(5) NOT NULL AUTO_INCREMENT,
  `mem_id` int(5) NOT NULL,
  `promo_id` int(5) NOT NULL,
  `usedate` datetime NOT NULL,
  UNIQUE KEY `uc_id` (`uc_id`),
  KEY `mem_id` (`mem_id`),
  KEY `promo_id` (`promo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_usedcoupons`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_wp_notes`
--

CREATE TABLE `ps4_wp_notes` (
  `note_id` int(5) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `date_added` datetime NOT NULL,
  `owner` int(4) NOT NULL DEFAULT '0',
  `viewable` int(4) NOT NULL DEFAULT '0',
  UNIQUE KEY `note_id` (`note_id`),
  KEY `owner` (`owner`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_wp_notes`
--


-- --------------------------------------------------------

--
-- Table structure for table `ps4_wp_stats`
--

CREATE TABLE `ps4_wp_stats` (
  `stat_id` int(1) NOT NULL AUTO_INCREMENT,
  `statmode` varchar(10) NOT NULL DEFAULT 'sales',
  `statlength` varchar(10) NOT NULL DEFAULT 'days',
  UNIQUE KEY `stat_id` (`stat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ps4_wp_stats`
--

INSERT INTO `ps4_wp_stats` VALUES(1, 'sales', 'years');

-- --------------------------------------------------------

--
-- Table structure for table `ps4_zipcodes`
--

CREATE TABLE `ps4_zipcodes` (
  `zipcode_id` int(6) NOT NULL AUTO_INCREMENT,
  `uzipcode_id` varchar(40) CHARACTER SET latin1 NOT NULL,
  `zipcode` varchar(25) CHARACTER SET latin1 NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `country_id` int(5) DEFAULT NULL,
  `state_id` int(5) DEFAULT NULL,
  `ship_percentage` int(4) NOT NULL DEFAULT '100',
  UNIQUE KEY `zipcode_id` (`zipcode_id`),
  KEY `country_id` (`country_id`),
  KEY `state_id` (`state_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ps4_zipcodes`
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
	define('BASE_PATH',dirname(__FILE__)); // Define the base path
	
	require_once BASE_PATH.'/session.php';
	require_once BASE_PATH.'/config.php';
	require_once BASE_PATH.'/lang.php';
	$inc = 1;
	require_once BASE_PATH.'/version.php';
	
	if($_SESSION['proInstall']) // If this is a pro install then skip this step
	{
		header('location: step1.php');
		exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?php require_once BASE_PATH.'/head.php'; ?>
</head>
<body>
	<form action="step1.php" method="post">
	<div id="container">
		<div id="installerBox">
			<p class="headerIcons">
				<a href="phpinfo.php" target="_blank"><img src="./images/php.logo.png" class="prodLogo opac40" title="PHP Info"></a>
				<a href="http://www.ktools.net/photostore/" target="_blank"><img src="./images/prod.logo.png" class="prodLogo opac40" title="Ktools.net PhotoStore"></a>
			</p>
			<h1 class="stepOn" style="margin-left: 20px; border-left: none;">PhotoStore Installer</h1>
			<div id="content">
				<p>Welcome to the PhotoStore product installer. You will be guided through 5 steps to complete the installation process. If at any time you need assistance please login to your Ktools.net account and open a support ticket.</p>
				<p>Please press the <strong>Continue</strong> button below to continue on to <strong>Step 1</strong>.</p>
				<p class="buttonRow"><input type="submit" value="Continue &raquo;"></p>
			</div>
			<?php require_once BASE_PATH.'/footer.php'; ?>
		</div>
	</div>
	</form>
</body>
</html>

<?php
	define('BASE_PATH',dirname(__FILE__)); // Define the base path
	define('ROOT_PATH',dirname(dirname(__FILE__))); // Define the base path
	
	require_once BASE_PATH.'/session.php';
	require_once BASE_PATH.'/config.php';
	require_once BASE_PATH.'/lang.php';
	$inc = 1;
	require_once BASE_PATH.'/version.php';
?>
<!DOCTYPE html>
<html>
<head>
	<?php require_once BASE_PATH.'/head.php'; ?>
	<script type="text/javascript" language="javascript">
		$(function()
		{
			// Go back to the previous page
			$('#formBackButton').click(function()
			{
				goto('step5.php');
			});
		});
	</script>
</head>
<body>
	<form action="../manager/" method="post">
	<div id="container">
		<div id="installerBox">
			<p class="headerIcons">
				<a href="phpinfo.php" target="_blank"><img src="./images/php.logo.png" class="prodLogo opac40" title="PHP Info"></a>
				<a href="http://www.ktools.net/photostore/" target="_blank"><img src="./images/prod.logo.png" class="prodLogo opac40" title="Ktools.net PhotoStore"></a>
			</p>
			<h1 class="stepOn" style="margin-left: 20px; border-left: none;">Installation Complete</h1>
			<div id="content">
				<p>
					Installation of PhotoStore has been completed. Please click the continue button to login to your PhotoStore management area using username: <strong>admin</strong> and password: <strong>admin</strong>.
				</p>
				
				<?php
					if(file_exists(ROOT_PATH.'/install') or file_exists(ROOT_PATH.'/photostore.zip'))
					{
						echo "<div id='warningsBox'>";
						echo "<h2 class='checkLow'>Warning</h2>";
						echo "<ul class='warnings'>";
							if(file_exists('../install')) echo "<li>You can now delete the <strong>install</strong> directory.</li>";
							if(file_exists('../ps.zip')) echo "<li>Please delete the <strong>photostore.zip</strong> file.</li>";
						echo "</ul>";
						echo "</div>";
					}
				?>
				
				<p class="buttonRow"><input type="button" value="&laquo; Back" id="formBackButton"><input type="submit" value="Continue &raquo;" id="formSubmitButton" ></p>
			</div>
			<?php require_once BASE_PATH.'/footer.php'; ?>
		</div>
	</div>
	</form>
</body>
</html>

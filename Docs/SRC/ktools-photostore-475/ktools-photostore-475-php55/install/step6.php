<?php
	define('BASE_PATH',dirname(__FILE__)); // Define the base path
	
	require_once BASE_PATH.'/session.php';
	require_once BASE_PATH.'/config.php';
	require_once BASE_PATH.'/lang.php';
	require_once BASE_PATH.'/functions.php';
	$inc = 1;
	require_once BASE_PATH.'/version.php';
	
	if(!$_SESSION['activationInfo'])
		die('No activation information is present in the session.'); // Make sure the activation session is present
	
	if($_POST['dbInfoForm']) // See if this is coming from the dbInfoForm
	{
		if($_POST['databaseHost'] and $_POST['databaseUsername'] and $_POST['databasePassword'] and $_POST['databaseName'])
		{
			$databaseHost = trim($_POST['databaseHost']);
			$databaseUsername = trim($_POST['databaseUsername']);
			$databasePassword = trim($_POST['databasePassword']);
			$databaseName = trim($_POST['databaseName']);
			$tablePrefix = trim($_POST['tablePrefix']);
			
			// Check the connections to the db
			if(@$db = mysqli_connect($databaseHost, $databaseUsername, $databasePassword, $databaseName))
			{
				//$db = mysqli_connect($dbinfo['host'], $dbinfo['username'], $dbinfo['password'], $dbinfo['name']);
				
				if(!mysqli_connect_errno())
				{
					$dbConfigContent = "<?php\n";
					$dbConfigContent.= "\$dbinfo['username']='{$databaseUsername}';\n";
					$dbConfigContent.= "\$dbinfo['password']='{$databasePassword}';\n";
					$dbConfigContent.= "\$dbinfo['name']='{$databaseName}';\n";
					$dbConfigContent.= "\$dbinfo['host']='{$databaseHost}';\n";
					$dbConfigContent.= "\$dbinfo['pre']='{$tablePrefix}';\n";
					$dbConfigContent.= "\$actkey='{$_SESSION[activationInfo][oldKey]}';\n";
					$dbConfigContent.= "?>";		
					
					$dbConfigFile = '../assets/includes/db.config.php';
					
					//$tablePrefix = $_POST['tablePrefix'];
					
					if(file_exists($dbConfigFile))
					{
						@chmod('../assets/includes/'.$dbConfigFile,0777);
						
						if(@$openedFile = fopen($dbConfigFile,'w'))
						{
							if(fputs($openedFile,$dbConfigContent))
							{	
								$fileName = './db/database.sql'; // Name of the file							
								$tempLine = ''; // Temporary variable, used to store current query							
								$lines = file($fileName); // Read in entire file
								
								// Loop through each line
								foreach ($lines as $lineNum => $line)
								{
									// Only continue if it's not a comment
									if(substr($line, 0, 2) != '--' and $line != '' and substr($line, 0, 2) != '/*')
									{
										// Add this line to the current segment
										$tempLine.=$line;
										// If it has a semicolon at the end, it's the end of the query
										if(substr(trim($line), -1, 1) == ';')
										{	
											$tempLine = str_replace('ps4_',$tablePrefix,$tempLine); // Update table prefix
											//$tempLine = str_replace('"', '\\"', $tempLine);
											//$tempLine = str_replace('\$', '\\$', $tempLine);										
											//$output.= "{$tempLine}<br />";
											if(!mysqli_query($db, $tempLine)) // Do query
												$mysqlError = true;
												//$instError[] = mysql_error();
											$tempLine = ''; // Reset temp variable to empty
										}
									}
								}
								
								$checkLastTable = mysqli_query($db,"SHOW TABLES LIKE '{$tablePrefix}zipcodes'");
								if($testQueryRows = mysqli_num_rows($checkLastTable)) // Test to make sure the last table was created
								{
									// Fill URL, lib path, import path
									$fillURL = dirname(curPageURL()); // Current URL;
									$fillLibPath = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'library'; // Library path on the server
									$fillIncomingPath = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'incoming'; // Incoming path on the server
									
									$fillLibPath = addslashes($fillLibPath); // Fix for windows servers
									$fillIncomingPath = addslashes($fillIncomingPath);
									
									$adminPassword = k_encrypt('admin',$_SESSION['activationInfo']['serialNumber']);
									
									// Update admins database
									mysqli_query($db,
										"
										UPDATE {$tablePrefix}admins SET 
										wizard='1',
										password='{$adminPassword}'
										WHERE admin_id = 3
										"
									);
									
									// Update settings database
									mysqli_query($db,
										"
										UPDATE {$tablePrefix}settings SET 
										serial_number='{$_SESSION[activationInfo][serialNumber]}',
										newkey='{$_SESSION[activationInfo][newKey]}',
										lockkey='{$_SESSION[activationInfo][lockKey]}',
										site_url='{$fillURL}',
										library_path='{$fillLibPath}',
										incoming_path='{$fillIncomingPath}',
										debug_pass='{$_SESSION[activationInfo][mgrPass]}',
										api_pass='{$_SESSION[activationInfo][apiPassEnc]}'
										WHERE settings_id = 1
										"
									);
									
									header('location: finished.php');
									exit;
								}
								else
									$instError[] = 'MySQL query failed. Please try again.';
							
								@chmod('../assets/includes/'.$dbConfigFile,0755);
							}
							else
								$instError[] = 'Could not write to db.config.php.';	
						}
						else
							$instError[] = 'Could not open db.config.php for writing. Please give the file /assets/includes/db.config.php write permissions (CHMOD 777) and try again.';
					}
					else
						$instError[] = 'db.config.php file does not exist.';	
				}
				else
					$instError[] = 'Database not found.';
			}
			else
				$instError[] = 'Database connection failed: '.mysqli_connect_error();	
		}
		else
			$instError[] = 'Some information was not passed from the form.';
	}
	
?>
<!DOCTYPE html>
<html>
<head>
	<?php require_once BASE_PATH.'/head.php'; ?>
	<script type="text/javascript" language="javascript">
		$(function()
		{
			// Go back to the previous page
			$('#formBackButton').click(function()
			{
				goto('step5.php');
			});
			
			// Launch support window
			$('.helpIcon').click(function()
			{
				supportPopup(0);
			});
		});
	</script>
</head>
<body>
	<form action="step6.php" method="post">
	<input type="hidden" value="1" name="dbInfoForm">
	<div id="container">
		<div id="installerBox">
			<p class="headerIcons">
				<a href="phpinfo.php" target="_blank"><img src="./images/php.logo.png" class="prodLogo opac40" title="PHP Info"></a>
				<a href="http://www.ktools.net/photostore/" target="_blank"><img src="./images/prod.logo.png" class="prodLogo opac40" title="Ktools.net PhotoStore"></a>
			</p>
			<h1 class="stepOff" style="margin-left: 20px; border-left: none;">Server Check</h1>
			<h1 class="stepOff">Activation</h1>
			<h1 class="stepOff">Add-ons</h1>
			<h1 class="stepOff">Install</h1>
			<h1 class="stepOff">Ioncube</h1>
			<h1 class="stepOn">Database</h1>
			<div id="content">
				<p>Please enter your database information below and click <strong>Continue</strong>.</p>
				<?php
					if(!$databaseHost) $databaseHost = 'localhost';
					if(!$tablePrefix) $tablePrefix = 'ps4_';
					
					if($instError)
					{
						echo "<div id='fatalErrorsBox'>";
						echo "<h2 class='checkFailed'>Fatal Errors</h2>";
						echo "<ul class='fatalErrors'>";
						foreach($instError as $key => $error)
						{
							echo "<li>{$error}</li>";	
						}
						echo "</ul>";
						echo "</div>";
					}
				?>
				<div class="divTable tableForm">
					<div class="divTableRow">
						<div class="divTableCell formQuestion"><img src="./images/question.png" class="helpIcon"></div>
						<div class="divTableCell formLabel">Database Name:<br><span class="infoTag"></span></div>
						<div class="divTableCell formFieldCell"><input type="text" value="<?php echo @$databaseName; ?>" name="databaseName" style="width: 200px;"></div><!--ktools_ps3dev2-->
					</div>
					<div class="divTableRow">
						<div class="divTableCell formQuestion"><img src="./images/question.png" class="helpIcon"></div>
						<div class="divTableCell formLabel">Database Username:<br><span class="infoTag"></span></div>
						<div class="divTableCell formFieldCell"><input type="text" value="<?php echo @$databaseUsername; ?>" name="databaseUsername" style="width: 200px;"></div>
					</div>
					<div class="divTableRow">
						<div class="divTableCell formQuestion"><img src="./images/question.png" class="helpIcon"></div>
						<div class="divTableCell formLabel">Database Password:<br><span class="infoTag"></span></div>
						<div class="divTableCell formFieldCell"><input type="text" value="<?php echo @$databasePassword; ?>" name="databasePassword" style="width: 200px;"></div>
					</div>
					<div class="divTableRow">
						<div class="divTableCell formQuestion"><img src="./images/question.png" class="helpIcon"></div>
						<div class="divTableCell formLabel">Database Host:<br><span class="infoTag"></span></div>
						<div class="divTableCell formFieldCell"><input type="text" value="<?php echo @$databaseHost; ?>" name="databaseHost" style="width: 200px;"></div>
					</div>
					<div class="divTableRow">
						<div class="divTableCell formQuestion"><img src="./images/question.png" class="helpIcon"></div>
						<div class="divTableCell formLabel">Table Prefix:<br><span class="infoTag"></span></div>
						<div class="divTableCell formFieldCell"><input type="text" value="<?php echo @$tablePrefix; ?>" name="tablePrefix" style="width: 200px;"></div>
					</div>
				</div>
				<p class="buttonRow"><input type="button" value="&laquo; Back" id="formBackButton"><input type="submit" value="Continue &raquo;" id="formSubmitButton"></p>
			</div>
			<?php require_once BASE_PATH.'/footer.php'; ?>
		</div>
	</div>
	</form>
</body>
</html>

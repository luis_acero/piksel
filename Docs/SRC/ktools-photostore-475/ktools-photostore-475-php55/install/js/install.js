
$(function()
{
	// Change the opacity of the product logo
	$('.prodLogo').mouseover(function()
	{
		$(this).removeClass('opac40').addClass('opac60');
	})
	.mouseout(function()
	{
		$(this).removeClass('opac60').addClass('opac40');
	});
});

/*
* Go to link or page
*/
function goto(gotolink)
{
	location.href = gotolink;
}

/*
* Open support popup window
*/
function supportPopup(pageid){
	window.open('http://www.ktools.net/wiki/content/5/276/en/how-to-use-the-installer.html','help');
}